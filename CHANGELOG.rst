..
  SPDX-License-Identifier: LGPL-2.1-or-later
  SPDX-FileCopyrightText: 2019 CERN

============
 Change Log
============

1.7.7 - 2025-01-21
==================
------------------
Changed
-------
- [ci] use acc-py_el9 instead of the deprecated acc-py_cc7
- [ci] change job dependency name after updating evergreen

1.7.6 - 2024-09-30
==================

Added
-----
- [hdl] add a first testbench

Deleted
-------
- [hdl] unused feature locmon_irq

Fixed
-----
- [drv] compatibility with Linux kernel up to version 6.10
- [drv] fix double class unregistration
- [drv] fix rescan path
- [hdl] code clean up and improvements around the DMA code
- [hdl] use style guidelines for in and out

1.7.5 - 2023-10-12
==================

Fixed
-----
- library project directory is `vmebus`
- public headers must include files using `#include <vmebus/...>`
- ensure tools are statically linked against libvmebus

1.7.4 - 2023-10-11
==================

Changed
-------
- [drv] default device name with leading zeros using `%02u`

1.7.3 - 2023-10-11
==================

Changed
-------
- [bld] improvements to the software building system
- [bld] migrate HDL building system hdlmake
- [bld] sources reorganization to better isolate common HDL code (A25 implementation
  details in a different project)
- [ci] CI pipeline from sources to tests on real hardware
- [tst] pytest based testing infrastructure completion

1.7.2 - 2023-07-26
==================

Added
-----
- [ci] basic CI pipeline
- [tst] basic pytest infrastructure

Changed
-------
- [drv] adapt code to be compatible with at least kernel 5.10
- [bld] improvements to the software building system
