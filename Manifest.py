# SPDX-FileCopyrightText: 2023 CERN (home.cern)
#
# SPDX-License-Identifier: CERN-OHL-S-2.0+

modules = {
    "local" : [
        "hdl/rtl",
    ]
}
