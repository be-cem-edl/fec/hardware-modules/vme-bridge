<!--
SPDX-FileCopyrightText: 2023 CERN

SPDX-License-Identifier: CC-BY-SA-4.0+
-->

VME Bridge HDL and Software
===========================

This project contains the software (driver, libraries and utilities) for the VME
bridge found in the MEN A20 and A25 SBCs.

It also contains the HDL module that provides a reusable Wishbone to VME bridge
(used in the MEN A25 SBC).  This module is also known as 16z002-01 in the MEN A25 SBC.

The remaining HDL files used in the MEN A25 are in the
vme-sbc-a25-pcie-vme-bridge reprository.
