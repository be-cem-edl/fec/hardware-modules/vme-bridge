# SPDX-FileCopyrightText: 2023 CERN (home.cern)
#
# SPDX-License-Identifier: CERN-OHL-S-2.0+

files = [
    "dma_mblt_boost.vhd",
    "vme_arbiter.vhd",
    "vme_au.vhd",
    "vme_bustimer.vhd",
    "vme_ctrl.vhd",
    "vme_dma_fifo.vhd",
    "vme_dma_mstr.vhd",
    "vme_dma.vhd",
    "vme_du.vhd",
    "vme_regs.vhd",
    "vme_regs_map.vhd",
    "vme_master.vhd",
    "vme_pkg.vhd",
    "vme_requester.vhd",
    "vme_wbs.vhd",
    "wbb2vme_top.vhd",
]
