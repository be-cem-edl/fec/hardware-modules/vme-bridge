# SPDX-FileCopyrightText: 2024 CERN (home.cern)
#
# SPDX-License-Identifier: LGPL-2.1-or-later

cheby --gen-hdl vme_regs_map.vhd -i vme_regs_map.cheby
