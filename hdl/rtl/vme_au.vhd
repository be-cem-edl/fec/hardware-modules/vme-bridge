-- SPDX-FileCopyrightText: 2016, MEN Mikro Elektronik GmbH
-- SPDX-License-Identifier: CERN-OHL-S-2.0+

--------------------------------------------------------------------------------
-- Title         : VME Address Unit
-- Project       : 16z002-01
--------------------------------------------------------------------------------
-- File          : vme_au.vhd
-- Author        : michael.miehling@men.de
-- Organization  : MEN Mikro Elektronik GmbH
-- Created       : 14/01/03
--------------------------------------------------------------------------------
-- Simulator     : Modelsim PE 6.6
-- Synthesis     : Quartus 15.1
--------------------------------------------------------------------------------
-- Description :
--
-- This module consists of all adress counters, switches and
-- muxes which are controlled by vme_master and vme_slave. 
-- The usage gets arbitrated by vme_sys_arbiter.
--------------------------------------------------------------------------------
-- Hierarchy:
--
-- wbb2vme
--    vme_ctrl
--       vme_au
--------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------
-- History:
--------------------------------------------------------------------------------
-- Revision 1.8  2017/06/13 07:00:00  mmiehling
-- changed vme_acc_type setting for CR/CSR and D32 to be compliant to DMA configuration bits
--
-- Revision 1.7  2015/09/16 09:20:09  mwawrik
-- Added generics A16_REG_MAPPING and USE_LONGADD
--
-- Revision 1.6  2015/04/07 14:30:16  AGeissler
-- R1: MAIN_PR002233
-- M1: Added a valid signal for sl_acc to inform the VME slave component, that
--     it can use sl_acc to define the type of the access
--
-- Revision 1.5  2014/04/17 07:35:31  MMiehling
-- added generic LONGADD_SIZE
-- changed vme slave access to A16 to sram access
-- added address modifiers for vme slave access: 0x3e, 0x3a, 0x0e, 0x0a
--
-- Revision 1.4  2014/02/07 17:00:06  MMiehling
-- bugfix: IACK addressing
--
-- Revision 1.2  2012/08/27 12:57:24  MMiehling
-- added comments
-- changed iackn handling
--
-- Revision 1.1  2012/03/29 10:14:51  MMiehling
-- Initial Revision
--
-- Revision 1.16  2010/03/12 13:38:18  mmiehling
-- changed
-- iackoutn <= iackoutn_int WHEN asn_in = '0' ELSE '1';  -- rising edge of asn_in must inactivate iackoutn immediately!
-- to
-- iackoutn <= iackoutn_int;
--
-- Revision 1.15  2006/11/17 08:55:58  mmiehling
-- added synchronisation register for iack_in and iachin_daisy
--
-- Revision 1.14  2006/06/02 15:48:53  MMiehling
-- logic for iackoutn => when asn=1 then iackoutn<=1
-- corrected condition for entering state otherirq
--
-- Revision 1.13  2006/05/26 14:34:48  MMiehling
-- added fsm for my_iack detection and iack-daisy-chain => irqs will not be lost
--
-- Revision 1.12  2006/05/18 14:29:01  MMiehling
-- iack daisy chain input was not correct detected (when dsa/b goes low is correct)
-- unused address signals of vme-master access are set to '0'
--
-- Revision 1.11  2004/11/02 11:29:50  mmiehling
-- improved timing and area
--
-- Revision 1.10  2004/07/27 17:15:35  mmiehling
-- changed pci-core to 16z014
-- changed wishbone bus to wb_bus.vhd
-- added clk_trans_wb2wb.vhd
-- improved dma
--
-- Revision 1.9  2004/06/17 13:02:23  MMiehling
-- removed clr_hit and sl_acc_reg
--
-- Revision 1.8  2003/12/17 15:51:41  MMiehling
-- byte swapping was wrong in "not swapped" mode
--
-- Revision 1.6  2003/07/14 08:38:04  MMiehling
-- lwordn was wrong
--
-- Revision 1.5  2003/06/24 13:47:04  MMiehling
-- changed int_adr
--
-- Revision 1.4  2003/06/13 10:06:31  MMiehling
-- improved
--
-- Revision 1.3  2003/04/22 11:02:56  MMiehling
-- improved timing
--
-- Revision 1.2  2003/04/02 16:11:31  MMiehling
-- Kommentar entfernt
--
-- Revision 1.1  2003/04/01 13:04:40  MMiehling
-- Initial Revision
--
--------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_unsigned.ALL;

ENTITY vme_au IS
   GENERIC (
      A16_REG_MAPPING   : boolean := TRUE;         -- if true, access to vme slave A16 space goes to vme runtime registers and above 0x800 to sram (compatible to old revisions)
                                                   -- if false, access to vme slave A16 space goes to sram
      LONGADD_SIZE      : integer range 3 TO 8:=3;
      USE_LONGADD       : boolean := TRUE          -- If FALSE, bits (7 DOWNTO 5) of SIGNAL longadd will be allocated to vme_adr_out(31 DOWNTO 29)
                                                   -- If TRUE, number of bits allocated to vme_adr_out depends on GENERIC LONGADD_SIZE
      );
   PORT (
      clk                     : IN std_logic;                        -- 66 MHz
      rst                     : IN std_logic;                        -- global reset signal (asynch)
      
      -- mensb slave
      wbs_adr_i               : IN std_logic_vector(31 DOWNTO 0);    -- mensb slave adress lines
      wbs_sel_i               : IN std_logic_vector(3 DOWNTO 0);     -- mensb slave byte enable lines
      wbs_we_i                : IN std_logic;                        -- mensb slave read/write
      vme_acc_type            : IN std_logic_vector(8 DOWNTO 0);     -- signal indicates the type of VME slave access
      ma_en_vme_data_out_reg  : IN std_logic;                        -- enable of vme_adr_out
      wbs_tga_i               : IN std_logic_vector(8 DOWNTO 0);
      
      -- vme
      vme_adr_in              : IN std_logic_vector(31 DOWNTO 0);    -- vme address input lines
      vme_adr_out             : OUT std_logic_vector(31 DOWNTO 0);   -- vme address output lines
      
      ---------------------------------------------------------------------------------------------------
      -- pins to vmebus
      asn_in                  : IN std_logic;                        -- vme adress strobe input
      vam_o                   : OUT std_logic_vector(5 DOWNTO 0);    -- vme address modifier
      vam_i                   : IN  std_logic_vector(5 DOWNTO 0);    -- vme address modifier
      dsan_out                : OUT std_logic;                       -- data strobe byte(0) out
      dsbn_out                : OUT std_logic;                       -- data strobe byte(1) out
      dsan_in                 : IN std_logic;                        -- data strobe byte(0) in
      dsbn_in                 : IN std_logic;                        -- data strobe byte(1) in
      writen_o                : OUT std_logic;                     -- write enable      tco = tbd.   tsu <= tbd.   PIN tbd.
      writen_i                : IN  std_logic;                     -- write enable      tco = tbd.   tsu <= tbd.   PIN tbd.
      iackn_o                 : OUT std_logic;                     -- handler's output !   PIN tbd.
      iackn_i                 : IN  std_logic;                     -- handler's output !   PIN tbd.
      iackin                  : IN  std_logic;                        -- vme daisy chain interrupt acknoledge input
      iackoutn                : OUT std_logic;                       -- vme daisy chain interrupt acknoledge output
      ---------------------------------------------------------------------------------------------------
      
      -- vme master
      mstr_cycle              : OUT std_logic;                       -- number of master cycles should be done (0=1x, 1=2x)
      second_word             : IN std_logic;                        -- indicates the second transmission if in D16 mode and 32bit should be transmitted
      dsn_ena                 : IN std_logic;                        -- signal switches dsan_out and dsbn_out on and off
      vam_oe                  : IN std_logic;                        -- vam output enable
      ma_d64                  : OUT std_logic;                       -- indicates a d64 burst transmission
      
      -- vme slave
      sl_writen_reg           : OUT std_logic;                       -- For berr
      iackn_in_reg            : OUT std_logic;                       -- iack signal (registered with en_vme_adr_in)
      
      -- sys_arbiter
      ma_byte_routing         : OUT std_logic;                       -- signal for byte swapping
      lwordn_mstr             : OUT std_logic;                       -- master access lwordn
      
      -- locmon
      vam_reg                 : OUT std_logic_vector(5 DOWNTO 0);    -- registered vam_i for location monitoring and berr_adr (registered with en_vme_adr_in)
      vme_adr_in_reg          : OUT std_logic_vector(31 DOWNTO 2);   -- vme adress for location monitoring and berr_adr (registered with en_vme_adr_in)

      -- vme_du
      mstr_reg                : IN std_logic_vector(13 DOWNTO 0);    -- master register (aonly, postwr, iberr, berr, req, rmw, A16_MODE, A24_MODE, A32_MODE)
      longadd                 : IN std_logic_vector(7 DOWNTO 0)      -- upper 3 address bits for A32 mode or dependent on LONGADD_SIZE
   );
END vme_au;

ARCHITECTURE vme_au_arch OF vme_au IS 
   CONSTANT AM_NON_DAT : std_logic_vector(1 DOWNTO 0):="00";   -- address modifier code for non-privileged data access
   CONSTANT AM_NON_PRO : std_logic_vector(1 DOWNTO 0):="01";   -- address modifier code for non-privileged program access
   CONSTANT AM_SUP_DAT : std_logic_vector(1 DOWNTO 0):="10";   -- address modifier code for supervisory data access    
   CONSTANT AM_SUP_PRO : std_logic_vector(1 DOWNTO 0):="11";   -- address modifier code for supervisory program access 
   
   SIGNAL vme_a1              : std_logic;
   SIGNAL dsan_out_int        : std_logic;
   SIGNAL dsbn_out_int        : std_logic;
   SIGNAL vam_in_reg          : std_logic_vector(5 DOWNTO 0);
   SIGNAL sl_writen_reg_int   : std_logic;
   SIGNAL vme_adr_in_reg_int  : std_logic_vector(31 DOWNTO 0);
   SIGNAL mstr_cycle_int      : std_logic;
   SIGNAL lwordn_mstr_int     : std_logic;
   SIGNAL asn_q, asn_q2       : std_logic;
   
BEGIN
   lwordn_mstr <= lwordn_mstr_int;
   sl_writen_reg <= sl_writen_reg_int;
   
   mstr_cycle <= mstr_cycle_int;
   
   vme_adr_in_reg <= vme_adr_in_reg_int(31 DOWNTO 2);
   
   -- if swapping is disabled, dsan and dsbn is exchanged
   -- vme_acc_type(5) = swap-bit
   dsan_out <= dsan_out_int WHEN dsn_ena = '1' AND vme_acc_type(5) = '1' ELSE
               dsbn_out_int WHEN dsn_ena = '1' AND vme_acc_type(5) = '0' ELSE '1';
   dsbn_out <= dsbn_out_int WHEN dsn_ena = '1' AND vme_acc_type(5) = '1' ELSE 
               dsan_out_int WHEN dsn_ena = '1' AND vme_acc_type(5) = '0' ELSE '1';
                  
   vme_adr_out(1 DOWNTO 0) <= vme_a1 & lwordn_mstr_int;
   
   vam_reg        <= vam_in_reg;
   
   iackoutn <= iackin;
   
   asn_writen : PROCESS (clk, rst)
   BEGIN
      IF rst = '1' THEN
         asn_q          <= '1';
         asn_q2         <= '1';
         writen_o     <= '1';
      ELSIF clk'EVENT AND clk = '1' THEN
         asn_q2 <= asn_q;
         asn_q <= asn_in;
         writen_o <= NOT wbs_we_i;
      END IF;
   END PROCESS asn_writen;
   
   mstr_adr : PROCESS(clk, rst)
   BEGIN
      IF rst = '1' THEN
         vam_in_reg                 <= (OTHERS => '0');
         vme_adr_in_reg_int         <= (OTHERS => '0');
         vme_adr_out(31 DOWNTO 2)   <= (OTHERS => '0');
         sl_writen_reg_int          <= '0';
         iackn_in_reg               <= '1';
      ELSIF clk'EVENT AND clk = '1' THEN
         
         -- select VME address based on address mode, LONGADD register and generics
         IF ma_en_vme_data_out_reg = '1' THEN
            if vme_acc_type(1 DOWNTO 0) = "00" then         -- A24
              vme_adr_out(31 DOWNTO 2) <= "00000000" & wbs_adr_i(23 DOWNTO 2);

            elsif vme_acc_type(1 downto 0) = "01" then      -- A32
              IF wbs_tga_i(7) = '0' THEN    -- single access from PCI / no dma?
                IF USE_LONGADD = TRUE THEN  -- flexible size of longadd parameter => not compatible to A21!
                  vme_adr_out(31 DOWNTO 2) <= longadd(7 DOWNTO (8-LONGADD_SIZE)) & wbs_adr_i((31-LONGADD_SIZE) DOWNTO 2);
                ELSE                        -- compatibility mode: uses 3 bits of longadd (compatible to A21/A15)
                  vme_adr_out(31 DOWNTO 2) <= longadd(2 DOWNTO 0) & wbs_adr_i(28 DOWNTO 2);
                END IF;
              ELSE                          -- dma access uses complete address (no LONGADD usage)
                vme_adr_out(31 DOWNTO 2) <= wbs_adr_i(31 DOWNTO 2);
              END IF;

            else  -- A16
              vme_adr_out(31 DOWNTO 2) <= "0000000000000000" & wbs_adr_i(15 DOWNTO 2);
            END if;

         END IF;
         
         IF asn_q = '0' and asn_q2 = '1' then                         -- samples adress and am at falling edge asn
            vme_adr_in_reg_int <= vme_adr_in;
            vam_in_reg <= vam_i;
            sl_writen_reg_int <= writen_i;
            iackn_in_reg <= iackn_i;
         END IF;
       END IF;
   END PROCESS mstr_adr;  
   
   -- vme_acc_type:
   --                  M D R S B D  A
   --                  8 7 6 5 4 32 10
   -- A16/D16          m d u 0 0 00 10
   -- A16/D32          m d u 0 0 01 10
   -- A24/D16          m d u 0 0 00 00
   -- A24/D32          m d u 0 0 01 00
   -- CR/CSR           x d u 0 0 10 00
   -- A32/D32          m d u 0 0 01 01
   -- IACK             m d u 0 0 00 11
   -- A32/D32/BLT      m d u 0 1 01 01
   -- A32/D64/BLT      m d u 0 1 11 01
   -- A24/D16/BLT      m d u 0 1 00 00
   -- A24/D32/BLT      m d u 0 1 01 00
   -- A24/D64/BLT      m d u 0 1 11 00 new
   --  " swapped       m d u 1 x xx xx
   --
   -- m = 0: non-privileged 
   -- m = 1: supervisory
   --
   -- d = 0: host access
   -- d = 1: DMA access
   --
   -- u: unused
   
   vam_proc : PROCESS (clk, rst)
   BEGIN
      IF rst = '1' THEN
         vam_o <= (OTHERS => '0');
         lwordn_mstr_int   <= '0';
         ma_byte_routing <= '0';
         mstr_cycle_int   <= '0';
         dsan_out_int <= '1';
         dsbn_out_int <= '1';
         vme_a1 <= '0';
         iackn_o <= '1';
         ma_d64 <= '0';
      ELSIF clk'EVENT AND clk = '1' THEN
         CASE vme_acc_type(4 DOWNTO 0) IS
            WHEN "00011" => 
               vam_o <= "010000";-- x10   IACK-Cycle
               iackn_o <= '0';
               mstr_cycle_int   <= '0';     -- only one cycle is permitted
               ma_d64 <= '0';
               IF wbs_sel_i = "1111" THEN
                  vme_a1 <= '0';         -- longword will be transmitted
                  dsan_out_int   <= '0';
                  dsbn_out_int   <= '0';
                  ma_byte_routing <= '0';
                  lwordn_mstr_int   <= '0';
               ELSIF (wbs_sel_i(0) = '1' OR wbs_sel_i(1) = '1') THEN
                  vme_a1 <= '0';         -- only low word will be transmitted
                  dsan_out_int   <= NOT wbs_sel_i(1);
                  dsbn_out_int   <= NOT wbs_sel_i(0);
                  ma_byte_routing <= '1';
                  lwordn_mstr_int   <= '1';
               ELSE
                  vme_a1 <= '1';         -- only high word will be transmitted
                  dsan_out_int   <= NOT wbs_sel_i(3);
                  dsbn_out_int   <= NOT wbs_sel_i(2);
                  ma_byte_routing <= '0';
                  lwordn_mstr_int   <= '1';
               END IF;

         -- A16 D16
            WHEN "00010" =>
               IF vme_acc_type(7) = '1' THEN      -- DMA access
                  IF vme_acc_type(8) = '1' THEN
                     vam_o <= "101101";   -- x2D   A16 D16 supervisory access
                  ELSE
                     vam_o <= "101001";   -- x29   A16 D16 non-privileged access
                  END IF;
               ELSE                               -- host access
                  CASE mstr_reg(9 DOWNTO 8) IS
                     WHEN AM_NON_DAT => vam_o <= "101001";   -- x29   A16 D16 non-privileged access
                     WHEN OTHERS     => vam_o <= "101101";   -- x2D   A16 D16 supervisory access
                  END CASE;
               END IF;
               iackn_o <= '1';
               ma_d64 <= '0';
               lwordn_mstr_int   <= '1';
               IF (wbs_sel_i(0) = '1' OR wbs_sel_i(1) = '1') AND (wbs_sel_i(2) = '1' OR wbs_sel_i(3) = '1') THEN
                 mstr_cycle_int   <= '1';         -- there must be two master cycles in order to transmitt 32bit in D16 mode
               ELSE
                 mstr_cycle_int   <= '0';
               END IF;
               IF second_word = '0' OR (second_word = '1' AND mstr_cycle_int = '0') THEN      -- first word of two
                 IF (wbs_sel_i(0) = '1' OR wbs_sel_i(1) = '1') THEN
                   vme_a1 <= '0';         -- only low word will be transmitted
                   dsan_out_int   <= NOT wbs_sel_i(1);
                   dsbn_out_int   <= NOT wbs_sel_i(0);
                   ma_byte_routing <= '1';
                 ELSE
                   vme_a1 <= '1';         -- only high word will be transmitted
                   dsan_out_int   <= NOT wbs_sel_i(3);
                   dsbn_out_int   <= NOT wbs_sel_i(2);
                   ma_byte_routing <= '0';
                 END IF;
               ELSE                     -- second word of two
                 dsan_out_int   <= NOT wbs_sel_i(3);
                 dsbn_out_int   <= NOT wbs_sel_i(2);
                 ma_byte_routing <= '0';
                 vme_a1 <= '1';
               END IF;
            
         -- A24 D16
            WHEN "00000" => 
              IF vme_acc_type(7) = '1' THEN      -- DMA access
                IF vme_acc_type(8) = '1' THEN
                  vam_o <= "111101";   -- x3D   A24 D16 supervisory data access
                ELSE
                  vam_o <= "111001";   -- x39   A24 D16 non-privileged data access
                END IF;
              ELSE
                CASE mstr_reg(11 DOWNTO 10) IS
                  WHEN AM_NON_DAT => vam_o <= "111001";   -- x39   A24 D16 non-privileged data access
                  WHEN AM_NON_PRO => vam_o <= "111010";   -- x3A   A24 D16 non-privileged program access
                  WHEN AM_SUP_DAT => vam_o <= "111101";   -- x3D   A24 D16 supervisory data access
                  WHEN OTHERS     => vam_o <= "111110";   -- x3E   A24 D16 supervisory program access
                END CASE;
              END IF;
              iackn_o <= '1';
              ma_d64 <= '0';
              lwordn_mstr_int   <= '1';
              IF (wbs_sel_i(0) = '1' OR wbs_sel_i(1) = '1') AND (wbs_sel_i(2) = '1' OR wbs_sel_i(3) = '1') THEN
                mstr_cycle_int   <= '1';         -- there must be two master cycles in order to transmit 32bit in D16 mode
              ELSE
                mstr_cycle_int   <= '0';
              END IF;
              IF second_word = '0' OR (second_word = '1' AND mstr_cycle_int = '0') THEN      -- first word of two
                IF (wbs_sel_i(0) = '1' OR wbs_sel_i(1) = '1') THEN
                  vme_a1 <= '0';         -- only low word will be transmitted
                  dsan_out_int   <= NOT wbs_sel_i(1);
                  dsbn_out_int   <= NOT wbs_sel_i(0);
                  ma_byte_routing <= '1';
                ELSE
                  vme_a1 <= '1';         -- only high word will be transmitted
                  dsan_out_int   <= NOT wbs_sel_i(3);
                  dsbn_out_int   <= NOT wbs_sel_i(2);
                  ma_byte_routing <= '0';
                END IF;
              ELSE                     -- second word of two
                dsan_out_int   <= NOT wbs_sel_i(3);
                dsbn_out_int   <= NOT wbs_sel_i(2);
                ma_byte_routing <= '0';
                vme_a1 <= '1';
              END IF;

         -- CR/CSR
            WHEN "01000" => 
              vam_o <= "101111";   -- x2f   CR/CSR access
              iackn_o <= '1';
              ma_d64 <= '0';
              mstr_cycle_int   <= '0';
              IF wbs_sel_i = "1111" THEN          -- D32 access
                IF vme_acc_type(5) = '1' THEN
                  ma_byte_routing <= '0';
                ELSE
                  ma_byte_routing <= '1';
                END IF;
                dsan_out_int <= '0';
                dsbn_out_int <= '0';
                vme_a1 <= '0';
                lwordn_mstr_int   <= '0';
              ELSE                                -- D16 access
                lwordn_mstr_int   <= '1';
                IF (wbs_sel_i(0) = '1' OR wbs_sel_i(1) = '1') THEN
                  vme_a1 <= '0';         -- only low word will be transmitted
                  dsan_out_int   <= NOT wbs_sel_i(1);
                  dsbn_out_int   <= NOT wbs_sel_i(0);
                  ma_byte_routing <= '1';
                ELSE
                  vme_a1 <= '1';         -- only high word will be transmitted
                  dsan_out_int   <= NOT wbs_sel_i(3);
                  dsbn_out_int   <= NOT wbs_sel_i(2);
                  ma_byte_routing <= '0';
                END IF;
              END IF;

         -- A24 D32
            WHEN "00100" => 
              IF vme_acc_type(7) = '1' THEN      -- DMA access
                IF vme_acc_type(8) = '1' THEN
                  vam_o <= "111101";   -- x3D   A24 D32 supervisory data access
                ELSE
                  vam_o <= "111001";   -- x39   A24 D32 non-privileged data access
                END IF;
              ELSE
                CASE mstr_reg(11 DOWNTO 10) IS
                  WHEN AM_NON_DAT => vam_o <= "111001";   -- x39   A24 D32 non-privileged data access
                  WHEN AM_NON_PRO => vam_o <= "111010";   -- x3A   A24 D32 non-privileged program access
                  WHEN AM_SUP_DAT => vam_o <= "111101";   -- x3D   A24 D32 supervisory data access
                  WHEN OTHERS     => vam_o <= "111110";   -- x3E   A24 D32 supervisory program access
                END CASE;
              END IF;
              iackn_o <= '1';
              ma_d64 <= '0';
              IF wbs_sel_i = "1111" THEN
                IF vme_acc_type(5) = '1' THEN
                  ma_byte_routing <= '0';
                ELSE
                  ma_byte_routing <= '1';
                END IF;
                mstr_cycle_int   <= '0';
                dsan_out_int <= '0';
                dsbn_out_int <= '0';
                vme_a1 <= '0';
                lwordn_mstr_int   <= '0';
              ELSE      -- same as D16 access
                lwordn_mstr_int   <= '1';
                IF (wbs_sel_i(0) = '1' OR wbs_sel_i(1) = '1') AND (wbs_sel_i(2) = '1' OR wbs_sel_i(3) = '1') THEN
                  mstr_cycle_int   <= '1';         -- there must be two master cycles in order to transmit 32bit in D16 mode
                ELSE
                  mstr_cycle_int   <= '0';
                END IF;
                IF second_word = '0' OR (second_word = '1' AND mstr_cycle_int = '0') THEN      -- first word of two
                  IF (wbs_sel_i(0) = '1' OR wbs_sel_i(1) = '1') THEN
                    vme_a1 <= '0';         -- only low word will be transmitted
                    dsan_out_int   <= NOT wbs_sel_i(1);
                    dsbn_out_int   <= NOT wbs_sel_i(0);
                    ma_byte_routing <= '1';
                  ELSE
                    vme_a1 <= '1';         -- only high word will be transmitted
                    dsan_out_int   <= NOT wbs_sel_i(3);
                    dsbn_out_int   <= NOT wbs_sel_i(2);
                    ma_byte_routing <= '0';
                  END IF;
                ELSE                     -- second word of two
                  dsan_out_int   <= NOT wbs_sel_i(3);
                  dsbn_out_int   <= NOT wbs_sel_i(2);
                  ma_byte_routing <= '0';
                  vme_a1 <= '1';
                END IF;
              END IF;
            
          -- A16 D32
            WHEN "00110" =>
              IF vme_acc_type(7) = '1' THEN      -- DMA access
                IF vme_acc_type(8) = '1' THEN
                  vam_o <= "101101";   -- x2D   A16 D32 supervisory access
                ELSE
                  vam_o <= "101001";   -- x29   A16 D32 non-privileged access
                END IF;
              ELSE
                CASE mstr_reg(9 DOWNTO 8) IS                 -- A16_MODE
                  WHEN AM_NON_DAT => vam_o <= "101001";   -- x29   A16 D32 non-privileged access
                  WHEN OTHERS     => vam_o <= "101101";   -- x2D   A16 D32 supervisory access
                END CASE;
              END IF;
              iackn_o <= '1';
              ma_d64 <= '0';
              IF wbs_sel_i = "1111" THEN
                IF vme_acc_type(5) = '1' THEN
                  ma_byte_routing <= '0';
                ELSE
                  ma_byte_routing <= '1';
                END IF;
                mstr_cycle_int   <= '0';
                dsan_out_int <= '0';
                dsbn_out_int <= '0';
                vme_a1 <= '0';
                lwordn_mstr_int   <= '0';
              ELSE      -- same as D16 access
                lwordn_mstr_int   <= '1';
                IF (wbs_sel_i(0) = '1' OR wbs_sel_i(1) = '1') AND (wbs_sel_i(2) = '1' OR wbs_sel_i(3) = '1') THEN
                  mstr_cycle_int   <= '1';         -- there must be two master cycles in order to transmitt 32bit in D16 mode
                ELSE
                  mstr_cycle_int   <= '0';
                END IF;
                IF second_word = '0' OR (second_word = '1' AND mstr_cycle_int = '0') THEN      -- first word of two
                  IF (wbs_sel_i(0) = '1' OR wbs_sel_i(1) = '1') THEN
                    vme_a1 <= '0';         -- only low word will be transmitted
                    dsan_out_int   <= NOT wbs_sel_i(1);
                    dsbn_out_int   <= NOT wbs_sel_i(0);
                    ma_byte_routing <= '1';
                  ELSE
                    vme_a1 <= '1';         -- only high word will be transmitted
                    dsan_out_int   <= NOT wbs_sel_i(3);
                    dsbn_out_int   <= NOT wbs_sel_i(2);
                    ma_byte_routing <= '0';
                  END IF;
                ELSE                     -- second word of two
                  dsan_out_int   <= NOT wbs_sel_i(3);
                  dsbn_out_int   <= NOT wbs_sel_i(2);
                  ma_byte_routing <= '0';
                  vme_a1 <= '1';
                END IF;
              END IF;
            
         -- A32 D32
            WHEN "00101" =>   
              IF vme_acc_type(7) = '1' THEN      -- DMA access
                IF vme_acc_type(8) = '1' THEN
                  vam_o <= "001101";   -- x0D   A32 D32 supervisory data access
                ELSE
                  vam_o <= "001001";   -- x09   A32 D32 non-privileged data access
                END IF;
              ELSE
                CASE mstr_reg(13 DOWNTO 12) IS               -- A32_MODE
                  WHEN AM_NON_DAT => vam_o <= "001001";   -- x09   A32 D32 non-privileged data access
                  WHEN AM_NON_PRO => vam_o <= "001010";   -- x0A   A32 D32 non-privileged program access
                  WHEN AM_SUP_DAT => vam_o <= "001101";   -- x0D   A32 D32 supervisory data access
                  WHEN OTHERS     => vam_o <= "001110";   -- x0E   A32 D32 supervisory program access
                END CASE;
              END IF;
              iackn_o <= '1';
              ma_d64 <= '0';
              IF wbs_sel_i = "1111" THEN
                IF vme_acc_type(5) = '1' THEN
                  ma_byte_routing <= '0';
                ELSE
                  ma_byte_routing <= '1';
                END IF;
                mstr_cycle_int   <= '0';
                dsan_out_int <= '0';
                dsbn_out_int <= '0';
                vme_a1 <= '0';
                lwordn_mstr_int   <= '0';
              ELSE      -- same as D16 access
                lwordn_mstr_int   <= '1';
                IF (wbs_sel_i(0) = '1' OR wbs_sel_i(1) = '1') AND (wbs_sel_i(2) = '1' OR wbs_sel_i(3) = '1') THEN
                  mstr_cycle_int   <= '1';         -- there must be two master cycles in order to transmit 32bit in D16 mode
                ELSE
                  mstr_cycle_int   <= '0';
                END IF;
                IF second_word = '0' OR (second_word = '1' AND mstr_cycle_int = '0') THEN      -- first word of two
                  IF (wbs_sel_i(0) = '1' OR wbs_sel_i(1) = '1') THEN
                    vme_a1 <= '0';         -- only low word will be transmitted
                    dsan_out_int   <= NOT wbs_sel_i(1);
                    dsbn_out_int   <= NOT wbs_sel_i(0);
                    ma_byte_routing <= '1';
                  ELSE
                    vme_a1 <= '1';         -- only high word will be transmitted
                    dsan_out_int   <= NOT wbs_sel_i(3);
                    dsbn_out_int   <= NOT wbs_sel_i(2);
                    ma_byte_routing <= '0';
                  END IF;
                ELSE                     -- second word of two
                  dsan_out_int   <= NOT wbs_sel_i(3);
                  dsbn_out_int   <= NOT wbs_sel_i(2);
                  ma_byte_routing <= '0';
                  vme_a1 <= '1';
                END IF;
              END IF;
                      
         -- A24 D16 BLT
            WHEN "10000" => 
              IF vme_acc_type(8) = '0' THEN
                vam_o <= "111011";-- x3b   A24 D16 blt non-privileged
              ELSE
                vam_o <= "111111";-- x3f   A24 D16 blt supervisory
              END IF;
              iackn_o <= '1';
              ma_d64 <= '0';
              lwordn_mstr_int   <= '1';
              IF (wbs_sel_i(0) = '1' OR wbs_sel_i(1) = '1') AND (wbs_sel_i(2) = '1' OR wbs_sel_i(3) = '1') THEN
                mstr_cycle_int   <= '1';         -- there must be two master cycles in order to transmit 32bit in D16 mode
              ELSE
                mstr_cycle_int   <= '0';
              END IF;
              IF second_word = '0' OR (second_word = '1' AND mstr_cycle_int = '0') THEN      -- first word of two
                IF (wbs_sel_i(0) = '1' OR wbs_sel_i(1) = '1') THEN
                  vme_a1 <= '0';         -- only low word will be transmitted
                  dsan_out_int   <= NOT wbs_sel_i(1);
                  dsbn_out_int   <= NOT wbs_sel_i(0);
                  ma_byte_routing <= '1';
                ELSE
                  vme_a1 <= '1';         -- only high word will be transmitted
                  dsan_out_int   <= NOT wbs_sel_i(3);
                  dsbn_out_int   <= NOT wbs_sel_i(2);
                  ma_byte_routing <= '0';
                END IF;
              ELSE                     -- second word of two
                dsan_out_int   <= NOT wbs_sel_i(3);
                dsbn_out_int   <= NOT wbs_sel_i(2);
                ma_byte_routing <= '0';
                vme_a1 <= '1';
              END IF;
                        
         -- A32 D32 BLT
            WHEN "10101" =>
              IF vme_acc_type(8) = '0' THEN
                vam_o <= "001011";-- x0b   A32 D32 blt non-privileged
              ELSE
                vam_o <= "001111";-- x0f   A32 D32 blt supervisory
              END IF;
              iackn_o <= '1';
              IF vme_acc_type(5) = '1' THEN
                ma_byte_routing <= '0';
              ELSE
                ma_byte_routing <= '1';
              END IF;
              mstr_cycle_int   <= '0';
              dsan_out_int <= '0';
              dsbn_out_int <= '0';
              vme_a1 <= '0';
              lwordn_mstr_int   <= '0';
              ma_d64 <= '0';
                      
         -- A24 D32 BLT
            WHEN "10100" =>   
               IF vme_acc_type(8) = '0' THEN
                  vam_o <= "111011";-- x3b   A24 D32 blt non-privileged
               ELSE
                  vam_o <= "111111";-- x3f   A24 D32 blt supervisory
               END IF;
               iackn_o <= '1';
               IF vme_acc_type(5) = '1' THEN
                  ma_byte_routing <= '0';
               ELSE
                  ma_byte_routing <= '1';
               END IF;
               mstr_cycle_int    <= '0';
               dsan_out_int      <= '0';
               dsbn_out_int      <= '0';
               vme_a1            <= '0';
               lwordn_mstr_int   <= '0';
               ma_d64            <= '0';
               
         -- A24 D64 MBLT
            WHEN "11100" =>
               IF vme_acc_type(8) = '0' THEN
                  vam_o <= "111000";-- x38   A24 D64 mblt non-privileged
               ELSE
                  vam_o <= "111100";-- x3c   A24 D64 mblt supervisory
               END IF;
               lwordn_mstr_int   <= '0';
               ma_byte_routing   <= '1';
               mstr_cycle_int    <= '1';   -- D64
               dsan_out_int      <= '0';
               dsbn_out_int      <= '0';
               vme_a1            <= '0';
               iackn_o         <= '1';
               ma_d64            <= '1';
               
         -- A32 D64 MBLT
            WHEN "11101" =>
               IF vme_acc_type(8) = '0' THEN
                  vam_o <= "001000";-- x08   A32 D64 mblt non-privileged
               ELSE
                  vam_o <= "001100";-- x0c   A32 D64 mblt supervisory
               END IF;
               lwordn_mstr_int   <= '0';
               ma_byte_routing   <= '1';
               mstr_cycle_int    <= '1';   -- D64
               dsan_out_int      <= '0';
               dsbn_out_int      <= '0';
               vme_a1            <= '0';
               iackn_o         <= '1';
               ma_d64            <= '1';
            
            WHEN OTHERS => -- A32 D64 MBLT
               IF vme_acc_type(8) = '0' THEN
                  vam_o <= "001000";-- x08   A32 D64 mblt non-privileged
               ELSE
                  vam_o <= "001100";-- x0c   A32 D64 mblt supervisory
               END IF;
               ma_d64            <= '0';
               iackn_o         <= '1';
               lwordn_mstr_int   <= '0';
               ma_byte_routing   <= '0';
               mstr_cycle_int    <= '0';
               dsan_out_int      <= '1';
               dsbn_out_int      <= '1';
               vme_a1            <= '0';
         END CASE;
      END IF;
   END PROCESS vam_proc;
   
END vme_au_arch;
