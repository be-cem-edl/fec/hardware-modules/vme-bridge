-- SPDX-FileCopyrightText: 2016, MEN Mikro Elektronik GmbH
-- SPDX-License-Identifier: CERN-OHL-S-2.0+

--------------------------------------------------------------------------------
-- Title         : VME IP Core Toplevel
-- Project       : 16z002-01
--------------------------------------------------------------------------------
-- File          : vme_ctrl.vhd
-- Author        : michael.miehling@men.de
-- Organization  : MEN Mikro Elektronik GmbH
-- Created       : 15/12/16
--------------------------------------------------------------------------------
-- Simulator     : Modelsim PE 6.6
-- Synthesis     : Quartus 15.1
--------------------------------------------------------------------------------
-- Description :
--

--------------------------------------------------------------------------------
-- Hierarchy:
--
-- wbb2vme
--    vme_ctrl
--       vme_du
--       vme_au
--       vme_locmon
--       vme_mailbox
--       vme_master
--       vme_slave
--       vme_requester
--       vme_bustimer
--       vme_sys_arbiter
--       vme_arbiter
--       vme_wbm
--       vme_wbs
--------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------
-- History:
--------------------------------------------------------------------------------
-- $Revision: 1.10 $
--
-- $Log: vme_ctrl.vhd,v $
-- Revision 1.10  2015/09/16 09:20:07  mwawrik
-- Added generics A16_REG_MAPPING and USE_LONGADD
--
-- Revision 1.9  2015/04/07 14:30:14  AGeissler
-- R1: New signals sl_acc_valid and asn_in_sl_reg
-- M1: Connected these signals to the corresponding component
--
-- Revision 1.8  2014/04/17 07:35:27  MMiehling
-- added generic LONGADD_SIZE
-- added signal prevent_sysrst
--
-- Revision 1.7  2013/09/12 08:45:32  mmiehling
-- added bit 8 of tga for address modifier extension (supervisory, non-privileged data/program)
--
-- Revision 1.6  2012/11/22 09:20:43  MMiehling
-- removed dummy signal
--
-- Revision 1.5  2012/11/15 09:43:55  MMiehling
-- connected each interrupt source to interface in order to support edge triggered msi
--
-- Revision 1.4  2012/11/12 08:13:13  MMiehling
-- changed comments
--
-- Revision 1.3  2012/09/25 11:21:47  MMiehling
-- removed unused signals
--
-- Revision 1.2  2012/08/27 12:57:20  MMiehling
-- general rework
--
-- Revision 1.1  2012/03/29 10:14:49  MMiehling
-- Initial Revision
--
-- Revision 1.14  2010/03/12 13:38:20  mmiehling
-- changed commments
--
-- Revision 1.13  2006/06/02 15:48:57  MMiehling
-- changed comment
--
-- Revision 1.12  2006/05/18 14:29:05  MMiehling
-- added sl_acc for mailbox
--
-- Revision 1.11  2005/02/04 13:44:14  mmiehling
-- added generic simulation; added combinations of addr3+4
--
-- Revision 1.10  2004/11/02 11:29:55  mmiehling
-- moved dma_reg to vme_du
--
-- Revision 1.9  2004/07/27 17:15:39  mmiehling
-- changed pci-core to 16z014
-- changed wishbone bus to wb_bus.vhd
-- added clk_trans_wb2wb.vhd
-- improved dma
--
-- Revision 1.8  2004/06/17 13:02:29  MMiehling
-- removed clr_hit and sl_acc_reg
--
-- Revision 1.7  2003/12/17 15:51:45  MMiehling
-- byte swapping in "not swapped" mode was wrong
--
-- Revision 1.6  2003/12/01 10:03:53  MMiehling
-- changed all
--
-- Revision 1.5  2003/07/14 08:38:08  MMiehling
-- changed rst_counter; added lwordn
--
-- Revision 1.4  2003/06/24 13:47:08  MMiehling
-- removed burst; added loc_keep and rst_aonly
--
-- Revision 1.3  2003/06/13 10:06:35  MMiehling
-- improved
--
-- Revision 1.2  2003/04/22 11:03:00  MMiehling
-- added locmon and mailbox
--
-- Revision 1.1  2003/04/01 13:04:42  MMiehling
-- Initial Revision
--
--------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;     
USE work.vme_pkg.all;

ENTITY vme_ctrl IS
GENERIC (
   g_simulation : boolean := false;
   A16_REG_MAPPING   : boolean := TRUE;                        -- if true, access to vme slave A16 space goes to vme runtime registers and above 0x800 to sram (compatible to old revisions)
                                                               -- if false, access to vme slave A16 space goes to sram
   LONGADD_SIZE      : integer range 3 TO 8:=3;
   USE_LONGADD       : boolean := TRUE                          -- If FALSE, bits (7 DOWNTO 5) of SIGNAL longadd will be allocated to vme_adr_out(31 DOWNTO 29)
                                                                -- If TRUE, number of bits allocated to vme_adr_out depends on GENERIC LONGADD_SIZE
   );
PORT (
   clk               : IN std_logic;                        -- 66 MHz
   rst               : IN std_logic;                        -- global reset signal (asynch)
   startup_rst       : IN std_logic;                        -- powerup reset
   prevent_sysrst    : IN std_logic;                        -- if "1", sysrst_n_out will not be activated after powerup,
                                                            -- if "0", sysrst_n_out will be activated if in slot1 and system reset is active (sysc_bit or rst)
   -- dma
   dma_reached_size_i    : in std_logic;
    
   -- mensb slave
   wbs_stb_i         : IN std_logic;
   wbs_ack_o         : OUT std_logic;
   wbs_err_o         : OUT std_logic;
   wbs_we_i          : IN std_logic;
   wbs_sel_i         : IN std_logic_vector(3 DOWNTO 0);
   wbs_cyc_i         : IN std_logic;
   wbs_adr_i         : IN std_logic_vector(31 DOWNTO 0);
   wbs_dat_o         : OUT std_logic_vector(31 DOWNTO 0);
   wbs_dat_i         : IN std_logic_vector(31 DOWNTO 0);
   wbs_tga_i         : IN std_logic_vector(8 DOWNTO 0);

   -- the VME signals:
   va_o              : OUT std_logic_vector(31 DOWNTO 0);    -- address
   va_i              : IN  std_logic_vector(31 DOWNTO 0);    -- address
   vd_o              : OUT std_logic_vector(31 DOWNTO 0);    -- data
   vd_i              : IN  std_logic_vector(31 DOWNTO 0);    -- data
   vam_o             : OUT std_logic_vector(5 DOWNTO 0);     -- address modifier (controlled by vam_oe)
   vam_i             : IN  std_logic_vector(5 DOWNTO 0);     -- address modifier
   writen_o          : OUT std_logic;                        -- write enable (controlled by vam_oe)
   writen_i          : IN  std_logic;                        -- write enable
   iackn_o           : OUT std_logic;                          -- iack (controlled by vam_oe)
   iackn_i           : IN  std_logic;                          -- iack input
   irq_i_n           : IN  std_logic_vector(7 DOWNTO 1);       -- interrupt request inputs
   as_o_n            : OUT   std_logic;                        -- address strobe out  
   as_oe_n           : OUT   std_logic;                        -- address strobe output enable  
   as_i_n            : IN    std_logic;                        -- address strobe in
   sysresn           : OUT   std_logic;                        -- system reset out 
   sysresin          : IN    std_logic;                        -- system reset in
   ds_o_n            : OUT   std_logic_vector(1 DOWNTO 0);     -- data strobe outputs
   ds_i_n            : IN   std_logic_vector(1 DOWNTO 0);      -- data strobe inputs
   ds_oe_n           : OUT std_logic;                          -- data strobe output enable
   berrn             : OUT   std_logic;                        -- bus error out    
   berrin            : IN    std_logic;                        -- bus error in 
   dtackn            : OUT   std_logic;                        -- dtack out   
   dtackin           : IN    std_logic;                        -- dtack in
   slot01n           : OUT   std_logic;                        -- indicates whether controller has detected position in slot 1 (low active)
   sysfail_i_n       : IN   std_logic;                        -- system failure interrupt input
   sysfail_o_n       : OUT   std_logic;                        -- system failure interrupt output
   bbsyn             : OUT   std_logic;                        -- bus busy out    
   bbsyin            : IN    std_logic;                        -- bus busy in     
   br_i_n            : IN std_logic_vector(3 DOWNTO 0);        -- bus request inputs
   br_o_n            : OUT std_logic_vector(3 DOWNTO 0);       -- bus request outputs
   iackin            : IN    std_logic;                        -- Interrupter's input
   iackoutn          : OUT   std_logic;                        -- Interrupter's output
   bg_i_n            : IN  std_logic_vector(3 DOWNTO 0);       -- bus grant input
   bg_o_n            : OUT std_logic_vector(3 DOWNTO 0);       -- bus grant output
   
   -- vme status signals
   berr_vam          : OUT std_logic_vector(5 DOWNTO 0);
   berr_adr          : OUT std_logic_vector(31 DOWNTO 2);
   berr_writen       : OUT std_logic;
   berr_iackn        : OUT std_logic;
   vme_mstr_busy     : OUT std_logic;                          -- indicates vme bus master is active

   brl               : IN std_logic_vector(1 DOWNTO 0);
   longadd           : IN std_logic_vector(7 DOWNTO 0);
   mstr_reg          : IN std_logic_vector(13 DOWNTO 0);
   sysc_reg          : IN std_logic_vector(1 DOWNTO 0);
   rst_rmw           : OUT std_logic;
   set_berr          : OUT std_logic;
   rst_aonly         : OUT std_logic;
   set_sysc          : OUT std_logic;
   clr_sysr          : OUT std_logic;
   set_ato           : OUT std_logic;

   --data bus bus control signals for vmebus drivers
   d_dir             : OUT std_logic;                          -- external driver control data direction (1: drive to vmebus 0: drive to fpga)
   d_oe_n            : OUT std_logic;                          -- external driver control data output enable low active
   am_dir            : OUT std_logic;                          -- external driver control address modifier direction (1: drive to vmebus 0: drive to fpga)
   am_oe_n           : OUT std_logic;                          -- external driver control address modifier output enable low activ 
   a_dir             : OUT std_logic;                          -- external driver control address direction (1: drive to vmebus 0: drive to fpga)
   a_oe_n            : OUT std_logic;                          -- external driver control address output enable low activ
   
   v2p_rst           : OUT std_logic                           -- Reset between VMEbus and Host CPU
     );
END vme_ctrl;

ARCHITECTURE vme_ctrl_arch OF vme_ctrl IS 
   -- vme_wbs
   SIGNAL sel_loc_data_out             : std_logic_vector(1 DOWNTO 0);
   SIGNAL vme_acc_type                 : std_logic_vector(8 DOWNTO 0);     -- signal indicates the type of VME access
   SIGNAL ma_en_vme_data_out_reg       : std_logic;   
   SIGNAL wb_dma_acc                   : std_logic;                        -- indicates dma_access
                                       
   -- mensb_mstr
   SIGNAL burst                        : std_logic;
                                       
   -- vme_du
   SIGNAL vme_adr_out                  : std_logic_vector(31 DOWNTO 0);
   SIGNAL FairReqEn                    : std_logic;

   -- vme_au
   SIGNAL ma_d64                       : std_logic;
   SIGNAL vme_adr_in                   : std_logic_vector(31 DOWNTO 0);
   SIGNAL ma_byte_routing              : std_logic;
   SIGNAL lwordn_mstr                  : std_logic;                        -- master access lwordn
   SIGNAL set_berr_int                 : std_logic;
   
   -- vme_sys_arbiter
   SIGNAL mensb_active                 : std_logic;
   SIGNAL ma_en_vme_data_out_reg_high  : std_logic:='0';
   SIGNAL swap                         : std_logic;

   -- vme_master
   SIGNAL ma_en_vme_data_in_reg        : std_logic;
   SIGNAL ma_en_vme_data_in_reg_high   : std_logic;                        -- load high register signal in data switch unit for rd vme
   SIGNAL dwb                          : std_logic;
   SIGNAL run_mstr                     : std_logic;
   SIGNAL mstr_busy                    : std_logic;
   SIGNAL mstr_ack                     : std_logic;
   SIGNAL brel                         : std_logic;
   SIGNAL dsn_ena                      : std_logic;
   SIGNAL mstr_cycle                   : std_logic;
   SIGNAL ma_second_word               : std_logic;
   SIGNAL vam_oe                       : std_logic;
   SIGNAL asn_out                      : std_logic;
   SIGNAL ds_o_n_int                   : std_logic_vector(1 DOWNTO 0);
   
   -- bustimer
   SIGNAL bgouten                      : std_logic;
   
   -- location
   
   -- vmearbiter
   SIGNAL bgintn                       : std_logic_vector(3 DOWNTO 0);
   
   -- interrupter
   
   -- handler
   
   -- requester
   SIGNAL dgb                          : std_logic;
   
   SIGNAL ma_io_ctrl                   : io_ctrl_type;
BEGIN
   
   d_dir     <= ma_io_ctrl.d_dir  ; -- NOT oe_fpga_dn_int;
   d_oe_n    <= ma_io_ctrl.d_oe_n ; -- '0';
   am_dir    <= ma_io_ctrl.am_dir ; -- dir_vam;
   am_oe_n   <= ma_io_ctrl.am_oe_n; -- '0';
   a_dir     <= ma_io_ctrl.a_dir  ; -- ;
   a_oe_n    <= ma_io_ctrl.a_oe_n ; -- '0';  
   
   ds_oe_n   <= '0' WHEN ds_o_n_int(0) = '0' OR ds_o_n_int(1) = '0' ELSE '1';
   ds_o_n <= ds_o_n_int;
   
   as_o_n <= asn_out;
   as_oe_n <= asn_out;
   
   vme_mstr_busy <= mstr_busy;
   FairReqEn <= mstr_reg(6);
   
   set_berr <= set_berr_int;

   du : entity work.vme_du
   PORT MAP (
      clk                        => clk,
      rst                        => rst,
      d64                        => ma_d64,
      sel_loc_data_out           => sel_loc_data_out,
      second_word                => ma_second_word,
      swap                       => swap,
      lwordn                     => lwordn_mstr,
      vme_adr_out                => vme_adr_out,
      byte_routing               => ma_byte_routing,
      vme_adr_in                 => vme_adr_in,
      en_vme_data_out_reg        => ma_en_vme_data_out_reg,
      en_vme_data_out_reg_high   => ma_en_vme_data_out_reg_high,
      en_vme_data_in_reg         => ma_en_vme_data_in_reg,
      en_vme_data_in_reg_high    => ma_en_vme_data_in_reg_high,
      wbs_dat_o                  => wbs_dat_o,
      wbs_dat_i                  => wbs_dat_i,
      vd_o                       => vd_o,
      vd_i                       => vd_i,
      va_o                       => va_o,
      va_i                       => va_i
   );

   
   mensb_active <= '1';
   swap <= vme_acc_type(5);

   wbs : entity work.vme_wbs
   PORT MAP (
      clk                           => clk,
      rst                           => rst,
      set_berr                      => set_berr_int,
      wbs_stb_i                     => wbs_stb_i,
      wbs_ack_o                     => wbs_ack_o,
      wbs_err_o                     => wbs_err_o,
      wbs_we_i                      => wbs_we_i,
      wbs_cyc_i                     => wbs_cyc_i,
      wbs_tga_i                     => wbs_tga_i,
      wb_dma_acc                    => wb_dma_acc,
      dma_reached_size_i            => dma_reached_size_i,
      ma_en_vme_data_out_reg        => ma_en_vme_data_out_reg,
      ma_en_vme_data_out_reg_high   => ma_en_vme_data_out_reg_high,
      mensb_req                     => open,
      mensb_active                  => mensb_active,
      vme_acc_type                  => vme_acc_type,
      run_mstr                      => run_mstr,
      mstr_ack                      => mstr_ack,
      mstr_busy                     => mstr_busy,
      burst                         => burst,
      sel_loc_data_out              => sel_loc_data_out
   );
   
   au : entity work.vme_au
   GENERIC MAP (
      A16_REG_MAPPING   => A16_REG_MAPPING,
      LONGADD_SIZE      => LONGADD_SIZE,
      USE_LONGADD       => USE_LONGADD
   )
   PORT MAP (
      clk                     => clk,
      rst                     => rst,
      wbs_adr_i               => wbs_adr_i,
      wbs_sel_i               => wbs_sel_i,
      wbs_we_i                => wbs_we_i,
      wbs_tga_i               => wbs_tga_i,
      vme_adr_in              => vme_adr_in,
      vme_adr_out             => vme_adr_out,
      vme_adr_in_reg          => berr_adr,
      ma_en_vme_data_out_reg  => ma_en_vme_data_out_reg,
      asn_in                  => as_i_n,
      iackn_o                 => iackn_o,
      iackn_i                 => iackn_i,
      iackin                  => iackin,
      iackoutn                => iackoutn,
      vam_oe                  => vam_oe,
      vam_o                   => vam_o,
      vam_i                   => vam_i,
      vme_acc_type            => vme_acc_type,
      second_word             => ma_second_word,
      dsn_ena                 => dsn_ena,
      mstr_reg                => mstr_reg,
      longadd                 => longadd,
      mstr_cycle              => mstr_cycle,
      ma_byte_routing         => ma_byte_routing,
      writen_i                => writen_i,
      writen_o                => writen_o,
      ma_d64                  => ma_d64,
      lwordn_mstr             => lwordn_mstr,
      vam_reg                 => berr_vam,
      dsan_out                => ds_o_n_int(0),
      dsbn_out                => ds_o_n_int(1),
      dsan_in                 => ds_i_n(0),
      dsbn_in                 => ds_i_n(1),
      sl_writen_reg           => berr_writen,
      iackn_in_reg            => berr_iackn
   );
   
   master : entity work.vme_master
   PORT MAP(
      clk                        => clk,
      rst                        => rst,
      run_mstr                   => run_mstr,
      mstr_ack                   => mstr_ack,
      mstr_busy                  => mstr_busy,
      vme_req                    => open,
      burst                      => burst,
      ma_en_vme_data_in_reg      => ma_en_vme_data_in_reg,
      ma_en_vme_data_in_reg_high => ma_en_vme_data_in_reg_high,
      wb_dma_acc                 => wb_dma_acc,
      brel                       => brel,
      wbs_we_i                   => wbs_we_i,
      dwb                        => dwb,
      dgb                        => dgb,
      berrn_in                   => berrin,
      dtackn_in                  => dtackin,
      d64                        => ma_d64,
      asn_out                    => asn_out,
      rst_aonly                  => rst_aonly,
      rst_rmw                    => rst_rmw,
      set_berr                   => set_berr_int,
      vam_oe                     => vam_oe,
      dsn_ena                    => dsn_ena,
      mstr_cycle                 => mstr_cycle,
      second_word                => ma_second_word,
      asn_in                     => as_i_n,
      mstr_reg                   => mstr_reg(5 DOWNTO 0),
      ma_io_ctrl                 => ma_io_ctrl
   );
   
   requester : entity work.vme_requester
   PORT MAP (
      clk                 => clk,
      rst                 => rst,
      br_i_n             => br_i_n,
      br_o_n             => br_o_n,
      bg_o_n             => bg_o_n,
      bbsyn_in            => bbsyin,
      bbsyn               => bbsyn,
      dwb                 => dwb,
      dgb                 => dgb,
      FairReqEn           => FairReqEn,
      brl                 => brl,
      bgintn              => bgintn,
      req_bit             => mstr_reg(1),
      brel                => brel
   );
   
   arbiter : entity work.vme_arbiter 
   PORT MAP (
      clk            => clk,
      rst            => rst,
      bgintn         => bgintn,
      set_ato        => set_ato,
      sysc_bit       => sysc_reg(0),
      bgouten        => bgouten,
      br_i_n         => br_i_n,
      bbsyn_in       => bbsyin,
      bg_i_n         => bg_i_n
   );
   
   bustimer : entity work.vme_bustimer
   generic map (
      g_simulation =>    g_simulation
   )
   PORT MAP (
      clk               => clk,
      rst               => rst,
      startup_rst       => startup_rst,
      prevent_sysrst    => prevent_sysrst,
      set_sysc          => set_sysc,
      sysc_bit          => sysc_reg(0),
      clr_sysr          => clr_sysr,
      sysr_bit          => sysc_reg(1),
      dsain             => ds_i_n(0),
      dsbin             => ds_i_n(1),
      bgouten           => bgouten,
      sysfailn          => sysfail_o_n,
      sysrstn_in        => sysresin,
      sysrstn_out       => sysresn,
      v2p_rst           => v2p_rst,
      bg3n_in           => bg_i_n(3),
      slot01n           => slot01n,
      berrn_out         => berrn
      );
   
   dtackn <= '1';
END vme_ctrl_arch;


