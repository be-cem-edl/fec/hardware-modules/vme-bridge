-- SPDX-FileCopyrightText: 2016, MEN Mikro Elektronik GmbH
--
-- SPDX-License-Identifier: CERN-OHL-S-2.0+
--------------------------------------------------------------------------------
-- Title         : DMA for VME Interface
-- Project       : 16z002-01
--------------------------------------------------------------------------------
-- File          : dma.vhd
-- Author        : michael.miehling@men.de
-- Organization  : MEN Mikro Elektronik GmbH
-- Created       : 24/06/03
--------------------------------------------------------------------------------
-- Simulator     : Modelsim PE 6.6
-- Synthesis     : Quartus 15.1
--------------------------------------------------------------------------------
-- Description :
--
-- The vme core has a DMA controller for high performance data transfers between
-- the SRAM, PCI space and VMEbus. It is operated through a series of registers
-- that control the source/destination for the data, length of the transfer and
-- the transfer protocol (A24 or A32) to be used. These registers are not
-- directly  accessible, but they will be loaded with the content of the Buffer
-- Descriptor(s) located in the local SRAM.
-- One buffer descriptor may be linked to the next buffer descriptor, such that
-- when the DMA has completed the operations described by one buffer descriptor,
-- it automatically moves on to the next buffer descriptor in the local SRAM
-- list. The last buffer descriptor is reached, when the DMA_NULL bit is set in
-- the corresponding buffer descriptor. The maximum number of linked buffer
-- descriptors is 112.
-- The DMA supports interrupt assertion when all specified buffer descriptors
-- are processed (signaled via dma_irq to PCIe, see DMA_IEN).
-- The DMA controller is able to transfer data from the SRAM, PCI space and
-- VMEbus to each other. For this reason source and/or destination address can
-- be incremented or not � depending on the settings. The source and destination
-- address must be 8-byte aligned to each other.
-- The scatter-gather list is located in the local SRAM area, so a DMA can also
-- be initiated by an external VME master by accessing the SRAM via A24/A32
-- slave and the DMA Status Register via A16 slave.
-- If transfers to PCI space has to be done, the used memory space must be
-- allocated for this function in order to prevent data mismatch!
-- If DMA functionality is used, the entire local SRAM cannot be used by other
-- functions, because the buffer descriptors are located at the end of this!
--------------------------------------------------------------------------------
-- Hierarchy:
--
-- wbb2vme
--    vme_dma
--       vme_dma_mstr
--       vme_dma_slv
--       vme_dma_arbiter
--       vme_dma_du
--       vme_dma_au
--       vme_dma_fifo
--          fifo_256x32bit
--------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------
-- History:
--------------------------------------------------------------------------------
-- $Revision: 1.2 $
--
-- $Log: vme_dma.vhd,v $
-- Revision 1.2  2013/09/12 08:45:30  mmiehling
-- added bit 8 of tga for address modifier extension (supervisory, non-privileged data/program)
--
-- Revision 1.1  2012/03/29 10:14:48  MMiehling
-- Initial Revision
--
-- Revision 1.4  2006/05/18 14:02:16  MMiehling
-- changed comment
--
-- Revision 1.1  2005/10/28 17:52:20  mmiehling
-- Initial Revision
--
-- Revision 1.3  2004/08/13 15:41:08  mmiehling
-- removed dma-slave and improved timing
--
-- Revision 1.2  2004/07/27 17:23:15  mmiehling
-- removed slave port
--
-- Revision 1.1  2004/07/15 09:28:46  MMiehling
-- Initial Revision
--
--------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY vme_dma IS
PORT (
   rst            : IN std_logic;
   clk            : IN std_logic;
   irq_o          : OUT std_logic;                          -- irq for cpu; asserted when done or error (if enabled)

   -- vme_du
   dma_sta        : IN std_logic_vector(9 DOWNTO 0);
   clr_dma_en     : OUT std_logic;
   set_dma_err    : OUT std_logic;
   dma_act_bd     : OUT std_logic_vector(3 DOWNTO 0);

   -- vme_master
   cycle_end_o    : out std_logic;

   --  dbram
   dbram_addr_o   : OUT std_logic_vector(3 downto 0);
   dbram_data_i   : IN  std_logic_vector(127 downto 0);

   -- wb-master
   stb_o          : OUT std_logic;
   ack_i          : IN std_logic;
   we_o           : OUT std_logic;
   cti_o          : OUT std_logic_vector(2 DOWNTO 0);
   tga_o          : OUT std_logic_vector(8 DOWNTO 0);      -- type of dma
   err_i          : IN std_logic;
   cyc_o_vme      : OUT std_logic;
   cyc_o_pci      : OUT std_logic;
   sel_o          : OUT std_logic_vector(3 DOWNTO 0);
   adr_o          : OUT std_logic_vector(31 DOWNTO 0);
   mstr_dat_o     : OUT std_logic_vector(31 DOWNTO 0);
   mstr_dat_i     : IN std_logic_vector(31 DOWNTO 0);

   -- dedicated PCI wb-master
   pci_cyc_o      : OUT std_logic;
   pci_stb_o      : OUT std_logic;
   pci_we_o       : OUT std_logic;
   pci_sel_o      : OUT std_logic_vector(3 DOWNTO 0);
   pci_tga_o      : OUT std_logic_vector(8 DOWNTO 0);
   pci_cti_o      : OUT std_logic_vector(2 DOWNTO 0);
   pci_adr_o      : OUT std_logic_vector(31 DOWNTO 0);
   pci_dat_o      : OUT std_logic_vector(31 DOWNTO 0);
   pci_dat_i      : IN std_logic_vector(31 DOWNTO 0);
   pci_ack_i      : IN std_logic;
   pci_err_i      : IN std_logic

  );
END vme_dma;

ARCHITECTURE vme_dma_arch OF vme_dma IS

   -- fifo
   SIGNAL fifo_almost_full    : std_logic;
   SIGNAL fifo_almost_empty   : std_logic;
   SIGNAL fifo_empty          : std_logic;
   SIGNAL fifo_full           : std_logic;

   -- mstr
   SIGNAL fifo_wr             : std_logic;
   SIGNAL mstr_fifo_rd        : std_logic;
   SIGNAL set_dma_err_int     : std_logic;
   SIGNAL clr_dma_en_int      : std_logic;

   -- du
   SIGNAL dma_dest_adr        : std_logic_vector(31 DOWNTO 2);
   SIGNAL dma_sour_adr        : std_logic_vector(31 DOWNTO 2);
   SIGNAL dma_sour_device     : std_logic_vector(2 DOWNTO 0);
   SIGNAL dma_dest_device     : std_logic_vector(2 DOWNTO 0);
   SIGNAL noinc_sour          : std_logic;
   SIGNAL noinc_dest          : std_logic;
   SIGNAL dma_size            : std_logic_vector(15 DOWNTO 0);
   SIGNAL start_dma           : std_logic;
   SIGNAL dma_en              : std_logic;

   -- au
   SIGNAL reached_size        : std_logic;
   SIGNAL dma_act_bd_int      : std_logic_vector(3 DOWNTO 0);
   SIGNAL boundary            : std_logic;
   SIGNAL almost_boundary     : std_logic;

   -- fifo
   signal fifo_rd      : std_logic;
   signal fifo_dat_out : std_logic_vector(31 downto 0);

   -- MBLT Boost
   signal boost_en      : std_logic;
   signal boost_active  : std_logic;
   signal boost_fifo_rd : std_logic;

BEGIN
   clr_dma_en <= clr_dma_en_int;
   set_dma_err <= set_dma_err_int;
   dma_act_bd <= dma_act_bd_int;
   start_dma <= dma_sta(8);
   dbram_addr_o <= dma_act_bd_int;

   dma_en <= dma_sta(0);
   irq_o  <= dma_sta(2);

   cycle_end_o <= reached_size or almost_boundary or boundary or fifo_full;

dma_mstr: entity work.vme_dma_mstr
PORT MAP (
   rst                  => rst,
   clk                  => clk,

   stb_o                => stb_o,
   cyc_o_pci            => cyc_o_pci,
   cyc_o_vme            => cyc_o_vme,
   adr_o                => adr_o,
   sel_o                => sel_o,
   tga_o                => tga_o,
   we_o                 => we_o,
   ack_i                => ack_i,
   err_i                => err_i,
   cti_o                => cti_o,

   fifo_empty           => fifo_empty,
   fifo_full            => fifo_full,
   fifo_almost_full     => fifo_almost_full,
   fifo_almost_empty    => fifo_almost_empty,
   fifo_wr              => fifo_wr,
   fifo_rd              => mstr_fifo_rd,

   dma_act_bd           => dma_act_bd_int,
   start_dma            => start_dma,
   set_dma_err          => set_dma_err_int,
   clr_dma_en           => clr_dma_en_int,
   dbram_data_i         => dbram_data_i,
   dma_dest_adr         => dma_dest_adr,
   dma_sour_adr         => dma_sour_adr,
   dma_sour_device_o    => dma_sour_device,
   dma_dest_device_o    => dma_dest_device,
   dma_en               => dma_en,
   noinc_sour_o         => noinc_sour,
   noinc_dest_o         => noinc_dest,
   dma_size             => dma_size,
   boundary_o           => boundary,
   almost_boundary_o    => almost_boundary,
   reached_size_o       => reached_size,
   boost_en_o           => boost_en,
   boost_active_i       => boost_active
   );

dma_fifo: entity work.vme_dma_fifo
PORT MAP (
   rst                  => rst,
   clk                  => clk,
   fifo_clr             => start_dma,
   fifo_wr              => fifo_wr,
   fifo_rd              => fifo_rd,
   fifo_dat_i           => mstr_dat_i,
   fifo_dat_o           => fifo_dat_out,
   fifo_almost_full     => fifo_almost_full ,
   fifo_almost_empty    => fifo_almost_empty,
   fifo_full            => fifo_full,
   fifo_empty           => fifo_empty
     );

     mstr_dat_o <= fifo_dat_out;

     fifo_rd <= boost_fifo_rd when (boost_active = '1') else
                mstr_fifo_rd;

MBLT_BOOST: entity work.dma_mblt_boost
  port map (
    rst_i              => rst,
    clk_i              => clk,
    en_i               => boost_en,
    active_o           => boost_active,
    dma_dest_adr_i     => dma_dest_adr,
    dma_reached_size_i => reached_size,
    dma_boundary_i     => boundary,
    -- dma_fifo interface
    fifo_rd_o         => boost_fifo_rd,
    fifo_dat_i        => fifo_dat_out,
    fifo_almost_empty => fifo_almost_empty,
    fifo_almost_full  => fifo_almost_full,
    fifo_empty        => fifo_empty,
    fifo_full         => fifo_full,
    -- dedicated PCI wb-master
    pci_cyc_o     =>  pci_cyc_o,
    pci_stb_o     =>  pci_stb_o,
    pci_we_o      =>  pci_we_o,
    pci_sel_o     =>  pci_sel_o,
    pci_tga_o     =>  pci_tga_o,
    pci_cti_o     =>  pci_cti_o,
    pci_adr_o     =>  pci_adr_o,
    pci_dat_o     =>  pci_dat_o,
    pci_dat_i     =>  pci_dat_i,
    pci_ack_i     =>  pci_ack_i,
    pci_err_i     =>  pci_err_i);

END vme_dma_arch;
