-- SPDX-FileCopyrightText: 2016, MEN Mikro Elektronik GmbH
-- SPDX-License-Identifier: CERN-OHL-S-2.0+

--------------------------------------------------------------------------------
-- Title         : FIFO for DMA
-- Project       : 16z002-01
--------------------------------------------------------------------------------
-- File          : vme_dma_fifo.vhd
-- Author        : michael.miehling@men.de
-- Organization  : MEN Mikro Elektronik GmbH
-- Created       : 18/09/03
--------------------------------------------------------------------------------
-- Simulator     : Modelsim PE 6.6
-- Synthesis     : Quartus 15.1
--------------------------------------------------------------------------------
-- Description :
--
-- This module consists of a fifo 256 x 32bit with logic.
-- A almost full and almost empty bit are generated.
--------------------------------------------------------------------------------
-- Hierarchy:
--
-- wbb2vme
--    vme_dma
--       vme_dma_fifo
--          fifo_256x32bit
--------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------
-- History:
--------------------------------------------------------------------------------
-- $Revision: 1.1 $
--
-- $Log: vme_dma_fifo.vhd,v $
-- Revision 1.1  2012/03/29 10:14:43  MMiehling
-- Initial Revision
--
-- Revision 1.5  2006/05/18 14:02:24  MMiehling
-- changed fifo depth from 16 to 64
--
-- Revision 1.1  2005/10/28 17:52:25  mmiehling
-- Initial Revision
--
-- Revision 1.4  2004/11/02 11:19:41  mmiehling
-- changed sclr to aclr
--
-- Revision 1.3  2004/08/13 15:41:14  mmiehling
-- removed dma-slave and improved timing
--
-- Revision 1.2  2004/07/27 17:23:24  mmiehling
-- removed slave port
--
-- Revision 1.1  2004/07/15 09:28:51  MMiehling
-- Initial Revision
--
--------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use ieee.numeric_std.all;

ENTITY vme_dma_fifo IS
PORT (
   rst               : IN std_logic;
   clk               : IN std_logic;

   fifo_clr            : IN std_logic;
   fifo_wr            : IN std_logic;
   fifo_rd            : IN std_logic;
   fifo_dat_i         : IN std_logic_vector(31 DOWNTO 0);
   fifo_dat_o         : OUT std_logic_vector(31 DOWNTO 0);
   fifo_almost_full   : OUT std_logic;                      -- two words can be written before fifo is full
   fifo_almost_empty  : OUT std_logic;                      -- one word is in fifo before empty
   fifo_full          : OUT std_logic;                      -- fifo is full
   fifo_empty         : OUT std_logic                       -- fifo is empty
     );
END vme_dma_fifo;

ARCHITECTURE vme_dma_fifo_arch OF vme_dma_fifo IS
   constant c_pointer_width  : integer := 9;
   constant CONST_FIFO_SIZE  : integer := 2**c_pointer_width;
   signal   rd_ptr, rd_ptr_m1, wr_ptr : unsigned(c_pointer_width-1 downto 0);
   SIGNAL fifo_usedw    : unsigned(c_pointer_width DOWNTO 0);
   signal rst_n : std_logic;
BEGIN
  rst_n <= not rst;

  -- Cannot use the generic fifo, because we need all the entries
  -- (not available if  the empty/full outputs are registered).
  -- Also, the logic for almost_full/almost_empty needs to be different.
  inst_ram : entity work.generic_dpram
  generic map (
    g_data_width               => fifo_dat_i'length,
    g_size                     => CONST_FIFO_SIZE,
    g_with_byte_enable         => false,
    g_addr_conflict_resolution => "dont_care",
    g_dual_clock               => false,
    g_implementation_hint      => open)
  port map (
    rst_n_i => rst_n,
    clka_i  => clk,
    wea_i   => fifo_wr,
    aa_i    => std_logic_vector(wr_ptr(c_pointer_width-1 downto 0)),
    da_i    => fifo_dat_i,
    qa_o    => open,
    clkb_i  => '0',
    ab_i    => std_logic_vector(rd_ptr_m1(c_pointer_width-1 downto 0)),
    db_i    => (others => '0'),
    qb_o    => fifo_dat_o,
    bwea_i  => "1111",
    bweb_i  => "0000",
    web_i   => '0');

  --  Ensure no show ahead
  rd_ptr_m1 <= rd_ptr when fifo_rd = '1' else rd_ptr - 1;

  p_pointers : process(clk)
  begin
    if rising_edge(clk) then
      if rst_n = '0' then
        wr_ptr <= (others => '0');
        rd_ptr <= (others => '0');
        fifo_usedw <= (others => '0');
      else
        if fifo_wr = '1' then
          wr_ptr <= wr_ptr + 1;
        end if;

        if fifo_rd = '1' then
          rd_ptr <= rd_ptr + 1;
        end if;

        if fifo_wr = '1' and fifo_rd = '0' then
          assert fifo_usedw < CONST_FIFO_SIZE report "fifo overflow" severity error;
          fifo_usedw <= fifo_usedw + 1;
        elsif fifo_wr = '0' and fifo_rd = '1' then
          assert fifo_usedw > 0 report "fifo underflow" severity error;
          fifo_usedw <= fifo_usedw - 1;
        end if;
      end if;
    end if;
  end process;

  --  Note: the full/empty/almost_full/almost_empty logic is slightly
  --  different from the one of the generic_sync_fifo.
  PROCESS(clk, rst)
   BEGIN
      IF rst = '1' THEN
         fifo_almost_full <= '0';
         fifo_full <= '0';
         fifo_empty <= '1';
         fifo_almost_empty <= '0';
      ELSIF clk'EVENT AND clk = '1' THEN
         -- indicate whether two words can be written to fifo before full
         IF fifo_usedw = CONST_FIFO_SIZE-3 AND fifo_wr = '1' and fifo_rd = '0' THEN
            fifo_almost_full <= '1';
         ELSIF fifo_rd = '1' and fifo_wr = '0' THEN
            fifo_almost_full <= '0';
         END IF;

         -- indicate whether fifo is full
         IF fifo_usedw = CONST_FIFO_SIZE-2 AND fifo_wr = '1' and fifo_rd = '0' THEN
            fifo_full <= '1';
         ELSIF fifo_rd = '1' and fifo_wr = '0' THEN
            fifo_full <= '0';
         END IF;

         -- indicate whether fifo is empty
         IF fifo_usedw = 1 AND fifo_rd = '1' and fifo_wr = '0' THEN
            fifo_empty <= '1';
         ELSIF fifo_wr = '1' and fifo_rd = '0' THEN
            fifo_empty <= '0';
         END IF;

         -- indicate whether one word can be read before empty
         IF fifo_usedw = 2 AND fifo_rd = '1' and fifo_wr = '0' THEN
            fifo_almost_empty <= '1';
         ELSIF fifo_usedw = 0 AND fifo_wr = '1' and fifo_rd = '0' THEN  -- if fifo is empty an one word gets written
            fifo_almost_empty <= '1';
         ELSIF fifo_wr = '1' OR fifo_rd = '1' THEN
            fifo_almost_empty <= '0';
         END IF;

      END IF;
   end process;
END vme_dma_fifo_arch;
