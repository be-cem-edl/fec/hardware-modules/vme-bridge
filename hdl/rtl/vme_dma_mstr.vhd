-- SPDX-FileCopyrightText: 2016, MEN Mikro Elektronik GmbH
-- SPDX-License-Identifier: CERN-OHL-S-2.0+

--------------------------------------------------------------------------------
-- Title         : DMA Master FSM
-- Project       : 16z002-01
--------------------------------------------------------------------------------
-- File          : vme_dma_mstr.vhd
-- Author        : michael.miehling@men.de
-- Organization  : MEN Mikro Elektronik GmbH
-- Created       : 18/09/03
--------------------------------------------------------------------------------
-- Simulator     : Modelsim PE 6.6
-- Synthesis     : Quartus 15.1
--------------------------------------------------------------------------------
-- Description :
--
-- This module consists of the main fsm for the dma.
-- It handles all actions which are required for dma
-- transmissions, including the wbm control signals.
--------------------------------------------------------------------------------
-- Hierarchy:
--
-- wbb2vme
--    vme_dma
--       vme_dma_mstr
--------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------
-- History:
--------------------------------------------------------------------------------
-- $Revision: 1.4 $
--
-- $Log: vme_dma_mstr.vhd,v $
-- Revision 1.4  2013/01/24 12:47:16  MMiehling
-- bugfix: termination by clearing dma_en bit interrupted wbb access => now wait until next ack is set and then terminate dma access
--
-- Revision 1.3  2012/09/25 11:21:45  MMiehling
-- added wbm_err signal for error signalling from pcie to vme
--
-- Revision 1.2  2012/08/27 12:57:16  MMiehling
-- added prep_write2/3 states for correct fifo handling
--
-- Revision 1.1  2012/03/29 10:14:42  MMiehling
-- Initial Revision
--
-- Revision 1.5  2006/05/18 14:02:26  MMiehling
-- changed comment
--
-- Revision 1.1  2005/10/28 17:52:26  mmiehling
-- Initial Revision
--
-- Revision 1.4  2004/11/02 11:19:43  mmiehling
-- improved timing
--
-- Revision 1.3  2004/08/13 15:41:16  mmiehling
-- removed dma-slave and improved timing
--
-- Revision 1.2  2004/07/27 17:23:26  mmiehling
-- removed slave port
--
-- Revision 1.1  2004/07/15 09:28:52  MMiehling
-- Initial Revision
--
--------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.all;

use work.vme_pkg.all;

ENTITY vme_dma_mstr IS
PORT (
   rst                  : IN std_logic;
   clk                  : IN std_logic;

   -- wb_master_bus
   stb_o                : OUT std_logic;                 -- request for wb_mstr_bus
   adr_o                : OUT std_logic_vector(31 DOWNTO 0);      -- adress for wb-bus
   sel_o                : OUT std_logic_vector(3 DOWNTO 0);       -- byte enables for wb_bus
   we_o                 : OUT std_logic;                          -- write/read
   tga_o                : OUT std_logic_vector(8 DOWNTO 0);       -- type of dma
   cyc_o_pci            : OUT std_logic;                          -- chip select for pci
   cyc_o_vme            : OUT std_logic;                          -- chip select for vme
   ack_i                : IN std_logic;                  -- acknoledge from wb_mstr_bus
   err_i                : IN std_logic;                  -- error answer from slave
   cti_o                : OUT std_logic_vector(2 DOWNTO 0);

   -- fifo
   fifo_empty           : IN std_logic;                  -- indicates that no more data is available
   fifo_full            : in std_logic;                  -- indicates that no more data can be stored in fifo
   fifo_almost_full     : IN std_logic;                  -- indicates that only one data can be stored in the fifo
   fifo_almost_empty    : IN std_logic;                  -- indicates that only one data is stored in the fifo
   fifo_wr              : OUT std_logic;                 -- if asserted, fifo will be filled with another data
   fifo_rd              : OUT std_logic;                 -- if asserted, data will be read out from fifo

   -- vme_dma
   start_dma            : IN std_logic;                  -- flag starts dma-fsm and clears counters
   dma_act_bd           : OUT std_logic_vector(3 DOWNTO 0);       -- [7:3] = active bd number
   set_dma_err          : OUT std_logic;                 -- sets dma error bit if vme error
   clr_dma_en           : OUT std_logic;                 -- clears dma_en bit and dma_act_bd if dma_mstr has done
   dma_en               : IN std_logic;                  -- starts dma_mstr, if 0 => clears dma_act_bd counter

   dbram_data_i         : IN std_logic_vector(127 downto 0);

   --  For booster
   dma_dest_adr         : OUT std_logic_vector(31 DOWNTO 2);      -- active bd destination adress
   dma_sour_adr         : OUT std_logic_vector(31 DOWNTO 2);      -- active bd source adress
   dma_sour_device_o    : OUT std_logic_vector(2 DOWNTO 0);       -- selects the source device
   dma_dest_device_o    : OUT std_logic_vector(2 DOWNTO 0);       -- selects the destination device
   noinc_sour_o         : OUT std_logic;                          -- indicates if source adress should be incremented
   noinc_dest_o         : OUT std_logic;                          -- indicates if destination adress should be incremented
   dma_size             : OUT std_logic_vector(15 DOWNTO 0);      -- size of data package
                                                                  -- when dma_err will be cleared

   boundary_o           : OUT std_logic;
   almost_boundary_o    : OUT std_logic;
   reached_size_o       : OUT std_logic;

   -- mblt_boost
   boost_en_o     : out std_logic;
   boost_active_i : in std_logic
   );
END vme_dma_mstr;

ARCHITECTURE vme_dma_mstr_arch OF vme_dma_mstr IS
   TYPE   mstr_states IS (idle, read_db, prep_read, prep_read2, read_data, read_ws, prep_write, prep_write2, prep_write3, write_data, write_ws, prep_read_bd, wait_boost);

   SIGNAL wr_d                : std_logic;
   SIGNAL dma_dest_adr_cur    : std_logic_vector(31 DOWNTO 2);
   SIGNAL dma_dest_adr_inc    : std_logic_vector(31 DOWNTO 2);
   SIGNAL dma_dest_adr_reg    : std_logic_vector(31 DOWNTO 2);
   SIGNAL dma_sour_adr_cur    : std_logic_vector(31 DOWNTO 2);
   SIGNAL dma_sour_adr_inc    : std_logic_vector(31 DOWNTO 2);
   SIGNAL dma_sour_adr_reg    : std_logic_vector(31 DOWNTO 2);
   SIGNAL dma_size_reg        : std_logic_vector(15 DOWNTO 0);
   SIGNAL act_bd_conf_int     : std_logic_vector(31 DOWNTO 0);
   signal noinc_sour          : std_logic;
   signal noinc_dest          : std_logic;
   signal dma_null            : std_logic;
   signal dma_sour_device     : std_logic_vector(2 DOWNTO 0);       -- selects the source device
   signal dma_dest_device     : std_logic_vector(2 DOWNTO 0);       -- selects the destination device
   signal use_boost           : std_logic;

   signal blk_sgl             : std_logic;
   signal dma_vme_am          : std_logic_vector(4 DOWNTO 0);       -- type of dma transmission


   SIGNAL blk_int                   : std_logic;
   SIGNAL dma_size_inc              : std_logic_vector(15 DOWNTO 0);
   SIGNAL dbram_sour_adr            : std_logic_vector(31 DOWNTO 2);
   SIGNAL dbram_dest_adr            : std_logic_vector(31 DOWNTO 2);
   SIGNAL cyc_o_pci_int             : std_logic;
   SIGNAL cyc_o_vme_int             : std_logic;
   SIGNAL n_sour_adr_out            : std_logic_vector(31 DOWNTO 2);
   SIGNAL n_dest_adr_out            : std_logic_vector(31 DOWNTO 2);
   SIGNAL reached_size_int          : std_logic;
   SIGNAL almost_reached_size_int   : std_logic;
   SIGNAL boundary_blt              : std_logic;
   SIGNAL boundary_mblt             : std_logic;
   SIGNAL almost_boundary_blt       : std_logic;
   SIGNAL almost_boundary_mblt      : std_logic;
   signal tga_int                   : std_logic_vector(8 DOWNTO 0);
   signal dma_vme_am_conv           : std_logic_vector(1 DOWNTO 0);
   signal we_int                    : std_logic;

      -- vme_dma_au
   signal inc_adr              :  std_logic;                 -- flag indicates when adr should be incremented (depend on sour_dest and get_bd_int)
   signal reached_size         : std_logic;                  -- if all data from one bd was read and stored in the fifo
   signal boundary             : std_logic;                  -- indicates 256 byte boundary if D16 or D32 burst
   signal almost_boundary      : std_logic;                  -- indicates 256 byte boundary if D16 or D32 burst
   signal almost_reached_size  : std_logic;                  -- if all data from one bd was read and stored in the fifo

   type reg_type is record
      mstr_state : mstr_states;
      load_cnt : std_logic;
      sour_dest : std_logic;
      get_bd_int : std_logic;
      dma_act_bd_int : std_logic_vector(3 DOWNTO 0);
      dma_size       : std_logic_vector(15 DOWNTO 0);
      clr_dma_en_int : std_logic;
      stb_int : std_logic;
      cti_int : std_logic_vector(2 downto 0);
      adr_out : std_logic_vector(31 downto 2);
      boost_en_o : std_logic;
   end record;

   signal regs, nregs : reg_type;
BEGIN
  -- detect when boost can be used, for now it's only for MBLT reads
  use_boost <= '1' when (dma_dest_device = c_DMA_PCI_DEV) else
               '0';

   clr_dma_en <= regs.clr_dma_en_int;

   fifo_wr <= '1' WHEN we_int = '0' AND ack_i = '1' ELSE '0';

   set_dma_err <= '0' WHEN regs.clr_dma_en_int = '1' AND fifo_empty = '1' AND reached_size = '1' AND dma_null = '1' ELSE regs.clr_dma_en_int;

   dma_act_bd <= regs.dma_act_bd_int;

   dma_dest_adr      <= dma_dest_adr_cur(31 DOWNTO 2);
   dma_sour_adr      <= dma_sour_adr_cur(31 DOWNTO 2);
   dma_size          <= dma_size_reg;

   dma_sour_device   <= act_bd_conf_int(18 DOWNTO 16);
   dma_dest_device   <= act_bd_conf_int(14 DOWNTO 12);

   dma_sour_device_o <= dma_sour_device;
   dma_dest_device_o <= dma_dest_device;

   dma_vme_am        <= act_bd_conf_int(8 DOWNTO 4);
   blk_sgl           <= '1' WHEN act_bd_conf_int(7 DOWNTO 4) = "0001" or act_bd_conf_int(7 DOWNTO 4) = "0101" ELSE   -- A16 does not provide block mode => always single will be selected
                        '0' WHEN act_bd_conf_int(7 DOWNTO 4) = "1100" ELSE                                           -- A24D64 does not provide single mode => always block will be selected
                        '0' WHEN act_bd_conf_int(7 DOWNTO 4) = "1110" ELSE                                           -- A32D64 does not provide single mode => always block will be selected
                        act_bd_conf_int(3);

   noinc_sour        <= act_bd_conf_int(2);
   noinc_dest        <= act_bd_conf_int(1);
   dma_null          <= act_bd_conf_int(0);
   noinc_sour_o <= noinc_sour;
   noinc_dest_o <= noinc_dest;

outdec : PROCESS(clk, rst)
  BEGIN
    IF rst = '1' THEN
       wr_d <= '0';
    ELSIF clk'EVENT AND clk = '1' THEN
       wr_d <= regs.get_bd_int;
    END IF;
  END PROCESS outdec;

  dbram_dest_adr <= dbram_data_i(31 DOWNTO 0 + 2);
  dbram_sour_adr <= dbram_data_i(63 downto 32 + 2);

  dma_dest_adr_inc <= std_logic_vector(unsigned(dma_dest_adr_cur) + 1);
  dma_sour_adr_inc <= std_logic_vector(unsigned(dma_sour_adr_cur) + 1);
  dma_size_inc <= std_logic_vector(unsigned(regs.dma_size) + 1);

  --  Next tfr address (depends on noinc)
  n_sour_adr_out <= dma_sour_adr_reg when noinc_sour = '1' else dma_sour_adr_cur;
  n_dest_adr_out <= dma_dest_adr_reg when noinc_dest = '1' else dma_dest_adr_cur;

  adr_o(31 downto 2) <= regs.adr_out;
  adr_o(1 DOWNTO 0) <= "00";

-- dma_dest_adr_cur 0x48
dest : PROCESS(clk, rst)
  BEGIN
     IF rst = '1' THEN
       dma_size_reg <= (OTHERS => '0');
       act_bd_conf_int <= (OTHERS => '0');
     ELSIF clk'EVENT AND clk = '1' THEN
       IF wr_d = '1' then
         dma_size_reg(15 DOWNTO 0)     <= dbram_data_i(79 downto 64);
         act_bd_conf_int(31 DOWNTO 0)  <= dbram_data_i(127 downto 96);
      END IF;
   END IF;
  END PROCESS dest;

fsm_ff : PROCESS (clk, rst)
BEGIN
   IF rst = '1' THEN
      regs <= (mstr_state => idle,
               load_cnt => '0',
               sour_dest => '0',
               get_bd_int => '0',
               dma_act_bd_int => (others => '0'),
               dma_size => (others => '0'),
               clr_dma_en_int => '0',
               stb_int => '0',
               cti_int => "000",
               adr_out => (others => '0'),
               boost_en_o => '0');
   ELSIF clk'EVENT AND clk = '1' THEN
      regs <= nregs;
   end if;
end process;

mstr_fsm : PROCESS (regs, start_dma, dma_null, noinc_dest, err_i, dma_en, ack_i,
                    n_sour_adr_out, n_dest_adr_out, dma_size_inc,
                    fifo_full, fifo_almost_full, fifo_empty, fifo_almost_empty, 
                    almost_reached_size, reached_size, almost_boundary, boundary,
                    use_boost, boost_active_i)
BEGIN
   nregs <= regs;

   nregs.load_cnt <= '0';
   nregs.get_bd_int <= '0';
   nregs.stb_int <= '0';
   nregs.clr_dma_en_int <= '0';
   nregs.boost_en_o <= '0';
   inc_adr  <= '0';
   fifo_rd  <= '0';


      CASE regs.mstr_state IS
         WHEN idle =>
            nregs.sour_dest      <= '0';
            nregs.cti_int <= "000";
            nregs.dma_act_bd_int <= (others => '0');
            IF start_dma = '1' THEN                      -- if start of dma => read first bd
               nregs.mstr_state     <= read_db;
               nregs.get_bd_int     <= '1';
            ELSE
               nregs.mstr_state     <= idle;
            END IF;

         WHEN read_db =>
            nregs.sour_dest      <= '1';
            nregs.mstr_state     <= prep_read;
            nregs.cti_int <= "000";                         -- no burst if address gets not incremented
            --  TODO: load descr, set almost_reached_size if 0.
            nregs.load_cnt <= '1';
            nregs.dma_size <= (OTHERS => '0');

         WHEN prep_read =>
            nregs.sour_dest      <= '1';
            nregs.mstr_state     <= prep_read2;
            nregs.cti_int <= "000";                         -- no burst if address gets not incremented

         WHEN prep_read2 =>
            nregs.sour_dest      <= '1';
            nregs.mstr_state     <= read_data;
            nregs.stb_int <= '1';
            nregs.adr_out <= n_sour_adr_out;
            inc_adr  <= '1';                                   -- if not first read then inc because of reached size
            nregs.dma_size <= dma_size_inc;

            if noinc_dest = '0' and (almost_reached_size = '1' or reached_size = '1') then
               nregs.cti_int <= "000";                         -- last longword => perform single access
            elsif noinc_dest = '0' and almost_boundary = '1' then
               nregs.cti_int <= "000";                         -- first longword before boundary => perform single access
            elsif noinc_dest = '0' and almost_reached_size = '0' then
               nregs.cti_int <= "010";                         -- more than one longword => perform burst access
            else
               nregs.cti_int <= "000";                         -- no burst if address gets not incremented
            end if;

         WHEN read_data =>                               -- request read from source address
            IF err_i = '1' OR (dma_en /= '1' AND ack_i = '1') THEN
               nregs.mstr_state   <= idle;                     -- error from source => end of dma acti_inton
               nregs.sour_dest <= '0';
               nregs.clr_dma_en_int <= '1';
               nregs.cti_int <= "000";
            elsif ack_i = '1' then
               if reached_size = '1' or fifo_full = '1' THEN
                  --  Cannot read more, has to write.
                  if use_boost = '1' then
                     nregs.mstr_state <= wait_boost;
                  else
                     nregs.mstr_state <= prep_write;             -- block of data was read => write data to destination
                  end if;
                     nregs.sour_dest      <= '0';
                     nregs.cti_int        <= "000";
            --GD
--            ELSIF noinc_sour = '1' THEN
--               mstr_state     <= read_ws;                -- got ack from source address => waitstate, then new single cycle
--               sour_dest      <= '1';
--               cti_int <= "000";
            --GD
               ELSE
                  inc_adr <= '1';
                  nregs.adr_out <= n_sour_adr_out;
                  nregs.dma_size <= dma_size_inc;

                  IF boundary = '1' THEN
                     --  That was the last beat (TODO: only for VME ?)
                     --  But can continue to read (do not set stb).
                     nregs.mstr_state     <= read_ws;                -- got ack from source address => waitstate, then new single cycle
                     nregs.sour_dest      <= '1';
                     nregs.cti_int <= "000";
                  ELSIF fifo_almost_full = '1' or almost_reached_size = '1' or almost_boundary = '1' THEN
                     --  Next beat is the last beat of the burst, adjust CTI.
                     nregs.mstr_state     <= read_data;
                     nregs.sour_dest      <= '1';
                     nregs.stb_int <= '1';
                     if regs.cti_int = "010" then
                        nregs.cti_int <= "111";                         -- do last data phase of burst
                     else
                        nregs.cti_int <= "000";                         -- if there was no burst, perform last single access
                     end if;
                  else
                     --  Continue reading
                     nregs.stb_int <= '1';
                  end if;
               end if;
            ELSE
               --  Wait for the ack.
               nregs.mstr_state     <= read_data;              -- wait on ack_i even if fifo_almost_full or reached_size
               nregs.sour_dest      <= '1';
               nregs.stb_int <= '1';
               nregs.cti_int <= regs.cti_int;
            END IF;

            --- enable boost for MBLT read to PCIe space
            nregs.boost_en_o <= use_boost;

         WHEN read_ws =>
            nregs.sour_dest      <= '1';
            nregs.mstr_state     <= read_data;
            nregs.stb_int <= '1';
            if noinc_dest = '0' and (reached_size = '1' or fifo_almost_full = '1') then
               nregs.cti_int <= "000";                         -- last longword => perform single access
            elsif noinc_dest = '0' and reached_size = '0' then
               nregs.cti_int <= "010";                         -- more than one longword => perform burst access
            else
               nregs.cti_int <= "000";                         -- no burst if address gets not incremented
            end if;

         -----------------------------------------------------
         when wait_boost =>
            if boost_active_i = '0' and reached_size = '1' and dma_null = '1' then
               -- boost done, we're done with current descriptor and there are no more descriptors
               nregs.sour_dest      <= '0';
               nregs.mstr_state     <= idle;  -- data of bd was written and end of bd list => dma finished
               nregs.clr_dma_en_int <= '1';   -- end of dma => clear dma_en bit
               nregs.cti_int        <= "000";
            elsif boost_active_i = '0' and reached_size = '1' then
               -- boost done, we're done with current descriptor, but there are more to be processed
               nregs.sour_dest      <= '0';
               nregs.mstr_state     <= prep_read_bd;           -- data of bd was written => read next bd
               nregs.cti_int <= "000";
            elsif boost_active_i = '0' and reached_size = '0' then
               nregs.sour_dest      <= '1';
               nregs.mstr_state     <= prep_read;              -- part data of bd was written => read next part of same bd
               nregs.cti_int <= "000";
            elsif boost_active_i = '0' then
               -- error occured ?
               nregs.sour_dest      <= '0';
               nregs.mstr_state     <= idle;                   -- error from destination => end of dma action
               nregs.clr_dma_en_int <= '1';
               nregs.cti_int <= "000";
            else
               nregs.sour_dest      <= '0';
               nregs.mstr_state     <= wait_boost;
            end if;

         -----------------------------------------------------

         WHEN prep_write =>
            nregs.sour_dest      <= '0';
            nregs.mstr_state     <= prep_write2;
            nregs.cti_int <= "000";

         WHEN prep_write2 =>
            nregs.sour_dest      <= '0';
            nregs.mstr_state     <= prep_write3;
            nregs.cti_int <= "000";

         WHEN prep_write3 =>
            inc_adr  <= '1';
            nregs.adr_out <= n_dest_adr_out;
            fifo_rd  <= '1';                                   -- prepare for first write

            nregs.sour_dest      <= '0';
            nregs.mstr_state     <= write_data;
            nregs.stb_int <= '1';
            if noinc_dest = '0' and fifo_almost_empty = '1' then
               nregs.cti_int <= "000";                         -- last longword => perform single access
            elsif noinc_dest = '0' and almost_boundary = '1' then
               nregs.cti_int <= "000";                         -- first longword before boundary => perform single access
            elsif noinc_dest = '0' and (fifo_almost_empty = '0' and almost_boundary = '0') then
               nregs.cti_int <= "010";                         -- more than one longword => perform burst access
            else
               nregs.cti_int <= "000";                         -- no burst if address gets not incremented
            end if;

         WHEN write_data =>
            IF err_i = '1' OR (dma_en /= '1' AND ack_i = '1') THEN
               nregs.sour_dest      <= '0';
               nregs.mstr_state     <= idle;                   -- error from destination => end of dma action
               nregs.clr_dma_en_int <= '1';
               nregs.cti_int <= "000";
            ELSIF ack_i = '1' then
               IF fifo_empty = '1' AND reached_size = '1' THEN
                  nregs.sour_dest      <= '0';
                  if dma_null = '1' then
                     --  End of all the descriptors. Done.
                     nregs.mstr_state     <= idle;                   -- data of bd was written and end of bd list => dma finished
                     nregs.clr_dma_en_int <= '1';                    -- end of dma => clear dma_en bit
                  else
                     nregs.mstr_state     <= prep_read_bd;           -- data of bd was written => read next bd
                  end if;
                  nregs.cti_int <= "000";
               ELSIF fifo_empty = '1' THEN
                  assert reached_size = '0';
                  --  Need to fill the fifo.
                  nregs.sour_dest      <= '1';
                  nregs.mstr_state     <= prep_read;              -- part data of bd was written => read next part of same bd
                  nregs.cti_int <= "000";
               ELSE
                  inc_adr  <= '1';                                -- read next data from fifo
                  nregs.adr_out <= n_dest_adr_out;

                  fifo_rd  <= '1';
                  IF boundary = '1' THEN
                     --  Need to stop the burst.
                     nregs.sour_dest      <= '0';
                     nregs.mstr_state     <= write_ws;               -- got ack from destination address => make waitstate and then next single cycle
                     nregs.cti_int <= "000";
                  ELSIF fifo_almost_empty = '1' or almost_boundary = '1' THEN
                     --  Start last beat, adjust CTI.
                     nregs.sour_dest      <= '0';
                     nregs.mstr_state     <= write_data;             -- got ack from destination address => write last data of burst
                     nregs.stb_int <= '1';
                     nregs.cti_int <= "111";
                  ELSE
                     --  continue writing.
                     nregs.stb_int <= '1';
                  end if;
               end if;
            ELSE
               nregs.sour_dest      <= '0';
               nregs.mstr_state     <= write_data;             -- wait on ack_i
               nregs.stb_int <= '1';
               nregs.cti_int <= regs.cti_int;
            END IF;

        WHEN write_ws =>
            nregs.sour_dest <= '0';
            nregs.mstr_state    <= write_data;
            nregs.stb_int <= '1';
            if noinc_dest = '0' and fifo_empty = '1' then
               nregs.cti_int <= "000";                         -- last longword => perform single access
            elsif noinc_dest = '0' and fifo_empty = '0' then
               nregs.cti_int <= "010";                         -- more than one longword => perform burst access
            else
               nregs.cti_int <= "000";                         -- no burst if address gets not incremented
            end if;

        WHEN prep_read_bd =>
            nregs.sour_dest <= '0';
            nregs.mstr_state    <= read_db;
            nregs.dma_act_bd_int <= std_logic_vector(unsigned(regs.dma_act_bd_int) + 1);
            nregs.get_bd_int <= '1';
            nregs.cti_int <= "000";

        WHEN OTHERS =>
            nregs.sour_dest <= '0';
            nregs.mstr_state    <= idle;
            nregs.cti_int <= "000";
      END CASE;
END PROCESS mstr_fsm;

cyc_o_pci   <= cyc_o_pci_int WHEN regs.stb_int = '1' ELSE '0';
cyc_o_vme   <= cyc_o_vme_int WHEN regs.stb_int = '1' ELSE '0';

-- perform VME block transfer when
-- 1. block transfer configured and access to source selected and source address shall be incremented
-- 2. block transfer configured and access to destination selected and destination address shall be incremented
-- 3. else single transfer
blk_int     <= '1' when blk_sgl = '0' and regs.sour_dest = '0' and noinc_sour = '0' else
               '1' when blk_sgl = '0' and regs.sour_dest = '1' and noinc_dest = '0' else
               '0';

reached_size_int <= '1' WHEN regs.dma_size = dma_size_reg ELSE '0';
almost_reached_size_int <= '1' WHEN (unsigned(regs.dma_size) + 1) = unsigned(dma_size_reg) ELSE '0';

boundary <= boundary_blt OR boundary_mblt;
almost_boundary <= almost_boundary_blt OR almost_boundary_mblt;

boundary_o <= boundary;
almost_boundary_o <= almost_boundary;
reached_size_o <= reached_size;

sel_o <= (OTHERS => '1');                         -- always longword accessess
we_o <= we_int;
stb_o <= regs.stb_int;
cti_o <= regs.cti_int;

boost_en_o <= regs.boost_en_o;

dma_vme_am_conv <=   "10" when dma_vme_am(1 DOWNTO 0) = "01" else    -- A32
                     "01" when dma_vme_am(1 DOWNTO 0) = "10" else    -- A16
                     "00";                                           -- A24

-- (1:0) : 00=A24, 01=A32, 10=A16
-- (3:2) : 00=D16, 01=D32, 10=D64
-- (4)   : if increment enabled the burst else single
-- (5)   : swapped(1) or non swapped (0)
-- (6)   : =0 always VME bus access (no register access)
-- (7)   : =1 indicates access to vme_ctrl by DMA
-- (8)   : 0= non-privileged 1= supervisory
tga_int <= dma_vme_am(4) & "10" & NOT dma_vme_am(0) & blk_int & dma_vme_am(3 DOWNTO 2) & dma_vme_am_conv;

adr_o_proc : PROCESS(clk, rst)
BEGIN
   IF rst = '1' THEN
      dma_sour_adr_cur <= (OTHERS => '0');
      dma_dest_adr_cur <= (OTHERS => '0');
      cyc_o_pci_int <= '0';
      cyc_o_vme_int <= '0';
      we_int <= '0';
      reached_size <= '0';
      almost_reached_size <= '0';
      tga_o <= (OTHERS => '0');
      boundary_blt    <= '0';
      boundary_mblt <= '0';
      almost_boundary_blt <= '0';
      almost_boundary_mblt <= '0';
   ELSIF clk'EVENT AND clk = '1' THEN
      -- rule of vmebus: do not cross 256 byte boundaries (0x100)
      IF dma_vme_am(3) = '0' AND ((dma_dest_device(1) = '1' AND dma_dest_adr_cur(7 DOWNTO 2) = "000000" AND regs.sour_dest = '0') OR
                                 (dma_sour_device(1) = '1' AND dma_sour_adr_cur(7 DOWNTO 2) = "000000" AND regs.sour_dest = '1')) THEN
         boundary_blt <= '1';
      ELSE
         boundary_blt <= '0';
      END IF;
      IF dma_vme_am(3) = '0' AND ((dma_dest_device(1) = '1' AND dma_dest_adr_cur(7 DOWNTO 2) = "111111" AND regs.sour_dest = '0') OR
                                 (dma_sour_device(1) = '1' AND dma_sour_adr_cur(7 DOWNTO 2) = "111111" AND regs.sour_dest = '1')) THEN
         almost_boundary_blt <= '1';
      ELSE
         almost_boundary_blt <= '0';
      END IF;

      -- for mblt-d64: do not cross 2k byte boundaries (0x800)
      IF dma_vme_am(3) = '1' AND ((dma_dest_device(1) = '1' AND dma_dest_adr_cur(10 DOWNTO 2) = "000000000" AND regs.sour_dest = '0') OR
                                 (dma_sour_device(1) = '1' AND dma_sour_adr_cur(10 DOWNTO 2) = "000000000" AND regs.sour_dest = '1')) THEN
         boundary_mblt <= '1';
      ELSE
         boundary_mblt <= '0';
      END IF;
      IF dma_vme_am(3) = '1' AND ((dma_dest_device(1) = '1' AND dma_dest_adr_cur(10 DOWNTO 2) = "111111111" AND regs.sour_dest = '0') OR
                                 (dma_sour_device(1) = '1' AND dma_sour_adr_cur(10 DOWNTO 2) = "111111111" AND regs.sour_dest = '1')) THEN
         almost_boundary_mblt <= '1';
      ELSE
         almost_boundary_mblt <= '0';
      END IF;

      IF regs.load_cnt = '1' THEN
         reached_size <= '0';
         if dma_size_reg = x"0000" then                -- if just one longword shall be transfered, indicate almost reached
            almost_reached_size <= '1';
         else
            almost_reached_size <= '0';
         end if;
      ELSIF inc_adr = '1' AND regs.sour_dest = '1' THEN
         reached_size <= reached_size_int;
         almost_reached_size <= almost_reached_size_int;
      END IF;


      IF regs.sour_dest = '1' THEN                           -- SOURCE
         cyc_o_vme_int <= dma_sour_device(1);
         cyc_o_pci_int <= dma_sour_device(2);
         we_int <= '0';                                    -- read from source
         if dma_sour_device(1) = '1' then                -- if access to vme range, use tga for space selection
            tga_o <= tga_int;
         else                                            -- if access to SRAM or PCI => no special tga setting
            tga_o <= (OTHERS => '0');
         end if;
      ELSE                                                -- DESTINATION
         cyc_o_vme_int <= dma_dest_device(1);
         cyc_o_pci_int <= dma_dest_device(2);
         we_int <= '1';                                    -- write to destination
         if dma_dest_device(1) = '1' then                -- if access to vme range, use tga for space selection
            tga_o <= tga_int;
         else                                            -- if access to SRAM or PCI => no special tga setting
            tga_o <= (OTHERS => '0');
         end if;
      END IF;

      --  Even in noinc mode, the address must be incremented to detect boundary cross.
      IF regs.load_cnt = '1' THEN
         dma_sour_adr_reg <= dbram_sour_adr;
         dma_sour_adr_cur <= dbram_sour_adr;
      ELSIF regs.sour_dest = '1' AND inc_adr = '1' THEN
         dma_sour_adr_cur <= dma_sour_adr_inc;
      END IF;

      --  Even in noinc mode, the address must be incremented to detect boundary cross.
      IF regs.load_cnt = '1' THEN
         dma_dest_adr_reg <= dbram_dest_adr;
         dma_dest_adr_cur <= dbram_dest_adr;
      ELSIF regs.sour_dest = '0' AND inc_adr = '1' THEN
         dma_dest_adr_cur <= dma_dest_adr_inc;
      END IF;

   END IF;
END PROCESS adr_o_proc;


END vme_dma_mstr_arch;
