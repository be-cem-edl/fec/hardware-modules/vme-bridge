-- SPDX-FileCopyrightText: 2016, MEN Mikro Elektronik GmbH
-- SPDX-License-Identifier: CERN-OHL-S-2.0+

--------------------------------------------------------------------------------
-- Title         : Data Unit of VME-Bridge
-- Project       : 16z002-01
--------------------------------------------------------------------------------
-- File          : vme_du.vhd
-- Author        : michael.miehling@men.de
-- Organization  : MEN Mikro Elektronik GmbH
-- Created       : 13/01/03
--------------------------------------------------------------------------------
-- Simulator     : Modelsim PE 6.6
-- Synthesis     : Quartus 15.1
--------------------------------------------------------------------------------
-- Description :
--
-- This unit handles the data path.
--------------------------------------------------------------------------------
-- Hierarchy:
--
-- vme_ctrl
--    vme_du
--------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------
-- History:
--------------------------------------------------------------------------------
-- $Revision: 1.8 $
--
-- $Log: vme_du.vhd,v $
-- Revision 1.8  2015/09/16 09:20:05  mwawrik
-- Added generic USE_LONGADD
--
-- Revision 1.7  2014/04/17 07:35:25  MMiehling
-- added generic LONGADD_SIZE
--
-- Revision 1.6  2013/09/12 08:45:23  mmiehling
-- support of address modifier supervisory, non-privileged data/program for A16, A24 and A32
--
-- Revision 1.5  2012/11/15 09:43:53  MMiehling
-- connected each interrupt source to interface in order to support edge triggered msi
--
-- Revision 1.4  2012/11/12 08:13:10  MMiehling
-- bugfix locmon: improved handling of adr(4:3) for stable results
--
-- Revision 1.3  2012/09/25 11:21:43  MMiehling
-- added wbm_err signal for error signalling from pcie to vme
--
-- Revision 1.2  2012/08/27 12:57:13  MMiehling
-- general rework of d64 slave access handling
-- rework of reset handling
--
-- Revision 1.1  2012/03/29 10:14:39  MMiehling
-- Initial Revision
--
-- Revision 1.11  2006/06/02 15:48:59  MMiehling
-- changed default of arbitration => now not fair is default
--
-- Revision 1.10  2005/02/04 13:44:17  mmiehling
-- added combinations of addr3+4
--
-- Revision 1.9  2004/11/02 11:29:58  mmiehling
-- improved timing and area
-- moved dma_reg to vme_du
--
-- Revision 1.8  2004/07/27 17:15:42  mmiehling
-- changed pci-core to 16z014
-- changed wishbone bus to wb_bus.vhd
-- added clk_trans_wb2wb.vhd
-- improved dma
--
-- Revision 1.7  2003/12/17 15:51:48  MMiehling
-- byte swapping in "not swapped" mode was wrong
--
-- Revision 1.6  2003/12/01 10:03:55  MMiehling
-- added d64
--
-- Revision 1.5  2003/07/14 08:38:10  MMiehling
-- changed mail_irq; added lwordn
--
-- Revision 1.4  2003/06/24 13:47:10  MMiehling
-- added rst_aonly; changed vme_data_in_reg sampling (lwordn)
--
-- Revision 1.3  2003/06/13 10:06:38  MMiehling
-- added address bits 3+4 for locmon;  changed locsta register
--
-- Revision 1.2  2003/04/22 11:03:02  MMiehling
-- changed irq and address map for locmon
--
-- Revision 1.1  2003/04/01 13:04:43  MMiehling
-- Initial Revision
--
--------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;          

ENTITY vme_du IS
PORT (
   clk                     : IN std_logic;                        -- 66 MHz
   rst                     : IN std_logic;                        -- global reset signal (asynch)

   -- arbiter
   sel_loc_data_out        : IN std_logic_vector(1 DOWNTO 0);     -- mux select signal for 0=reg, 1=vme data_out

   -- vme_au
   vme_adr_out             : IN std_logic_vector(31 DOWNTO 0);    -- vme adress lines
   byte_routing            : IN std_logic;                        -- mux select for byte routing
   vme_adr_in              : OUT std_logic_vector(31 DOWNTO 0);   -- vme adress input lines
   d64                     : IN std_logic;                        -- indicates d64 mblt

   -- sys_arbiter
   lwordn                  : IN std_logic;                        -- stored for vme slave access
   
   -- master
   second_word             : IN std_logic;                        -- indicates data phase of d64

   -- slave
   en_vme_data_out_reg     : IN std_logic;                        -- register enable for vme data out
   en_vme_data_out_reg_high: IN std_logic;                        -- register enable for vme data out high long
   en_vme_data_in_reg      : IN std_logic;                        -- register enable for vme data in
   en_vme_data_in_reg_high : IN std_logic;                        -- register enable for vme data in high long
   
   -- wbb_slave
   wbs_dat_o               : OUT std_logic_vector(31 DOWNTO 0);
   wbs_dat_i               : IN std_logic_vector(31 DOWNTO 0);
   swap                    : IN std_logic;                        -- swapps bytes when enabled
   
   --vme
   vd_o                    : OUT std_logic_vector(31 DOWNTO 0);
   vd_i                    : IN  std_logic_vector(31 DOWNTO 0);
   va_o                    : OUT std_logic_vector(31 DOWNTO 0);
   va_i                    : IN  std_logic_vector(31 DOWNTO 0)
   );
END vme_du;

ARCHITECTURE vme_du_arch OF vme_du IS 
   SIGNAL vme_data_in_reg_mux    : std_logic_vector(31 DOWNTO 0);
   SIGNAL vme_data_in_reg        : std_logic_vector(63 DOWNTO 0);
   SIGNAL vme_data_out_reg       : std_logic_vector(63 DOWNTO 0);
   SIGNAL vd_in_reg              : std_logic_vector(31 DOWNTO 0);
   SIGNAL vd_in_reg_int          : std_logic_vector(31 DOWNTO 0);
   SIGNAL va_in_reg              : std_logic_vector(31 DOWNTO 0);
   SIGNAL wbs_dat_o_reg          : std_logic_vector(31 DOWNTO 0);
   SIGNAL swap_byte_routing      : std_logic_vector(1 DOWNTO 0);
  
BEGIN
   vme_adr_in <= va_in_reg;
   
   -- swap = 1, byte_routing = 1      => 3210
   -- swap = 1, byte_routing = 0      => 1032
   -- byte_routing = 1                => 2301
   -- byte_routing = 0                => 0123

   swap_byte_routing <= swap & byte_routing;
   
PROCESS (vd_in_reg_int, swap_byte_routing)
   BEGIN
      CASE swap_byte_routing IS
         WHEN "01" => vme_data_in_reg_mux <= vd_in_reg_int(31 DOWNTO 0);
         WHEN "00" => vme_data_in_reg_mux <= vd_in_reg_int(15 DOWNTO 0) & vd_in_reg_int(31 DOWNTO 16);
         WHEN "11" => vme_data_in_reg_mux <= vd_in_reg_int(23 DOWNTO 16) & vd_in_reg_int(31 DOWNTO 24) & vd_in_reg_int(7 DOWNTO 0) & vd_in_reg_int(15 DOWNTO 8);
         WHEN "10" => vme_data_in_reg_mux <= vd_in_reg_int(7 DOWNTO 0) & vd_in_reg_int(15 DOWNTO 8) & vd_in_reg_int(23 DOWNTO 16) & vd_in_reg_int(31 DOWNTO 24);
         WHEN OTHERS => vme_data_in_reg_mux <= vd_in_reg_int(7 DOWNTO 0) & vd_in_reg_int(15 DOWNTO 8) & vd_in_reg_int(23 DOWNTO 16) & vd_in_reg_int(31 DOWNTO 24);
      END CASE;
   END PROCESS;                     
                     
   wbs_dat_o <= wbs_dat_o_reg;

   vd_in_reg_int <= va_in_reg WHEN d64 = '1' AND en_vme_data_in_reg_high = '1' ELSE vd_in_reg;
   
reg : PROCESS(clk, rst)
BEGIN
   IF rst = '1' THEN
      vd_in_reg <= (OTHERS => '0');
      vd_o <= (OTHERS => '0');
      vme_data_in_reg <= (OTHERS => '0');
      vme_data_out_reg <= (OTHERS => '0');
      va_o <= (OTHERS => '0');
      va_in_reg <= (OTHERS => '0');
      wbs_dat_o_reg <= (OTHERS => '0');
   ELSIF clk'EVENT and clk = '1' THEN
      -- synchronization registers
      vd_in_reg <= vd_i;
      
      IF swap = '1' AND d64 = '1' THEN
         -- swapping for d64: high and low 4 byte are swapped
         IF en_vme_data_in_reg_high = '1' THEN
            vme_data_in_reg(31 DOWNTO 0) <= va_in_reg(7 DOWNTO 0) & va_in_reg(15 DOWNTO 8) & va_in_reg(23 DOWNTO 16) & va_in_reg(31 DOWNTO 24);    
         END IF;
         IF en_vme_data_in_reg = '1' THEN
            vme_data_in_reg(63 DOWNTO 32) <= vd_in_reg(7 DOWNTO 0) & vd_in_reg(15 DOWNTO 8) & vd_in_reg(23 DOWNTO 16) & vd_in_reg(31 DOWNTO 24);  
         END IF;
      ELSE
         IF en_vme_data_in_reg = '1' AND (lwordn = '0' OR (lwordn = '1' AND byte_routing = '0')) THEN
            vme_data_in_reg(31 DOWNTO 16) <= vme_data_in_reg_mux(31 DOWNTO 16);   
         END IF;
         IF en_vme_data_in_reg = '1' AND (lwordn = '0' OR (lwordn = '1' AND byte_routing = '1')) THEN
            vme_data_in_reg(15 DOWNTO 0) <= vme_data_in_reg_mux(15 DOWNTO 0);   
         END IF;
         IF en_vme_data_in_reg_high = '1' THEN
            vme_data_in_reg(63 DOWNTO 32) <= vme_data_in_reg_mux(31 DOWNTO 0);
         END IF;
      END IF;        
      
      -- DATA output
      IF swap = '1' THEN
         IF d64 = '1' THEN                                 -- data phase for d64 mblt 7654
            vd_o <=  vme_data_out_reg(39 DOWNTO 32) & vme_data_out_reg(47 DOWNTO 40) & vme_data_out_reg(55 DOWNTO 48) & vme_data_out_reg(63 DOWNTO 56);
         ELSIF byte_routing = '1' THEN                                           -- data phase with byte routing 0123
            vd_o <= vme_data_out_reg(23 DOWNTO 16) & vme_data_out_reg(31 DOWNTO 24) & vme_data_out_reg(7 DOWNTO 0) & vme_data_out_reg(15 DOWNTO 8);
         ELSE                                                                    -- data phase with byte routing 2301
            vd_o <= vme_data_out_reg(7 DOWNTO 0) & vme_data_out_reg(15 DOWNTO 8) & vme_data_out_reg(23 DOWNTO 16) & vme_data_out_reg(31 DOWNTO 24);
         END IF;
      ELSE
         IF byte_routing = '1' THEN                                 
            vd_o <= vme_data_out_reg(31 DOWNTO 0);-- data phase with byte routing 3210
         ELSE                                                            
            vd_o <= vme_data_out_reg(15 DOWNTO 0) & vme_data_out_reg(31 DOWNTO 16);-- data phase with byte routing 1032
         END IF;
      END IF;
      
      -- ADDRESS output
      IF swap = '1' THEN
         IF second_word = '1' AND d64 = '1' THEN                           -- master d64 data phases
            va_o <= vme_data_out_reg(7 DOWNTO 0) & vme_data_out_reg(15 DOWNTO 8) & vme_data_out_reg(23 DOWNTO 16) & vme_data_out_reg(31 DOWNTO 24);
         ELSE                                                              -- master address phase
            va_o <= vme_adr_out;
         END IF;
      ELSE
         IF second_word = '1' AND d64 = '1' THEN                           -- master d64 data phases    
            va_o <= vme_data_out_reg(63 DOWNTO 32);
         ELSE                                                              -- master address phase  
            va_o <= vme_adr_out;
         END IF;
      END IF;
      
     va_in_reg <= va_i;
      
      IF en_vme_data_out_reg = '1' THEN
         vme_data_out_reg(31 DOWNTO 0) <= wbs_dat_i;
      END IF;
      
      IF en_vme_data_out_reg_high = '1' THEN
         vme_data_out_reg(63 DOWNTO 32) <= wbs_dat_i;                                        -- vme master 64-bit write access
      END IF;
           
      IF sel_loc_data_out(1) = '0' THEN
         wbs_dat_o_reg <= vme_data_in_reg(31 DOWNTO 0);
      ELSE
         wbs_dat_o_reg <= vme_data_in_reg(63 DOWNTO 32);
      END IF;
      
   END IF;
END PROCESS reg;   

END vme_du_arch;






