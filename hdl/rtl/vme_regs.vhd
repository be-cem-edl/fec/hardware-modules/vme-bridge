-- SPDX-FileCopyrightText: 2016, MEN Mikro Elektronik GmbH
-- SPDX-License-Identifier: CERN-OHL-S-2.0+

--------------------------------------------------------------------------------
-- Title         : Data Unit of VME-Bridge
-- Project       : 16z002-01
--------------------------------------------------------------------------------
-- File          : vme_du.vhd
-- Author        : michael.miehling@men.de
-- Organization  : MEN Mikro Elektronik GmbH
-- Created       : 13/01/03
--------------------------------------------------------------------------------
-- Simulator     : Modelsim PE 6.6
-- Synthesis     : Quartus 15.1
--------------------------------------------------------------------------------
-- Description :
--
-- This unit handles the data path.
--------------------------------------------------------------------------------
-- Hierarchy:
--
-- vme_ctrl
--    vme_du
--------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------
-- History:
--------------------------------------------------------------------------------
-- $Revision: 1.8 $
--
-- $Log: vme_du.vhd,v $
-- Revision 1.8  2015/09/16 09:20:05  mwawrik
-- Added generic USE_LONGADD
--
-- Revision 1.7  2014/04/17 07:35:25  MMiehling
-- added generic LONGADD_SIZE
--
-- Revision 1.6  2013/09/12 08:45:23  mmiehling
-- support of address modifier supervisory, non-privileged data/program for A16, A24 and A32
--
-- Revision 1.5  2012/11/15 09:43:53  MMiehling
-- connected each interrupt source to interface in order to support edge triggered msi
--
-- Revision 1.4  2012/11/12 08:13:10  MMiehling
-- bugfix locmon: improved handling of adr(4:3) for stable results
--
-- Revision 1.3  2012/09/25 11:21:43  MMiehling
-- added wbm_err signal for error signalling from pcie to vme
--
-- Revision 1.2  2012/08/27 12:57:13  MMiehling
-- general rework of d64 slave access handling
-- rework of reset handling
--
-- Revision 1.1  2012/03/29 10:14:39  MMiehling
-- Initial Revision
--
-- Revision 1.11  2006/06/02 15:48:59  MMiehling
-- changed default of arbitration => now not fair is default
--
-- Revision 1.10  2005/02/04 13:44:17  mmiehling
-- added combinations of addr3+4
--
-- Revision 1.9  2004/11/02 11:29:58  mmiehling
-- improved timing and area
-- moved dma_reg to vme_du
--
-- Revision 1.8  2004/07/27 17:15:42  mmiehling
-- changed pci-core to 16z014
-- changed wishbone bus to wb_bus.vhd
-- added clk_trans_wb2wb.vhd
-- improved dma
--
-- Revision 1.7  2003/12/17 15:51:48  MMiehling
-- byte swapping in "not swapped" mode was wrong
--
-- Revision 1.6  2003/12/01 10:03:55  MMiehling
-- added d64
--
-- Revision 1.5  2003/07/14 08:38:10  MMiehling
-- changed mail_irq; added lwordn
--
-- Revision 1.4  2003/06/24 13:47:10  MMiehling
-- added rst_aonly; changed vme_data_in_reg sampling (lwordn)
--
-- Revision 1.3  2003/06/13 10:06:38  MMiehling
-- added address bits 3+4 for locmon;  changed locsta register
--
-- Revision 1.2  2003/04/22 11:03:02  MMiehling
-- changed irq and address map for locmon
--
-- Revision 1.1  2003/04/01 13:04:43  MMiehling
-- Initial Revision
--
--------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;          
USE ieee.numeric_std.all;

ENTITY vme_regs IS
GENERIC (
   LONGADD_SIZE      : integer range 3 TO 8:=3;
   USE_LONGADD       : boolean := TRUE                            -- If FALSE, bits (7 DOWNTO 5) of SIGNAL longadd will be allocated to vme_adr_out(31 DOWNTO 29)
                                                                  -- If TRUE, number of bits allocated to vme_adr_out depends on GENERIC LONGADD_SIZE
   );
PORT (
   clk                     : IN std_logic;                        -- 66 MHz
   rst                     : IN std_logic;                        -- global reset signal (asynch)
   startup_rst             : IN std_logic;                        -- powerup reset
   vme_irq                 : OUT std_logic_vector(7 DOWNTO 0);    -- interrupt request to pci-bus
   berr_irq                : OUT std_logic;                       -- signal berrn interrupt request

   -- vme_au
   wbr_stb_i               : IN std_logic;
   wbr_cyc_i               : IN std_logic;
   wbr_ack_o               : OUT std_logic;
   wbr_adr_i               : IN std_logic_vector(9 DOWNTO 0);    -- internal adress for reg
   wbr_sel_i               : IN std_logic_vector(3 DOWNTO 0);     -- internal byte enables
   wbr_we_i                : IN std_logic;                        -- write flag for register write access
   wbr_dat_o               : OUT std_logic_vector(31 DOWNTO 0);
   wbr_dat_i               : IN std_logic_vector(31 DOWNTO 0);
   wbr_err_o               : OUT std_logic;

   -- dma
   dma_sta                 : OUT std_logic_vector(9 DOWNTO 0);
   clr_dma_en              : IN std_logic;
   set_dma_err             : IN std_logic;
   dma_act_bd              : IN std_logic_vector(7 DOWNTO 4);

   -- requester
   brl                     : OUT std_logic_vector(1 DOWNTO 0);    -- bus request leve

   --  For berr
   vam_reg                 : IN std_logic_vector(5 DOWNTO 0);     -- registered vam_in for location monitoring and berr_adr (registered with en_vme_adr_in)
   vme_adr_in_reg          : IN std_logic_vector(31 DOWNTO 2);    -- vme adress for location monitoring and berr_adr (registered with en_vme_adr_in)
   sl_writen_reg           : IN std_logic;                        -- vme read/wrtie signal (registered with en_vme_adr_in)
   iackn_in_reg            : IN std_logic;                        -- iack signal (registered with en_vme_adr_in)

   -- register out
   longadd                 : OUT std_logic_vector(7 DOWNTO 0);    -- upper 3 address bits for A32 mode or dependent on LONGADD_SIZE
   mstr_reg                : OUT std_logic_vector(13 DOWNTO 0);   -- master register (aonly, postwr, iberr, berr, req, rmw, A16_MODE, A24_MODE, A32_MODE)
   sysc_reg                : OUT std_logic_vector(1 DOWNTO 0);    -- system control register (ato, sysr, sysc)
   
   -- register bits
   set_berr                : IN std_logic;                        -- if bit is set => berr bit will be set
   rst_rmw                 : IN std_logic;                        -- if bit is set => rmw bit will be cleared
   set_sysc                : IN std_logic;                        -- if bit is set => sysc bit will be set
   set_ato                 : IN std_logic;                        -- if bit is set => ato bit will be set
   clr_sysr                : IN std_logic;                        -- if bit is set => sysr bit will be cleared
   rst_aonly               : IN std_logic;                        -- resets aonly bit
   
   --  dbram
   dbram_addr_i            : IN std_logic_vector(3 downto 0);
   dbram_data_o            : OUT std_logic_vector(127 downto 0);

   -- irq pins
   irq_i_n                 : IN std_logic_vector(7 DOWNTO 1);
   irq_o_n                 : OUT std_logic_vector(7 DOWNTO 1);
   acfailn                 : IN  std_logic;                       -- ACFAIL# input from Power Supply
   
   --vme
   ga                      : IN std_logic_vector(4 DOWNTO 0);     -- geographical addresses
   gap                     : IN std_logic                         -- geographical addresses parity
   );
END vme_regs;

ARCHITECTURE vme_regs_arch OF vme_regs IS 
   SIGNAL longadd_int            : std_logic_vector(7 DOWNTO 0);
   SIGNAL intid_int              : std_logic_vector(7 DOWNTO 0);
   SIGNAL istat                  : std_logic_vector(7 DOWNTO 0);
   SIGNAL imask                  : std_logic_vector(7 DOWNTO 0);
   SIGNAL acfail_regd            : std_logic;
   SIGNAL irqregd                : std_logic_vector(7 DOWNTO 1);
   SIGNAL set_dma_err_q          : std_logic;
   SIGNAL ga_q                   : std_logic_vector(5 DOWNTO 0);        -- geographical addresses and parity
   SIGNAL slot_nr                : std_logic_vector(4 DOWNTO 0);        -- slot number
   SIGNAL berr_vam               : std_logic_vector(5 downto 0);
   SIGNAL berr_adr               : std_logic_vector(31 downto 0);
   SIGNAL berr_rw                : std_logic;
   SIGNAL berr_iack              : std_logic;
   signal rst_n                  : std_logic;
   signal mstr_wr                : std_logic;
   signal mstr_rmwena, mstr_rmwena_out : std_logic;
   signal mstr_berr              : std_logic;
   signal mstr_ado, mstr_ado_out : std_logic;
   signal mstr_iberr             : std_logic;
   signal sysc_wr                : std_logic;
   signal sysc_sysc, sysc_sysc_out : std_logic;
   signal sysc_sysr, sysc_sysr_out : std_logic;
   signal dmasta_wr              : std_logic;
   signal dmasta_en, dmasta_en_out : std_logic;
   signal dmasta_en_p            : std_logic;
   signal dmasta_irq, dmasta_irq_out : std_logic;
   signal dmasta_ien             : std_logic;
   signal dmasta_err, dmasta_err_out : std_logic;
   signal dmasta_err_p           : std_logic;
   signal istat_acfail           : std_logic;

   signal dbram_din_int          : std_logic_vector(31 downto 0);
   signal dbram_dout_int         : std_logic_vector(31 downto 0);
   signal dbram_dout_sram        : std_logic_vector(127 downto 0);
   signal dbram_addr_int         : std_logic_vector(8 downto 2);
   signal dbram_we_int           : std_logic;
   signal dbram_bank             : natural range 0 to 3;
BEGIN
   longadd <= longadd_int WHEN USE_LONGADD ELSE longadd_int(2 DOWNTO 0)&"00000";
   vme_irq <= istat;
   
reg : PROCESS(clk, rst)
BEGIN
   IF rst = '1' THEN
      ga_q  <= (OTHERS => '0');
      slot_nr <= (OTHERS => '0');
   ELSIF clk'EVENT and clk = '1' THEN
      -- synchronization registers
      ga_q  <= gap & ga;
      if (gap xor ga(4) xor ga(3) xor ga(2) xor ga(1) xor ga(0)) = '1' then
         slot_nr <= not ga;
      else
         slot_nr <= "11110"; --  amnesia address
      end if;
   END IF;
END PROCESS reg;   

  rst_n <= not rst;

  inst_map: entity work.vme_regs_map
    port map (
      rst_n_i => rst_n,
      clk_i => clk,
      wb_cyc_i => wbr_cyc_i,
      wb_stb_i => wbr_stb_i,
      wb_adr_i => wbr_adr_i(9 downto 2),
      wb_sel_i => wbr_sel_i,
      wb_we_i => wbr_we_i,
      wb_dat_i => wbr_dat_i,
      wb_ack_o => wbr_ack_o,
      wb_err_o => wbr_err_o,
      wb_rty_o => open,
      wb_stall_o => open,
      wb_dat_o => wbr_dat_o,
      version_i => x"5a00_04_00",
      intid_val_o => intid_int,
      istat_acfail_i => acfail_regd,
      istat_acfail_o => istat_acfail,
      istat_irq_i => istat(7 downto 1),
      istat_irq_o => open,
      imask_acfail_o => imask(0),
      imask_irq_o => imask(7 downto 1),
      mstr_rmwena_i => mstr_rmwena,
      mstr_rmwena_o => mstr_rmwena_out,
      mstr_req_o => mstr_reg(1),
      mstr_berr_i => set_berr,
      mstr_berr_o => mstr_berr,
      mstr_iberr_o => mstr_iberr,
      mstr_postwr_o => mstr_reg(4),
      mstr_ado_i => mstr_ado,
      mstr_ado_o => mstr_ado_out,
      mstr_fairreq_o => mstr_reg(6),
      mstr_a16_mode_o => mstr_reg(9 downto 8),
      mstr_a24_mode_o => mstr_reg(11 downto 10),
      mstr_a32_mode_o => mstr_reg(13 downto 12),
      mstr_wr_o => mstr_wr,
      sysc_sysc_i => sysc_sysc,
      sysc_sysc_o => sysc_sysc_out,
      sysc_sysr_i => sysc_sysr,
      sysc_sysr_o => sysc_sysr_out,
      sysc_ato_i => set_ato,
      sysc_wr_o => sysc_wr,
      longadd_val_o => longadd_int,
      dmasta_en_i => dmasta_en,
      dmasta_en_o => dmasta_en_out,
      dmasta_ien_o => dmasta_ien,
      dmasta_irq_i => dmasta_irq,
      dmasta_irq_o => dmasta_irq_out,
      dmasta_err_i => dmasta_err,
      dmasta_err_o => dmasta_err_out,
      dmasta_bd_i => dma_act_bd,
      dmasta_bd_o => open,
      dmasta_wr_o => dmasta_wr,
      slot_ga_i => ga_q,
      slot_slot_i => slot_nr,
      brl_val_o => brl,
      berr_addr_i => berr_adr,
      berr_extra_vam_i => berr_vam,
      berr_extra_rw_i => berr_rw,
      berr_extra_iack_i => berr_iack,

      dbram_addr_o => dbram_addr_int,
      dbram_wr_o   => dbram_we_int,
      dbram_data_i => dbram_dout_int,
      dbram_data_o => dbram_din_int
    );

-------------------------------------------------------------------------------
-- dma_sta_int register 0x2c
sta :PROCESS(clk, rst)
BEGIN
   IF rst = '1' THEN
      dmasta_en <= '0';
      dmasta_en_p <= '0';
      dmasta_irq <= '0';
      dmasta_err <= '0';
      dmasta_err_p <= '0';
      set_dma_err_q <= '0';
   ELSIF clk'EVENT AND clk = '1' THEN
      set_dma_err_q <= set_dma_err;

      dmasta_en_p <= '0';
      IF clr_dma_en = '1' THEN
         dmasta_en <= '0';
      ELSIF dmasta_wr = '1' then
         dmasta_en <= dmasta_en_out;
         dmasta_en_p <= dmasta_en_out;
      END IF;
      
      IF clr_dma_en = '1' AND dmasta_ien = '1' THEN
         dmasta_irq <= '1';
      ELSIF dmasta_wr = '1' and dmasta_irq_out = '1' then
         dmasta_irq <= '0';
      END IF;

      dmasta_err_p <= '0';
      IF set_dma_err = '1' AND set_dma_err_q = '0' THEN
         dmasta_err <= '1';
         dmasta_err_p <= '1';
      ELSIF dmasta_wr = '1' and dmasta_err_out = '1' then
         dmasta_err <= '0';
         dmasta_err_p <= '1';  --  TODO: check
      END IF;
   END IF;
END PROCESS sta;   
  
   dma_sta(0) <= dmasta_en;
   dma_sta(1) <= dmasta_ien;
   dma_sta(2) <= dmasta_irq;
   dma_sta(3) <= dmasta_err;
   dma_sta(7 DOWNTO 4) <= dma_act_bd;
   dma_sta(8) <= dmasta_en_p;
   dma_sta(9) <= dmasta_err_p;

  
  irq_o_n <= "1111111";

-------------------------------------------------------------------------------
-- The IMASK Register. '0' means interrupt is masked, '1' means it is enabled.
-------------------------------------------------------------------------------
i_mask : PROCESS(clk, rst)
  BEGIN
     IF rst = '1' THEN
        istat <= (OTHERS => '0');
        irqregd <= (OTHERS => '0');
        acfail_regd <= '0';
        berr_irq <= '0';
     ELSIF clk'EVENT AND clk = '1' THEN
        istat <= (irqregd & istat_acfail) AND imask;

        irqregd <= NOT irq_i_n;
        acfail_regd <= not acfailn;
        
        berr_irq <= mstr_berr AND mstr_iberr;
     END IF;
  END PROCESS i_mask;   
 
-------------------------------------------------------------------------------
-- Here is the Master Register:
-- Consists of (AONLY-bit, POSTWR-bit, IBERR-bit, BERR-bit, REQ-bit, RMW-bit
-- A16_MODE, A24_MODE, A32_MODE)
-------------------------------------------------------------------------------
   
  -- RMW-bit:
rmwena : PROCESS (clk, rst)
  BEGIN
    IF rst = '1' THEN
      mstr_rmwena <= '0';
    ELSIF clk'EVENT AND clk = '1' THEN
      IF mstr_wr = '1' then
         mstr_rmwena <= mstr_rmwena_out;
      ELSIF rst_rmw = '1' THEN
        mstr_rmwena <= '0';
      END IF;
    END IF;
  END PROCESS rmwena;

  mstr_reg(0) <= mstr_rmwena;
  --  mstr_reg(1) is directly assigned
  mstr_reg(2) <= mstr_berr;
  mstr_reg(3) <= mstr_iberr;
  --  mstr_reg(4) is directly assigned
  mstr_reg(5) <= mstr_ado;
  mstr_reg(7) <= '0'; -- unused

  -- BERR-bit and BERR_ADR/BERR_ACC
berrena : PROCESS (clk, rst)
  BEGIN
    IF rst = '1' THEN
      berr_vam <= (OTHERS => '0');
      berr_adr <= (OTHERS => '0');
      berr_rw <= '0';
      berr_iack <= '0';
    ELSIF (clk'event AND clk = '1') THEN
      IF set_berr = '1' THEN
         berr_vam <= vam_reg;
         berr_adr <= vme_adr_in_reg & "00";
         berr_rw <= sl_writen_reg;
         berr_iack <= NOT iackn_in_reg;
      ELSIF mstr_berr = '0' then
         berr_vam <= (OTHERS => '0');
         berr_adr <= (OTHERS => '0');
         berr_rw <= '0';
         berr_iack <= '0';
      END IF;
    END IF;
  END PROCESS berrena;

  -- AONLY-bit:
aonlyena : PROCESS (clk, rst)
  BEGIN
    IF rst = '1' THEN
      mstr_ado <= '0';
    ELSIF (clk'event AND clk = '1') THEN
      IF mstr_wr = '1' then
         mstr_ado <= mstr_ado_out;
      ELSIF rst_aonly = '1' THEN
         mstr_ado   <= '0';
      END IF;
    END IF;
  END PROCESS aonlyena;

      

-------------------------------------------------------------------------------
-- Here is the System Control Register:
-- Consists of (ATO-bit, SYSR-bit, SYSC-bit)
-------------------------------------------------------------------------------

-- sysr-bit
sysrena : PROCESS (clk, rst)
  BEGIN
    IF rst = '1' THEN
      sysc_sysr   <= '0';
    ELSIF clk'EVENT AND clk = '1' THEN
      IF sysc_wr = '1' then
         sysc_sysr <= sysc_sysr_out;
      ELSIF clr_sysr = '1' THEN
         sysc_sysr <= '0';
      END IF;
    END IF;
  END PROCESS sysrena;

-- sysc-bit
syscena : PROCESS (clk, startup_rst)
  BEGIN
     IF startup_rst = '1' THEN
        sysc_sysc <= '0';
     ELSIF clk'EVENT AND clk = '1' THEN
      IF set_sysc = '1' THEN
        sysc_sysc  <= '1';
      ELSIF sysc_wr = '1' then
         sysc_sysc <= sysc_sysc_out;
      END IF;
    END IF;
  END PROCESS syscena;

  sysc_reg(0) <= sysc_sysc;
  sysc_reg(1) <= sysc_sysr;

  --  DMA regs
  dbram_bank <= to_integer(unsigned(dbram_addr_int(3 downto 2)));

  g_db_ram: for i in 0 to 3 generate
    signal we : std_logic;
  begin
    we <= '1' when dbram_we_int = '1' and i = dbram_bank else '0';
    inst_ram: entity work.generic_dpram
    generic map (
      g_data_width => 32,
      g_size => 16,
      g_with_byte_enable => false,
      g_addr_conflict_resolution => open,
      g_init_file => "",
      g_dual_clock => False,
      g_implementation_hint => open
    )
    port map (
      rst_n_i => rst_n,
      clka_i => clk,
      bwea_i => "1111",
      wea_i => we,
      aa_i => dbram_addr_int(7 downto 4),
      da_i => dbram_din_int,
      qa_o => dbram_dout_sram(i*32 + 31 downto i * 32),
      clkb_i => clk,
      bweb_i => "0000",
      web_i => '0',
      ab_i => dbram_addr_i,
      db_i => (others => 'X'),
      qb_o => dbram_data_o(i*32 + 31 downto i * 32)
    );
  end generate;

  dbram_dout_int <= dbram_dout_sram(dbram_bank*32 + 31 downto dbram_bank * 32);
END vme_regs_arch;
