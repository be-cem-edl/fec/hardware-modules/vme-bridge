-- SPDX-FileCopyrightText: 2016, MEN Mikro Elektronik GmbH
--
-- SPDX-License-Identifier: CERN-OHL-S-2.0+
-- --------------------------------------------------------------------------------
-- Title         : WBB to VME Bridge
-- Project       : 16z002-01
--------------------------------------------------------------------------------
-- File          : wbb2vme_top.vhd
-- Author        : michael.miehling@men.de
-- Organization  : MEN Mikro Elektronik GmbH
-- Created       : 13/01/12
--------------------------------------------------------------------------------
-- Simulator     : Modelsim PE 6.6
-- Synthesis     : Quartus 15.1
--------------------------------------------------------------------------------
-- Description :
--
-- The IP-core WBB2VME is used for interfacing the VME bus as master and as 
-- slave. It is able to control external driver chips as 74VMEH22501 or 74ABT125. 
-- An external SRAM is used for shared memory applications and can be accessed 
-- from CPU and VME side. 
-- The main functions of the 16z002-01 are: 
--	o Wishbone to VME access: VME master D08(EO):D16:D32:D64:A16:A24:A32; BLT; 
--   non-privileged program/data; supervisory
--	o VME to Wishbone access: VME slave D08(EO):D16:D32:D64:A16:A24:A32; BLT
--	o VME slave access routing to SRAM or other bus via Wishbone bus (e.g. PCI)
--	o VME Slot1 function with auto-detection
--	o VME Interrupter D08(O):I(7-1):ROAK
--	o VME Interrupt Handler D08(O):IH(7-1)
--	o VME Bus requester 
--  o ROR (release on request); 
--  o RWD (release-when done); 
--  o SGL (single level 3 fair requester)
--	o VME multi-level 0-3 bus arbiter
--	o BTO � VME Bus time out
--	o ADO � VME Address only cycles
--	o mailbox functionality
--	o VME location monitor A16:A24:A32
--	o DMA controller with scatter gather capabilities (A24; A32; D32; D64; 
--   non-privileged; supervisory)
--	o DMA access capabilities VME, SRAM and other bus via Wishbone bus (e.g. PCI)
--	o VME utility functions
--	o access to 1 MByte local SRAM accessible via Wishbone bus
--	o VME Slot geographical addressing

--------------------------------------------------------------------------------
-- Hierarchy:
--
-- wbb2vme
--    vme_ctrl
--       vme_du
--       vme_au
--       vme_locmon
--       vme_mailbox
--       vme_master
--       vme_slave
--       vme_requester
--       vme_bustimer
--       vme_sys_arbiter
--       vme_arbiter
--       vme_wbm
--       vme_wbs
--    vme_dma
--       vme_dma_mstr
--       vme_dma_slv
--       vme_dma_arbiter
--       vme_dma_du
--       vme_dma_au
--       vme_dma_fifo
--          fifo_256x32bit
--------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------
-- History:
--------------------------------------------------------------------------------
-- $Revision: 1.7 $
--
-- $Log: wbb2vme_top.vhd,v $
-- Revision 1.7  2015/09/16 09:19:48  mwawrik
-- Added generics A16_REG_MAPPING and USE_LONGADD
--
-- Revision 1.6  2014/04/17 07:35:18  MMiehling
-- added generic LONGADD_SIZE
-- added status outputs vme_berr and vme_mstr_busy
-- added signal prevent_sysrst
--
-- Revision 1.5  2013/09/12 08:45:19  mmiehling
-- added bit 8 of tga for address modifier extension
--
-- Revision 1.4  2012/11/15 09:43:50  MMiehling
-- connected each interrupt source to interface in order to support edge triggered msi
--
-- Revision 1.3  2012/09/25 11:21:37  MMiehling
-- added wbm_err signal for error signalling from pcie to vme
--
-- Revision 1.2  2012/08/27 12:57:00  MMiehling
-- changed comments
--
-- Revision 1.1  2012/03/29 10:14:27  MMiehling
-- Initial Revision
--
--------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE work.vme_pkg.all;

ENTITY wbb2vme_top IS
GENERIC (
   g_simulation : boolean := false;
   A16_REG_MAPPING   : boolean := TRUE;                        -- if true, access to vme slave A16 space goes to vme runtime registers and above 0x800 to sram (compatible to old revisions)
                                                               -- if false, access to vme slave A16 space goes to sram
   LONGADD_SIZE      : integer range 3 TO 8:=3;
   USE_LONGADD       : boolean := TRUE                          -- If FALSE, bits (7 DOWNTO 5) of SIGNAL longadd will be allocated to vme_adr_out(31 DOWNTO 29)
   );
PORT (
   clk               : IN std_logic;                      -- 66 MHz
   rst               : IN std_logic;                      -- global reset signal (asynch)
   startup_rst       : IN std_logic;                      -- powerup reset
   postwr            : OUT std_logic;                     -- posted write
   vme_irq           : OUT std_logic_vector(7 DOWNTO 0);  -- interrupt request to pci-bus
   berr_irq          : OUT std_logic;                     -- signal berrn interrupt request
   locmon_irq        : OUT std_logic_vector(1 DOWNTO 0);  -- interrupt request location monitor to pci-bus
   mailbox_irq       : OUT std_logic_vector(1 DOWNTO 0);  -- interrupt request mailbox to pci-bus
   dma_irq           : OUT std_logic;                     -- interrupt request dma to pci-bus
   prevent_sysrst    : IN std_logic;                      -- if "1", sysrst_n_out will not be activated after powerup,
                                                          -- if "0", sysrst_n_out will be activated if in slot1 and system reset is active (sysc_bit or rst)
   -- vmectrl slave
   wbs_stb_i         : IN std_logic;
   wbs_ack_o         : OUT std_logic;
   wbs_err_o         : OUT std_logic;
   wbs_we_i          : IN std_logic;
   wbs_sel_i         : IN std_logic_vector(3 DOWNTO 0);
   wbs_cyc_i         : IN std_logic;
   wbs_adr_i         : IN std_logic_vector(31 DOWNTO 0);
   wbs_dat_o         : OUT std_logic_vector(31 DOWNTO 0);
   wbs_dat_i         : IN std_logic_vector(31 DOWNTO 0);
   wbs_tga_i         : IN std_logic_vector(8 DOWNTO 0);

   -- Registers slave
   wbr_stb_i         : IN std_logic;
   wbr_ack_o         : OUT std_logic;
   wbr_err_o         : OUT std_logic;
   wbr_we_i          : IN std_logic;
   wbr_sel_i         : IN std_logic_vector(3 DOWNTO 0);
   wbr_cyc_i         : IN std_logic;
   wbr_adr_i         : IN std_logic_vector(31 DOWNTO 0);
   wbr_dat_o         : OUT std_logic_vector(31 DOWNTO 0);
   wbr_dat_i         : IN std_logic_vector(31 DOWNTO 0);

   wbm_dma_stb_o    : OUT std_logic;
   wbm_dma_ack_i    : IN std_logic;
   wbm_dma_we_o     : OUT std_logic;
   wbm_dma_cti      : OUT std_logic_vector(2 DOWNTO 0);
   wbm_dma_tga_o    : OUT std_logic_vector(8 DOWNTO 0);
   wbm_dma_err_i    : IN std_logic;
   wbm_dma_sel_o    : OUT std_logic_vector(3 DOWNTO 0);
   wbm_dma_cyc_vme  : OUT std_logic;
   wbm_dma_cyc_pci  : OUT std_logic;
   wbm_dma_adr_o    : OUT std_logic_vector(31 DOWNTO 0);
   wbm_dma_dat_o    : OUT std_logic_vector(31 DOWNTO 0);
   wbm_dma_dat_i    : IN std_logic_vector(31 DOWNTO 0);

   -- dedicated PCI wb-master for DMA boost
   pci_cyc_o      : OUT std_logic;
   pci_stb_o      : OUT std_logic;
   pci_we_o       : OUT std_logic;
   pci_sel_o      : OUT std_logic_vector(3 DOWNTO 0);
   pci_tga_o      : OUT std_logic_vector(8 DOWNTO 0);
   pci_cti_o      : OUT std_logic_vector(2 DOWNTO 0);
   pci_adr_o      : OUT std_logic_vector(31 DOWNTO 0);
   pci_dat_o      : OUT std_logic_vector(31 DOWNTO 0);
   pci_dat_i      : IN std_logic_vector(31 DOWNTO 0);
   pci_ack_i      : IN std_logic;
   pci_err_i      : IN std_logic;

   -- vmebus
   va_o              : OUT std_logic_vector(31 DOWNTO 0);    -- address
   va_i              : IN  std_logic_vector(31 DOWNTO 0);    -- address
   vd_o              : OUT std_logic_vector(31 DOWNTO 0);    -- data
   vd_i              : IN  std_logic_vector(31 DOWNTO 0);    -- data
   vam_o             : OUT std_logic_vector(5 DOWNTO 0);     -- address modifier
   vam_i             : IN  std_logic_vector(5 DOWNTO 0);     -- address modifier
   writen_o          : OUT std_logic;                        -- write enable
   writen_i          : IN  std_logic;                        -- write enable
   iackn_o           : OUT std_logic;                        -- Handler's output
   iackn_i           : IN  std_logic;                        -- Handler's output
   irq_i_n           : IN  std_logic_vector(7 DOWNTO 1);        -- interrupt request inputs
   irq_o_n           : OUT std_logic_vector(7 DOWNTO 1);       -- interrupt request outputs
   as_o_n            : OUT   std_logic;                        -- address strobe out  
   as_oe_n           : OUT   std_logic;                        -- address strobe output enable  
   as_i_n            : IN    std_logic;                        -- address strobe in
   sysresn           : OUT   std_logic;                        -- system reset out 
   sysresin          : IN    std_logic;                        -- system reset in
   ds_o_n            : OUT   std_logic_vector(1 DOWNTO 0);     -- data strobe outputs
   ds_i_n            : IN   std_logic_vector(1 DOWNTO 0);      -- data strobe inputs
   ds_oe_n           : OUT std_logic;                          -- data strobe output enable
   berrn             : OUT   std_logic;                        -- bus error out    
   berrin            : IN    std_logic;                        -- bus error in 
   dtackn            : OUT   std_logic;                        -- dtack out   
   dtackin           : IN    std_logic;                        -- dtack in
   slot01n           : OUT   std_logic;                        -- indicates whether controller has detected position in slot 1 (low active)
   sysfail_i_n       : IN   std_logic;                        -- system failure interrupt input
   sysfail_o_n       : OUT   std_logic;                        -- system failure interrupt output
   bbsyn             : OUT   std_logic;                        -- bus busy out    
   bbsyin            : IN    std_logic;                        -- bus busy in     
   bclr_i_n          : IN std_logic;                           -- bus clear input
   bclr_o_n          : OUT std_logic;                          -- bus clear output
   retry_i_n         : IN std_logic;                           -- bus retry input
   retry_o_n         : OUT std_logic;                          -- bus retry output
   retry_oe_n        : OUT std_logic;                          -- bus retry output enable
   br_i_n            : IN std_logic_vector(3 DOWNTO 0);        -- bus request inputs
   br_o_n            : OUT std_logic_vector(3 DOWNTO 0);       -- bus request outputs
   iackin            : IN    std_logic;                        -- Interrupter's input
   iackoutn          : OUT   std_logic;                        -- Interrupter's output
   acfailn           : IN    std_logic;                        -- from Power Supply
   bg_i_n            : IN  std_logic_vector(3 DOWNTO 0);       -- bus grant input
   bg_o_n            : OUT std_logic_vector(3 DOWNTO 0);       -- bus grant output
   ga                : IN std_logic_vector(4 DOWNTO 0);        -- geographical addresses
   gap               : IN std_logic;                           -- geographical addresses parity

   -- vme status signals (for leds)
   vme_berr          : OUT std_logic;                          -- indicates vme bus error (=MSTR(2)), must be cleared by sw           
   vme_mstr_busy     : OUT std_logic;                          -- indicates vme bus master is active
        
   --data bus bus control signals for vmebus drivers
   d_dir             : OUT std_logic;                          -- external driver control data direction (1: drive to vmebus 0: drive to fpga)
   d_oe_n            : OUT std_logic;                          -- external driver control data output enable low active
   am_dir            : OUT std_logic;                          -- external driver control address modifier direction (1: drive to vmebus 0: drive to fpga)
   am_oe_n           : OUT std_logic;                          -- external driver control address modifier output enable low activ 
   a_dir             : OUT std_logic;                          -- external driver control address direction (1: drive to vmebus 0: drive to fpga)
   a_oe_n            : OUT std_logic;                          -- external driver control address output enable low activ
   
   v2p_rst           : OUT std_logic                           -- Reset between VMEbus and Host CPU
     );
END wbb2vme_top;

ARCHITECTURE wbb2vme_top_arch OF wbb2vme_top IS 
   SIGNAL dma_sta           : std_logic_vector(9 DOWNTO 0);
   SIGNAL clr_dma_en        : std_logic;
   SIGNAL set_dma_err       : std_logic;
   SIGNAL dma_act_bd        : std_logic_vector(7 DOWNTO 4);
   signal dma_reached_size  : std_logic;

   SIGNAL berr_vam          : std_logic_vector(5 DOWNTO 0);
   SIGNAL berr_adr          : std_logic_vector(31 DOWNTO 2);
   SIGNAL berr_writen       : std_logic;
   SIGNAL berr_iackn        : std_logic;

   SIGNAL brl                          : std_logic_vector(1 DOWNTO 0);
   SIGNAL longadd                      : std_logic_vector(7 DOWNTO 0);
   SIGNAL mstr_reg                     : std_logic_vector(13 DOWNTO 0);
   SIGNAL sysc_reg                     : std_logic_vector(1 DOWNTO 0);

   SIGNAL rst_rmw                      : std_logic;
   SIGNAL set_berr                     : std_logic;
   SIGNAL rst_aonly                    : std_logic;
   SIGNAL set_sysc                     : std_logic:='0';
   SIGNAL clr_sysr                     : std_logic;
   SIGNAL set_ato                      : std_logic;

   signal dbram_addr         : std_logic_vector(3 downto 0);
   signal dbram_data         : std_logic_vector(127 downto 0);
BEGIN
   bclr_o_n   <= '1';
   retry_o_n  <= '1';
   retry_oe_n <= '1';

   locmon_irq <= "00";

   vme_berr <= mstr_reg(2);
   postwr <= mstr_reg(4);
   mailbox_irq <= "00";

   regs : entity work.vme_regs
   GENERIC MAP (
      LONGADD_SIZE      => LONGADD_SIZE,
      USE_LONGADD       => USE_LONGADD
      )
   PORT MAP (
      clk                        => clk,
      rst                        => rst,
      wbr_cyc_i                  => wbr_cyc_i,
      wbr_stb_i                  => wbr_stb_i,
      wbr_adr_i                  => wbr_adr_i(9 downto 0),
      wbr_sel_i                  => wbr_sel_i,
      wbr_we_i                   => wbr_we_i,
      wbr_dat_i                  => wbr_dat_i,
      wbr_dat_o                  => wbr_dat_o,
      wbr_ack_o                  => wbr_ack_o,
      wbr_err_o                  => wbr_err_o,

      dbram_addr_i               => dbram_addr,
      dbram_data_o               => dbram_data,

      dma_sta                    => dma_sta,
      clr_dma_en                 => clr_dma_en,
      set_dma_err                => set_dma_err,
      dma_act_bd                 => dma_act_bd,
      startup_rst                => startup_rst,
      vme_irq                    => vme_irq,
      berr_irq                   => berr_irq,
      vam_reg                    => berr_vam,
      vme_adr_in_reg             => berr_adr,
      sl_writen_reg              => berr_writen,
      iackn_in_reg               => berr_iackn,
      brl                        => brl,
      longadd                    => longadd,
      mstr_reg                   => mstr_reg,
      sysc_reg                   => sysc_reg,
      set_berr                   => set_berr,
      rst_rmw                    => rst_rmw,
      set_sysc                   => set_sysc,
      set_ato                    => set_ato,
      clr_sysr                   => clr_sysr,
      rst_aonly                  => rst_aonly,
      irq_i_n                    => irq_i_n,
      irq_o_n                    => irq_o_n,
      acfailn                    => acfailn,
      ga                         => ga,
      gap                        => gap
   );

vmectrl: entity work.vme_ctrl 
GENERIC MAP (
   g_simulation      => g_simulation,
   A16_REG_MAPPING   => A16_REG_MAPPING,
   LONGADD_SIZE      => LONGADD_SIZE,
   USE_LONGADD       => USE_LONGADD
   )
PORT MAP(
   clk               => clk,
   rst               => rst,
   startup_rst       => startup_rst,
   prevent_sysrst    => prevent_sysrst,
                                     
   dma_reached_size_i    => dma_reached_size,
                                     
   wbs_stb_i         => wbs_stb_i,
   wbs_ack_o         => wbs_ack_o,
   wbs_err_o         => wbs_err_o,
   wbs_we_i          => wbs_we_i,
   wbs_sel_i         => wbs_sel_i,
   wbs_cyc_i         => wbs_cyc_i,
   wbs_adr_i         => wbs_adr_i,
   wbs_dat_o         => wbs_dat_o,
   wbs_dat_i         => wbs_dat_i,
   wbs_tga_i         => wbs_tga_i,

   va_o              => va_o,
   va_i              => va_i,
   vd_o              => vd_o,
   vd_i              => vd_i,
   vam_o             => vam_o,
   vam_i             => vam_i,
   writen_o          => writen_o,
   writen_i          => writen_i,
   iackn_o           => iackn_o,
   iackn_i           => iackn_i,
                                     
   irq_i_n           => irq_i_n,
   as_o_n            => as_o_n,
   as_oe_n           => as_oe_n,
   as_i_n            => as_i_n,
   sysresn           => sysresn,
   sysresin          => sysresin,
   ds_o_n            => ds_o_n,
   ds_i_n            => ds_i_n,
   ds_oe_n           => ds_oe_n,
   berrn             => berrn,
   berrin            => berrin,
   dtackn            => dtackn,
   dtackin           => dtackin,
   slot01n           => slot01n,
   sysfail_i_n       => sysfail_i_n,
   sysfail_o_n       => sysfail_o_n,
   bbsyn             => bbsyn,
   bbsyin            => bbsyin,
   br_i_n            => br_i_n,
   br_o_n            => br_o_n,
   iackin            => iackin,
   iackoutn          => iackoutn,
   bg_i_n            => bg_i_n,
   bg_o_n            => bg_o_n,

   berr_vam          => berr_vam,
   berr_adr          => berr_adr,
   berr_writen       => berr_writen,
   berr_iackn        => berr_iackn,
   vme_mstr_busy     => vme_mstr_busy,

   brl               => brl,
   longadd           => longadd,
   mstr_reg          => mstr_reg,
   sysc_reg          => sysc_reg,
   rst_rmw           => rst_rmw,
   set_berr          => set_berr,
   rst_aonly         => rst_aonly,
   set_sysc          => set_sysc,
   clr_sysr          => clr_sysr,
   set_ato           => set_ato,

   d_dir             => d_dir,
   d_oe_n            => d_oe_n,
   am_dir            => am_dir,
   am_oe_n           => am_oe_n,
   a_dir             => a_dir,
   a_oe_n            => a_oe_n,
   v2p_rst           => v2p_rst   
     );

vmedma: entity work.vme_dma
PORT MAP (
   rst            => rst,
   clk            => clk,
   irq_o          => dma_irq,
                                
   dma_sta        => dma_sta,
   clr_dma_en     => clr_dma_en,
   set_dma_err    => set_dma_err,
   dma_act_bd     => dma_act_bd,

   cycle_end_o    => dma_reached_size,
                                
   dbram_addr_o   => dbram_addr,
   dbram_data_i   => dbram_data,

   stb_o          => wbm_dma_stb_o,
   ack_i          => wbm_dma_ack_i,
   we_o           => wbm_dma_we_o,
   cti_o          => wbm_dma_cti,
   tga_o          => wbm_dma_tga_o,
   err_i          => wbm_dma_err_i,
   cyc_o_vme      => wbm_dma_cyc_vme,
   cyc_o_pci      => wbm_dma_cyc_pci,
   sel_o          => wbm_dma_sel_o,
   adr_o          => wbm_dma_adr_o,
   mstr_dat_o     => wbm_dma_dat_o,
   mstr_dat_i     => wbm_dma_dat_i,

   pci_cyc_o      => pci_cyc_o,
   pci_stb_o      => pci_stb_o,
   pci_we_o       => pci_we_o,
   pci_sel_o      => pci_sel_o,
   pci_tga_o      => pci_tga_o,
   pci_cti_o      => pci_cti_o,
   pci_adr_o      => pci_adr_o,
   pci_dat_o      => pci_dat_o,
   pci_dat_i      => pci_dat_i,
   pci_ack_i      => pci_ack_i,
   pci_err_i      => pci_err_i);
END wbb2vme_top_arch;
