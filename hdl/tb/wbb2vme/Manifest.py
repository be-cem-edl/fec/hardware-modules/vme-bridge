# SPDX-FileCopyrightText: 2022 CERN (home.cern)
#
# SPDX-License-Identifier: CERN-OHL-W-2.0+

sim_tool   = "ghdl"
sim_top    = "tb_wbb2vme"
action     = "simulation"
syn_device = 'any'
ghdl_opt = "-fsynopsys -fexplicit"

target = "xilinx"

files = [ 'tb_wbb2vme.vhd', 'wb_arb2.vhd', 'slave_map.vhd',
          'tbsram.vhd']

# Allow the user to override fetchto using:
#  hdlmake -p "fetchto='xxx'"
if locals().get('fetchto', None) is None:
    fetchto = "../../../dependencies"

modules = {
    "local" :  [
        "../../rtl",
    ],
    "git" : [
        'https://ohwr.org/project/general-cores.git',
        'https://ohwr.org/project/vme64x-core.git',
    ]
}
