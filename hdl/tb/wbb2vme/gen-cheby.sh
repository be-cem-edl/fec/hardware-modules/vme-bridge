# SPDX-FileCopyrightText: 2022 CERN (home.cern)
#
# SPDX-License-Identifier: CERN-OHL-W-2.0+


cheby -i slave_map.cheby --gen-hdl slave_map.vhd
