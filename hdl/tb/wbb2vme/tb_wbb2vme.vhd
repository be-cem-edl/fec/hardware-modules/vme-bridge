-- SPDX-FileCopyrightText: 2022 CERN (home.cern)
--
-- SPDX-License-Identifier: CERN-OHL-W-2.0+

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.wishbone_pkg.all;
use work.vme64x_pkg.all;

entity tb_wbb2vme is
end;

architecture arch of tb_wbb2vme is
  signal clk, rst, rst_n, startup_rst : std_logic;
  signal wb_vme_tga,  wb_usr_tga, wb_dma_tga, wb_pci_tga : std_logic_vector(8 downto 0);
  signal wb_vme_in, wb_usr_in, wb_ctrl_in, wb_dma_in, wb_pci_in, wb_regs_in : t_wishbone_master_in;
  signal wb_dma2vme_in, wb_dma2pci_in : t_wishbone_master_in;
  signal wb_vme_out, wb_usr_out, wb_dma_out, wb_pci_out, wb_regs_out : t_wishbone_master_out;
  signal wb_dma2pci_out : t_wishbone_master_out;

  signal wb_dma_cyc_pci, wb_dma_cyc_vme : std_logic;
  signal wb_dma_cti, wb_pci_cti : std_logic_vector(2 downto 0);

  signal vme1_data_out : std_logic_vector(31 downto 0);
  signal vme1_addr_in, vme1_addr_out : std_logic_vector(31 downto 0);
  signal vme1_ds_out_n, vme1_ds_in_n : std_logic_vector(1 downto 0);
  signal vme1_ds_oe_n : std_logic;
  signal vme1_am_out : std_logic_vector(5 downto 0);
  signal vme1_wen_out : std_logic;
  signal vme1_iackn_out : std_logic;
  signal irq_i_n, irq_o_n : std_logic_vector(7 downto 1);
  signal vme1_as_o_n, vme1_as_oe_n : std_logic;
  signal sysresn, sysresin : std_logic;
  signal berrn, berrin : std_logic;
  signal vme1_dtackn: std_logic;
  signal slot01n : std_logic;
  signal sysfail_i_n, sysfail_o_n : std_logic;
  signal bbsyn, bbsyin : std_logic;
  signal bclr_i_n, bclr_o_n : std_logic;
  signal retry_i_n, retry_o_n, retry_oe_n : std_logic;
  signal br_i_n, br_o_n : std_logic_vector(3 downto 0);
  signal vme1_iackin_n, vme1_iackout_n : std_logic;
  signal acfailn : std_logic;
  signal bg_i_n, bg_o_n : std_logic_vector(3 downto 0);
  signal ga : std_logic_vector(4 downto 0);
  signal gap : std_logic;
  signal vme_berr, vme_mstr_busy : std_logic;
  signal vme1_d_dir, vme1_d_oe_n : std_logic;
  signal am_dir, am_oe_n : std_logic;
  signal vme1_a_dir, vme1_a_oe_n : std_logic;
  signal v2p_rst : std_logic;

  signal vme2_in : t_vme64x_in;
  signal vme2_out : t_vme64x_out;

  signal wb2_in : t_wishbone_master_in;
  signal wb2_out : t_wishbone_master_out;
  signal rst2_in_n, rst2_out_n : std_logic;
  signal irq2, iack2 : std_logic;

  signal sl2_rega : std_logic_vector(31 downto 0);
  signal sl2_pat_err : unsigned(31 downto 0);

  --  Simple bus for tb access to the host sram (by dma booster)
  signal hsram_addr : std_logic_vector(11 downto 2);
  signal hsram_we : std_logic;
  signal hsram_dati, hsram_dato : std_logic_vector(31 downto 0);

  --  Simple bus for tb access to the host sram (by dma pci)
  signal hpsram_addr : std_logic_vector(11 downto 2);
  signal hpsram_we : std_logic;
  signal hpsram_dati, hpsram_dato : std_logic_vector(31 downto 0);

  signal dma_irq : std_logic;
  signal done : boolean := false;

  --  VME bus
  signal vme_a : std_logic_vector(31 downto 1);
  signal vme_d : std_logic_vector(31 downto 0);
  signal vmeo_as_n, vme_as_n : std_logic;
  signal vme_am : std_logic_vector(5 downto 0);
  signal vme_write_n : std_logic;
  signal vmeo_ds_n, vme_ds_n : std_logic_vector(1 downto 0);
  signal vme_lword_n : std_logic;
  signal vmeo_iack_n, vme_iack_n : std_logic;
  signal vmeo_dtack_n, vme_dtack_n : std_logic;
  signal vme_iack_n_1_2 : std_logic;
  signal vme_rst_n : std_logic;

  subtype t_slv32 is std_logic_vector(31 downto 0);

  function gen_pat1 (addr : std_logic_vector(11 downto 2)) return t_slv32 is
  begin
    return x"f" & addr & "00" & x"0" & addr & "00";
  end gen_pat1;

  function swap32 (v : t_slv32) return t_slv32 is
  begin
    return v(7 downto 0) & v(15 downto 8) & v(23 downto 16) & v(31 downto 24);
  end swap32;
begin

  DUT: entity work.wbb2vme_top
    generic map (
      g_simulation => True,
      a16_reg_mapping => True,
      longadd_size => open,
      use_longadd => false
      )
    port map (
      clk => clk,
      rst => rst,
      startup_rst => startup_rst,
      postwr => open,
      vme_irq => open,
      berr_irq => open,
      locmon_irq => open,
      mailbox_irq => open,
      dma_irq => dma_irq,
      prevent_sysrst => '1',

      wbs_stb_i => wb_vme_out.stb,
      wbs_ack_o => wb_vme_in.ack,
      wbs_err_o => wb_vme_in.err,
      wbs_we_i => wb_vme_out.we,
      wbs_sel_i => wb_vme_out.sel,
      wbs_cyc_i => wb_vme_out.cyc,
      wbs_adr_i => wb_vme_out.adr,
      wbs_dat_o => wb_vme_in.dat,
      wbs_dat_i => wb_vme_out.dat,
      wbs_tga_i => wb_vme_tga,

      wbr_stb_i => wb_regs_out.stb,
      wbr_cyc_i => wb_regs_out.cyc,
      wbr_we_i  => wb_regs_out.we,
      wbr_sel_i => wb_regs_out.sel,
      wbr_adr_i => wb_regs_out.adr,
      wbr_dat_i => wb_regs_out.dat,
      wbr_dat_o => wb_regs_in.dat,
      wbr_ack_o => wb_regs_in.ack,
      wbr_err_o => wb_regs_in.err,

      wbm_dma_stb_o => wb_dma_out.stb,
      wbm_dma_ack_i => wb_dma_in.ack,
      wbm_dma_we_o => wb_dma_out.we,
      wbm_dma_cti => wb_dma_cti,
      wbm_dma_tga_o => wb_dma_tga,
      wbm_dma_err_i => wb_dma_in.err,
      wbm_dma_sel_o => wb_dma_out.sel,
      wbm_dma_cyc_vme => wb_dma_cyc_vme,
      wbm_dma_cyc_pci => wb_dma_cyc_pci,
      wbm_dma_adr_o => wb_dma_out.adr,
      wbm_dma_dat_o => wb_dma_out.dat,
      wbm_dma_dat_i => wb_dma_in.dat,

      pci_cyc_o => wb_pci_out.cyc,
      pci_stb_o => wb_pci_out.stb,
      pci_we_o  => wb_pci_out.we,
      pci_sel_o => wb_pci_out.sel,
      pci_tga_o => wb_pci_tga,
      pci_cti_o => wb_pci_cti,
      pci_adr_o => wb_pci_out.adr,
      pci_dat_o => wb_pci_out.dat,
      pci_dat_i => wb_pci_in.dat,
      pci_ack_i => wb_pci_in.ack,
      pci_err_i => wb_pci_in.err,

      va_o => vme1_addr_out,
      va_i => vme1_addr_in,
      vd_o => vme1_data_out,
      vd_i => vme_d,
      vam_o => vme1_am_out,
      vam_i => vme_am,
      writen_o => vme1_wen_out,
      writen_i => vme_write_n,
      iackn_o => vme1_iackn_out,
      iackn_i => vme_iack_n,
      irq_i_n => irq_i_n,
      irq_o_n => irq_o_n,
      as_o_n => vme1_as_o_n,
      as_oe_n => vme1_as_oe_n,
      as_i_n => vme_as_n,
      sysresn => sysresn,
      sysresin => sysresin,
      ds_o_n => vme1_ds_out_n,
      ds_i_n => vme1_ds_in_n,
      ds_oe_n => vme1_ds_oe_n,
      berrn => berrn,
      berrin => berrin,
      dtackn => vme1_dtackn,
      dtackin => vme_dtack_n,
      slot01n => slot01n,
      sysfail_i_n => sysfail_i_n,
      sysfail_o_n => sysfail_o_n,
      bbsyn => bbsyn,
      bbsyin => bbsyin,
      bclr_i_n => bclr_i_n,
      bclr_o_n => bclr_o_n,
      retry_i_n => retry_i_n,
      retry_o_n => retry_o_n,
      retry_oe_n => retry_oe_n,
      br_i_n => br_i_n,
      br_o_n => br_o_n,
      iackin => vme1_iackin_n,
      iackoutn => vme1_iackout_n,
      acfailn => acfailn,
      bg_i_n => bg_i_n,
      bg_o_n => bg_o_n,
      ga => ga,
      gap => gap,
      vme_berr => vme_berr,
      vme_mstr_busy => vme_mstr_busy,
      d_dir => vme1_d_dir,
      d_oe_n => vme1_d_oe_n,
      am_dir => am_dir,
      am_oe_n => am_oe_n,  --  Control am, write_n, iack
      a_dir => vme1_a_dir,
      a_oe_n => vme1_a_oe_n,
      v2p_rst => v2p_rst
      );

  inst_xvme64x_core: entity work.xvme64x_core
    generic map (
      g_clock_period => 16,
      g_decode_am => False,
      g_enable_cr_csr => False,
      g_user_csr_ext => False,
      g_vme32 => True,
      g_vme_2e => False,
      g_async_dtack => False,
      g_wb_granularity => BYTE,
      g_wb_mode => CLASSIC,
      g_manufacturer_id => x"aa_bb_cc",
      g_board_id => x"00_11_22_00",
      g_revision_id => x"00_00_00_01",
      g_program_id => x"00",
      g_ascii_ptr => open,
      g_beg_user_cr => open,
      g_end_user_cr => open,
      g_beg_cram => open,
      g_end_cram => open,
      g_beg_user_csr => open,
      g_end_user_csr => open,
      g_beg_sn => open,
      g_end_sn => open,
      g_decoder => open
      )
    port map (
      clk_i => clk,
      rst_n_i => rst2_in_n,
      rst_n_o => rst2_out_n,
      vme_i => vme2_in,
      vme_o => vme2_out,
      wb_i => wb2_in,
      wb_o => wb2_out,
      int_i => irq2,
      irq_ack_o => iack2,
      irq_level_i => "010",
      irq_vector_i => x"45",
      user_csr_addr_o => open,
      user_csr_data_i => open,
      user_csr_data_o => open,
      user_csr_we_o => open,
      user_cr_addr_o => open,
      user_cr_data_i => open
      );

  b_slave2: block
    signal mem32_addr             : std_logic_vector(11 downto 2);
    signal mem32_dati, mem32_dato : std_logic_vector(31 downto 0);
    signal mem32_we               : std_logic;
  begin
    inst_slave2: entity work.vme2_map
      port map (
        rst_n_i => rst2_in_n,
        clk_i => clk,
        wb_i => wb2_out,
        wb_o => wb2_in,
        rega_o => sl2_rega,
        mem32_addr_o => mem32_addr,
        mem32_data_i => mem32_dati,
        mem32_data_o => mem32_dato,
        mem32_wr_o => mem32_we
        );

    mem32_dati <= gen_pat1(mem32_addr);

    process (clk)
    begin
      if rising_edge(clk) then
        if rst = '1' then
          sl2_pat_err <= (others => '0');
        elsif mem32_we = '1' and gen_pat1(mem32_addr) /= mem32_dato then
          report "vme sl2: invalid pat at " & f_bits2string(mem32_addr & "00") & ", got " & f_bits2string(mem32_dato) & ", expect " & f_bits2string(gen_pat1(mem32_addr));
          sl2_pat_err <= unsigned(sl2_pat_err) + 1;
        end if;
      end if;
    end process;
  end block;

  --  Slot 2 (vme64x core)
  rst2_in_n <= not rst;
  vme2_in <= (rst_n => vme_rst_n,
    as_n => vme_as_n,
    write_n => vme_write_n,
    am => vme_am,
    ds_n => vme_ds_n,
    ga => b"1_11101",
    lword_n => vme_lword_n,
    data => vme_d,
    addr => vme_a,
    iack_n => vme_iack_n,
    iackin_n => vme_iack_n_1_2);

  inst_wbs_arb: entity work.wb_arb2
    port map (
      clk_i => clk,
      rst_n_i => rst_n,
      wb1_i => wb_usr_out,
      wb1_o => wb_usr_in,
      tga1_i => wb_usr_tga,
      wb2_i => wb_dma_out,
      wb2_o => wb_dma2vme_in,
      tga2_i => wb_dma_tga,
      wb_i => wb_vme_in,
      wb_o => wb_vme_out,
      tga_o => wb_vme_tga
      );
  wb_dma_out.cyc <= wb_dma_cyc_vme;
  wb_dma_in <= wb_dma2vme_in when wb_dma_cyc_vme = '1' else
    wb_dma2pci_in when wb_dma_cyc_pci = '1' else
    (dat => (others => 'X'), ack => '0', err => 'X', rty => 'X', stall => 'X');

  process
  begin
    clk <= '1';
    wait for 15 ns;
    clk <= '0';
    wait for 15 ns;
    if done then
      wait;
    end if;
  end process;

  rst <= '1', '0' after 40 ns;
  rst_n <= not rst;
  startup_rst <= rst;

  g_booster: if True generate
    --  PCI slave from booster (host memory).
    process (clk)
    begin
      if rising_edge(clk) then
        wb_pci_in.ack <= '0';
        if (wb_pci_out.stb and wb_pci_out.cyc) = '1' then
          if wb_pci_out.we = '1' then
            report "pci write [" & f_bits2string(wb_pci_out.adr) & "]=" & f_bits2string(wb_pci_out.dat);
          else
            report "pci read";
          end if;
          wb_pci_in.ack <= '1';
        end if;
      end if;
    end process;
    --  PCI slave from dma (host memory).
    inst_host_sram: entity work.tbsram
      generic map (
        g_addr_msb => 11
        )
      port map (
        clk_i => clk,
        rst_n_i => rst_n,
        wb_i => wb_pci_out,
        wb_cti_i => "000",
        wb_o => wb_pci_in,
        addr_i => hsram_addr,
        dat_o => hsram_dato,
        dat_i => hsram_dati,
        we_i => hsram_we
        );
  end generate;

  g_nobooster: if True generate
    --  PCI slave from dma (host memory).
    inst_host_sram: entity work.tbsram
      generic map (
        g_addr_msb => 11
        )
      port map (
        clk_i => clk,
        rst_n_i => rst_n,
        wb_i => wb_dma2pci_out,
        wb_cti_i => wb_dma_cti,
        wb_o => wb_dma2pci_in,
        addr_i => hpsram_addr,
        dat_o => hpsram_dato,
        dat_i => hpsram_dati,
        we_i => hpsram_we
        );
  end generate;
  wb_dma2pci_out <=
    (adr => wb_dma_out.adr,
    cyc => wb_dma_cyc_pci,
    stb => wb_dma_out.stb, sel => wb_dma_out.sel, we => wb_dma_out.we,
    dat => wb_dma_out.dat);

  --  VME to pci
  wb_ctrl_in.ack <= '0';

  br_i_n <= br_o_n;

  --  Ensure slot 1
  --  Auto system controller (VITA-1 5.8)
  bg_i_n <= b"0111";

  bbsyin <= bbsyn;

  berrin <= '1';

  vme_a <= vme2_out.addr when vme2_out.addr_oe_n = '0' and vme2_out.addr_dir = '1' else (others => 'Z');
  vme_a <= vme1_addr_out(31 downto 1) when vme1_a_oe_n = '0' and vme1_a_dir = '1' else (others => 'Z');

  vme_lword_n <= vme1_addr_out (0) when vme1_a_oe_n = '0' and vme1_d_dir = '1' else 'Z';
  vme_lword_n <= vme2_out.lword_n when vme2_out.addr_oe_n = '0' and vme2_out.addr_dir = '1' else 'Z';

  vme1_addr_in(31 downto 1) <= vme_a;
  vme1_addr_in(0) <= vme_lword_n;

  vme_d <= vme2_out.data when vme2_out.data_oe_n = '0' and vme2_out.data_dir = '1' else (others => 'Z');
  vme_d <= vme1_data_out when vme1_d_oe_n = '0' and vme1_d_dir = '1' else (others => 'Z');

  vmeo_as_n <= vme1_as_o_n when vme1_as_oe_n = '0' else 'Z';
  vmeo_as_n <= 'H';
  vme_as_n <= to_X01 (vmeo_as_n);

  vme_am <= vme1_am_out when am_oe_n = '0' else (others => 'Z');

  vme_write_n <= vme1_wen_out when am_oe_n = '0' else 'Z';

  vmeo_ds_n <= vme1_ds_out_n when vme1_ds_oe_n = '0' else (others => 'Z');
  vmeo_ds_n <= "HH";
  vme_ds_n <= to_X01 (vmeo_ds_n);

  vmeo_dtack_n <= '0' when vme1_dtackn = '0' else 'Z';
  vmeo_dtack_n <= vme2_out.dtack_n when vme2_out.dtack_oe = '1' else 'Z';
  vmeo_dtack_n <= 'H';
  vme_dtack_n <= to_x01(vmeo_dtack_n);

  vmeo_iack_n <= vme1_iackn_out when am_oe_n = '0' else 'Z';
  vmeo_iack_n <= 'H';
  vme_iack_n <= to_x01 (vmeo_iack_n);

  --  iack daisy chain
  vme1_iackin_n <= '1';
  vme_iack_n_1_2 <= vme1_iackout_n;

  p_tb: process
    constant TGA_A24D32  : std_logic_vector(8 downto 0)  := b"0_0000_01_00";
    constant REGS_DMASTA : std_logic_vector(31 downto 0) := x"0000_002c";

    --variable data : std_logic_vector(31 downto 0);
    procedure write_regs (adr : std_logic_vector (31 downto 0);
                          dat : std_logic_vector (31 downto 0))is
    begin
      wb_regs_out.adr <= adr;
      wb_regs_out.we <= '1';
      wb_regs_out.dat <= dat;
      wb_regs_out.sel <= "1111";
      wb_regs_out.cyc <= '1';
      wb_regs_out.stb <= '1';
      loop
        wait until rising_edge (clk);
        exit when wb_regs_in.ack = '1' or wb_regs_in.err = '1';
      end loop;
      assert wb_regs_in.ack = '1';
      wb_regs_out.stb <= '0';
      wb_regs_out.cyc <= '0';
      wait until rising_edge(clk);
    end write_regs;

    procedure read_regs (adr :     std_logic_vector (31 downto 0);
                         dat : out std_logic_vector (31 downto 0))is
    begin
      wb_regs_out.adr <= adr;
      wb_regs_out.we <= '0';
      wb_regs_out.sel <= "1111";
      wb_regs_out.cyc <= '1';
      wb_regs_out.stb <= '1';
      loop
        wait until rising_edge (clk);
        exit when wb_regs_in.ack = '1' or wb_regs_in.err = '1';
      end loop;
      assert wb_regs_in.ack = '1';
      dat := wb_regs_in.dat;
      wb_regs_out.stb <= '0';
      wb_regs_out.cyc <= '0';
      wait until rising_edge(clk);
    end read_regs;

    procedure write_wb (adr : std_logic_vector (31 downto 0);
      tga                   : std_logic_vector (8 downto 0);
      dat                   : std_logic_vector (31 downto 0);
      sel                   : std_logic_vector(3 downto 0))is
    begin
      wb_usr_out.adr <= adr;
      wb_usr_tga <= tga;
      wb_usr_out.we <= '1';
      wb_usr_out.dat <= dat;
      wb_usr_out.sel <= sel;
      wb_usr_out.cyc <= '1';
      wb_usr_out.stb <= '1';
      loop
        wait until rising_edge (clk);
        exit when wb_usr_in.ack = '1' or wb_usr_in.err = '1';
      end loop;
      assert wb_usr_in.ack = '1';
      wb_usr_out.stb <= '0';
      wb_usr_out.cyc <= '0';
      wait until rising_edge(clk);
    end write_wb;

    procedure read_wb (adr :     std_logic_vector (31 downto 0);
      tga                  :     std_logic_vector (8 downto 0);
      dat                  : out std_logic_vector (31 downto 0);
      sel                  :     std_logic_vector(3 downto 0))is
    begin
      wb_usr_out.adr <= adr;
      wb_usr_tga <= tga;
      wb_usr_out.we <= '0';
      wb_usr_out.sel <= sel;
      wb_usr_out.cyc <= '1';
      wb_usr_out.stb <= '1';
      loop
        wait until rising_edge (clk);
        exit when wb_usr_in.ack = '1' or wb_usr_in.err = '1';
      end loop;
      assert wb_usr_in.ack = '1';
      dat := wb_usr_in.dat;
      wb_usr_out.stb <= '0';
      wb_usr_out.cyc <= '0';
      wait until rising_edge(clk);
    end read_wb;

    procedure write_dbram (adr : std_logic_vector(7 downto 2); dat : t_slv32) is
    begin
      write_regs(x"0000_02" & adr & "00", dat);
    end write_dbram;

    variable data        : std_logic_vector(31 downto 0);
  begin
    wb_usr_out.stb <= '0';
    wb_regs_out.cyc <= '0';
    hpsram_we <= '0';
    hsram_we <= '0';
    wait until rising_edge(clk) and sysresn = '1';
    wait until rising_edge(clk);

    read_wb(x"0010_0000", TGA_A24D32, data, x"f");
    assert data = x"12345678" report "bad read at 0" severity failure;

    report "Write a register";
    write_wb(x"0010_0008", TGA_A24D32, x"1234_5678", x"f");

    --  Write to intid
    write_regs(x"0000_0004", x"1100_115a");
    read_regs(x"0000_0004", data);
    assert data = x"0000_005a" report "bad intid register read-back" severity failure;

    --  Test DMA: from VME to host
    --  Initialize DMA buffer 0
    write_dbram (b"0000_00", x"0001_0000");  -- Dest Addr
    write_dbram (b"0000_01", x"0010_1000");  -- Src Addr
    write_dbram (b"0000_10", x"0000_0005");  -- Size
    write_dbram (b"0000_11", x"000" & b"0010_0100_0001_1100_0001");  -- Flags

    --  Start DMA...
    report "Start DMA VME->PCIe (booster)";
    write_regs(REGS_DMASTA, x"000000_03");

    wait until dma_irq = '1';
    wait until rising_edge(clk);
    for i in 0 to 5 loop
      hsram_addr <= std_logic_vector(to_unsigned(i, 10));
      wait until rising_edge (clk);
      wait for 0 ns;
      report "hram[" & f_bits2string(hsram_addr) & "]=" & f_bits2string(hsram_dato);
      assert swap32(hsram_dato) = gen_pat1 (hsram_addr(11 downto 2)) report "invalid data in hram" severity failure;
    end loop;

    --  Test DMA: from host to VME
    --  Initialize DMA buffer 0
    write_dbram (b"0000_00", x"0010_1100");  -- Dest Addr
    write_dbram (b"0000_01", x"0001_0010");  -- Src Addr
    write_dbram (b"0000_10", x"0000_0206");  -- Size
    write_dbram (b"0000_11", x"000" & b"0100_0010_0001_1100_0001");  -- Flags

    --  Write correct patterns.
    for i in 0 to 16#206# loop
      hpsram_addr <= std_logic_vector(to_unsigned(16#4# + i, 10));
      hpsram_we <= '1';
      hpsram_dati <= swap32 (gen_pat1 (std_logic_vector(to_unsigned(16#40# + i, 10))));
      wait until rising_edge(clk);
      report "write pattern " & f_bits2string(hpsram_dati) & " at " & f_bits2string(hpsram_addr & "00");
    end loop;
    hpsram_we <= '0';

    --  Start DMA (and clear irq)...
    assert sl2_pat_err = 0 report "no pattern errors expected" severity failure;
    write_regs(REGS_DMASTA, x"000000_07");
    wait until dma_irq = '1';

    assert sl2_pat_err = 0 report "pattern errors after DMA" severity failure;

    report "done";
    wait for 200 ns;
    done <= true;
    wait;
  end process;
end arch;