-- SPDX-FileCopyrightText: 2022 CERN (home.cern)
--
-- SPDX-License-Identifier: CERN-OHL-W-2.0+

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.wishbone_pkg.all;
entity tbsram is
  generic (
    g_addr_msb : natural := 11
  );
  port (
    clk_i : in std_logic;
    rst_n_i : in std_logic;

    wb_i : in t_wishbone_slave_in;
    wb_o : out t_wishbone_slave_out;
    wb_cti_i : in std_logic_vector(2 downto 0) := "000";

    addr_i : in std_logic_vector(g_addr_msb downto 2);
    dat_o : out std_logic_vector(31 downto 0);
    dat_i : in std_logic_vector(31 downto 0);
    we_i : in std_logic
    );
end tbsram;

architecture arch of tbsram is
  type t_state is (S_IDLE, S_CLASSIC, S_BURST);
  signal s_state : t_state;
  signal s_addr : natural;
begin
  p_ssram: process (clk_i)
    type t_memory is array(natural range <>) of std_logic_vector(31 downto 0);

    variable mem  : t_memory(0 to 2**(g_addr_msb + 1 - 2) - 1);
    variable addr, usr_addr : natural;
    variable state : t_state;
  begin
    if rising_edge(clk_i) then
      wb_o.ack <= '0';
      if rst_n_i = '0' then
        state := S_IDLE;
      elsif (wb_i.stb and wb_i.cyc) = '1' then
        case state is
          when S_IDLE =>
            addr := to_integer(unsigned(wb_i.adr(g_addr_msb downto 2)));
            wb_o.ack <= '1';
          when S_BURST =>
            addr := addr + 1;
          when S_CLASSIC =>
            wb_o.ack <= '0';
            null;
        end case;
        case state is
          when S_IDLE | S_BURST =>
            if wb_i.we = '1' then
              for i in 0 to 3 loop
                if wb_i.sel(i) = '1' then
                  mem(addr)(8*i + 7 downto 8*i) := wb_i.dat(8*i+7 downto 8*i);
                end if;
              end loop;
            else
              wb_o.dat <= mem(addr);
            end if;
            wb_o.ack <= '1';
          when S_CLASSIC =>
            wb_o.ack <= '0';
        end case;
        if wb_cti_i = "010" then
          state := S_BURST;
        else
          --  Ensure ack is set for only one cycle.
          state := S_CLASSIC;
        end if;
      else
        state := S_IDLE;
      end if;

      s_state <= state;
      s_addr <= addr;

      --  User port
      usr_addr := to_integer(unsigned(addr_i));
      dat_o <= mem(usr_addr);
      if we_i = '1' then
        mem(usr_addr) := dat_i;
      end if;
    end if;
  end process;
end arch;
