-- SPDX-FileCopyrightText: 2022 CERN (home.cern)
--
-- SPDX-License-Identifier: CERN-OHL-W-2.0+

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.wishbone_pkg.all;

entity wb_arb2 is
  port (
    clk_i : in std_logic;
    rst_n_i : in std_logic;

    wb1_i : in t_wishbone_slave_in;
    wb1_o : out t_wishbone_slave_out;
    tga1_i : in std_logic_vector(8 downto 0);

    wb2_i : in t_wishbone_slave_in;
    wb2_o : out t_wishbone_slave_out;
    tga2_i : in std_logic_vector(8 downto 0);

    wb_i : in t_wishbone_master_in;
    wb_o : out t_wishbone_master_out;
    tga_o : out std_logic_vector(8 downto 0)
  );
end wb_arb2;

architecture arch of wb_arb2 is
  type t_state is (S_IDLE1, S_IDLE2, S_PORT1, S_PORT2, S_DONE1, S_DONE2);
  signal state : t_state;
begin
  process (clk_i)
  begin
    if rising_edge(clk_i) then
      wb1_o.ack <= '0';
      wb1_o.rty <= '0';
      wb1_o.err <= '0';
      wb1_o.stall <= '0';

      wb2_o.ack <= '0';
      wb2_o.rty <= '0';
      wb2_o.err <= '0';
      wb2_o.stall <= '0';

      wb_o.cyc <= '0';
      wb_o.stb <= '0';
      if rst_n_i = '0' then
        state <= S_IDLE1;
      else
        case state is
          when S_IDLE1 =>
            if (wb1_i.cyc and wb1_i.stb) = '1' then
              wb_o  <= wb1_i;
              tga_o <= tga1_i;
              state <= S_PORT1;
            elsif (wb2_i.cyc and wb2_i.stb) = '1' then
              wb_o  <= wb2_i;
              tga_o <= tga2_i;
              state <= S_PORT2;
            end if;
          when S_IDLE2 =>
            if (wb2_i.cyc and wb2_i.stb) = '1' then
              wb_o  <= wb2_i;
              tga_o <= tga2_i;
              state <= S_PORT2;
            elsif (wb1_i.cyc and wb1_i.stb) = '1' then
              wb_o  <= wb1_i;
              tga_o <= tga1_i;
              state <= S_PORT1;
            end if;
          when S_PORT1 =>
            wb_o  <= wb1_i;
            wb1_o <= wb_i;
            if (wb_i.ack or wb_i.err) = '1' then
              wb_o.stb <= '0';
              state <= S_DONE1;
            end if;
          when S_PORT2 =>
            wb_o  <= wb2_i;
            wb2_o <= wb_i;
            if (wb_i.ack or wb_i.err) = '1' then
              wb_o.stb <= '0';
              state <= S_DONE2;
            end if;
          when S_DONE1 =>
            if wb1_i.cyc = '0' then
              state <= S_IDLE2;
            else
              wb_o.cyc <= '1';
              if (wb_i.ack or wb_i.err) = '0' then
                state <= S_IDLE2;
              end if;
            end if;
          when S_DONE2 =>
            if wb2_i.cyc = '0' then
              state <= S_IDLE1;
            else
              wb_o.cyc <= '1';
              if (wb_i.ack or wb_i.err) = '0' then
                state <= S_IDLE1;
              end if;
            end if;
        end case;
      end if;
    end if;
  end process;
end arch;
