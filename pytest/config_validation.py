# SPDX-License-Identifier: LGPL-2.1-or-later
# SPDX-FileCopyrightText: 2022 CERN


import os


class CheckConfig():
    """Configuration check to see if we can even start doing real tests"""

    _err_msg = ""
    _sbc_name = ""

    def _pcie_vendor_device_id():
        """Our VME driver works only with known PCIe to VME bridges:"""
        valid_bridges = 0
        for vendor, device, sbc_name in [(0x10e3, 0x0148, "MenA20"),   # TSI148
                                         (0x1a88, 0x4d45, "MenA25")]:  # MEN PLDZ
            cmd = "lspci -d {:04x}:{:04x}".format(vendor, device)
            with os.popen(cmd) as out:
                valid_bridges += len(out.readlines())
                if valid_bridges != 0:
                    CheckConfig._sbc_name = sbc_name
                    break

        if valid_bridges == 0:
            CheckConfig._err_msg += 'Not found supported PCIe bridges, '

    def _driver_loaded():
        """A VME driver must be loaded for this test suite to work"""
        if not os.path.isdir("/sys/bus/vme"):
            CheckConfig._err_msg += 'VME driver not loaded, '

    @staticmethod
    def is_valid():
        CheckConfig._err_msg = ""
        CheckConfig._pcie_vendor_device_id()
        CheckConfig._driver_loaded()
        return True if len(CheckConfig._err_msg) == 0 else False

    @staticmethod
    def get_err_msg():
        return 'Errors: ' + CheckConfig._err_msg

    @staticmethod
    def get_sbc_name():
        return CheckConfig._sbc_name
