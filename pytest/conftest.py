# SPDX-License-Identifier: LGPL-2.1-or-later
# SPDX-FileCopyrightText: 2022 CERN

import pytest
import logging

from PyVME.PyVmeDevice import PyVmeDevice
from PyVME.PyVmeDevice import PyVme

vdev_param = {
    "am": 0x39,
    "base_addr": 0x0,
    "size": 0x80000,
    "devname": "fmc-svec-a24",
    "slot": 1,
    "f0": {"size": 0x80000, 'am': 0x09, 'ader': 0x0},      # function 0 A32
    "f1": {"size": 0x80000, 'am': 0x39, 'ader': 0x0},      # function 1 A24
    "f2": {"size": 0x2000, 'am': 0x029, 'ader': 0x0},      # function 2 A16
    "cr/csr": {"size": 0x80000, 'am': 0x2f, 'ader': 0x0},  # CR/CSR space:
}


# when the svec is flashed with the bistream 'svec_vmecore_test_top.bit', the
# SVEC is configured this way:
# Function F0: A32 addressing mode
# Function F1: A24 addressing mode
# Function F2: A16 addressing mode
# Supported VME configuration:
all_mapping_params = [  # used by several tests
    pytest.param({'func': 'f0', 'dwidth': 32}, id="A32/D32 mapping",),
    pytest.param({'func': 'f1', 'dwidth': 16}, id="A24/D16 mapping",),
    pytest.param({'func': 'f1', 'dwidth': 32}, id="A24/D32 mapping",),
    pytest.param({'func': 'f2', 'dwidth': 16}, id="A16/D16 mapping",),
    pytest.param({'func': 'f2', 'dwidth': 32}, id="A16/D32 mapping",),
]


def init_ader(slot):
    # Set module base address
    vdev_param["base_addr"] = vdev_param["size"] * slot
    # ADER for func 0, A32
    addr = (vdev_param["f0"]["size"] << 5) * slot
    vdev_param["f0"]["ader"] = addr
    # ADER for func 1, A24
    addr = vdev_param["f1"]["size"] * slot
    vdev_param["f1"]["ader"] = addr
    # ADER for func 2, A16
    # As the size of F2 is 8KB, at most 8 boards can be mapped
    # Use modulo to support more boards through aliasing
    addr = vdev_param["f2"]["size"] * (slot % 8)
    vdev_param["f2"]["ader"] = addr
    # ADER for cr/csr
    addr = vdev_param["cr/csr"]["size"] * slot
    vdev_param["cr/csr"]["ader"] = addr


def _create_vdev(slot, irq_level, irq_vector):
    vdev = PyVmeDevice(slot)
    try:
        vdev.unregister()
        vdev.register(vdev_param["am"],
                      vdev_param["base_addr"],
                      vdev_param["size"],
                      irq_vector,
                      irq_level,
                      vdev_param["devname"])
        # set func 0 ADER
        vdev.ader_set(0, vdev_param["f0"]["ader"], vdev_param["f0"]["am"])
        # set func 1 ADER
        vdev.ader_set(1, vdev_param["f1"]["ader"], vdev_param["f1"]["am"])
        # set func 2 ADER
        vdev.ader_set(2, vdev_param["f2"]["ader"], vdev_param["f2"]["am"])
    except Exception as e:
        vdev.unregister()
        raise e
    return vdev


IRQ_LEVEL_DEFAULT = 2
IRQ_VECTOR_DEFAULT = 0xD9
IRQ_LEVEL_OFFSET = 0x7ff5b
IRQ_VECTOR_OFFSET = 0x7ff5f


@pytest.fixture(scope="class")
def vdev_class_scope(svec_slot):
    vdev = _create_vdev(svec_slot[0], IRQ_LEVEL_DEFAULT, IRQ_VECTOR_DEFAULT)
    yield vdev
    vdev.unregister()


@pytest.fixture(scope="function")
def irq_level():
    return IRQ_LEVEL_DEFAULT


@pytest.fixture(scope="function")
def irq_vector():
    return IRQ_VECTOR_DEFAULT


@pytest.fixture(scope="function")
def vdev_func_scope(svec_slot, irq_level, irq_vector):
    vdev = _create_vdev(svec_slot[0], irq_level, irq_vector)
    # Program SVEC HW with irq_level and irq_vector
    mm, desc = create_mmap_buf('cr/csr', 0x7FFFF)
    mm[IRQ_LEVEL_OFFSET] = irq_level
    mm[IRQ_VECTOR_OFFSET] = irq_vector
    # HW is programmed, unmap the CR/CSR window
    PyVme.unmap(mm, desc, 1)
    yield vdev
    vdev.unregister()


@pytest.fixture(scope="session")
def vdev_cfg():
    return vdev_param


def create_mmap_buf(func, sz, dwidth=32):
    addr_base = vdev_param[func]["ader"]
    am = vdev_param[func]['am']
    desc = PyVme.VME_MAPPING(data_width=dwidth, am=am, sizel=sz,
                             vme_addrl=addr_base)
    return PyVme.mmap(desc, 1), desc


@pytest.fixture(scope="class")
def f1_mmap_buf():
    mm, desc = create_mmap_buf('f1', 0x1000)
    yield mm
    PyVme.unmap(mm, desc, 1)


@pytest.fixture(scope="function")
def skip_if_previous_test_case_failed(request, failed=set()):
    key = request.node.name.split("[")[0]
    failed_before = request.session.testsfailed
    if key in failed:
        pytest.skip("previous test {} failed".format(key))
    yield
    failed_after = request.session.testsfailed
    if failed_before != failed_after:
        failed.add(key)


@pytest.fixture(scope="session")
def logger():
    return logging.getLogger(__name__)


# Execute this at the beginning
@pytest.fixture(scope="session", autouse=True)
def svec_init(svec_slot):
    # program SVEC in terms of ADER (F0, F1, F2) + IRQ level & vector
    init_ader(svec_slot[0])  # Init vdev_param
    vdev = _create_vdev(svec_slot[0], IRQ_LEVEL_DEFAULT, IRQ_VECTOR_DEFAULT)
    # Program SVEC HW with defualt irq_level and irq_vector
    mm, desc = create_mmap_buf('cr/csr', 0x7FFFF)
    mm[IRQ_LEVEL_OFFSET] = IRQ_LEVEL_DEFAULT
    mm[IRQ_VECTOR_OFFSET] = IRQ_VECTOR_DEFAULT
    # HW is programmed, unmap the CR/CSR window and unregister device
    PyVme.unmap(mm, desc, 1)
    vdev.unregister()


@pytest.fixture(scope='session', autouse=True)
def svec_slot(request):
    return request.config.getoption("--slot")


def pytest_addoption(parser):
    parser.addoption("--slot", type=int, action="append", choices=range(1, 22),
                     required=True, help="A VME slot to be used for testing")
