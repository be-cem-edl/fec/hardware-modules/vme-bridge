// SPDX-FileCopyrightText: 2023 CERN (home.cern)
//
// SPDX-License-Identifier: GPL-2.0-or-later

#include <linux/io.h>
#include <linux/module.h>
#include <vmebus/vmebus.h>

#define DRV_NAME "svec_vme_test"

static char irq_dev_name[] = DRV_NAME;
static int remap_res_idx = 1; /* Function 1 A24/D32 0x39 */
static int intc_sr_reg_offset = 0x824;
static int irq_freq_reg_offset = 0x828;

struct svec_vme_test_dev {
	void __iomem *bar1_remap;
	int intc_read_count;
};

/*
 * Attribute to define the number of SCT (single cycle transfer) included in
 * the IRQ handler to measure the CPU performance.
 */
static ssize_t irq_read_count_store(struct device *pdev,
				    struct device_attribute *attr,
				    const char *buf, size_t count)
{
	struct svec_vme_test_dev *test_dev = dev_get_drvdata(pdev);
	int v, i;

	i = sscanf(buf, "%i", &v);
	if (i != 1)
		return -EINVAL;
	test_dev->intc_read_count = v;
	return count;
}

static ssize_t irq_read_count_show(struct device *pdev,
				   struct device_attribute *attr, char *buf)
{
	struct svec_vme_test_dev *test_dev = dev_get_drvdata(pdev);

	return snprintf(buf, PAGE_SIZE, "%d\n", test_dev->intc_read_count);
}

static DEVICE_ATTR(irq_read_count, S_IRWXU | S_IRGRP | S_IROTH,
	   	   irq_read_count_show, irq_read_count_store);

static int vme_irq_handler(void *arg) {
	struct svec_vme_test_dev *test_dev = (struct svec_vme_test_dev *)arg;
	char __iomem *addr;
	uint32_t intc_sr_val;
	volatile uint32_t val; /* prevent from GCC optimization */
	int i;

	addr = test_dev->bar1_remap + intc_sr_reg_offset;
	/* ack interrupt */
	intc_sr_val = ioread32be(addr);

	/* Additional VME read to simulate an INT handler that lasts longer */
	addr = test_dev->bar1_remap + irq_freq_reg_offset;
	for (i = 0; i < test_dev->intc_read_count; ++i) {
		val = ioread32be(addr);
	}

	if (intc_sr_val == 0)
		return IRQ_NONE;
	return IRQ_HANDLED;
}

static int svec_remap(struct vme_dev *vdev, struct svec_vme_test_dev *test_dev)
{
	struct resource *r;

	r = &vdev->resource[remap_res_idx];
	test_dev->bar1_remap = ioremap(r->start, resource_size(r));
	return 0;
}

static int svec_irq_init(struct vme_dev *vdev, struct svec_vme_test_dev *test_dev)
{
	return vme_request_irq(vdev->irq_vector, vme_irq_handler,
			       test_dev, irq_dev_name);
}

static int svec_probe(struct device *pdev, unsigned int ndev)
{
	struct svec_vme_test_dev *test_dev;
	struct vme_dev *vdev = to_vme_dev(pdev);
	int err;

	test_dev = devm_kzalloc(pdev, sizeof(struct svec_vme_test_dev),
				GFP_KERNEL);
	if (test_dev == NULL)
		return -ENOMEM;
	dev_set_drvdata(pdev, test_dev);

	/* Remap BAR 1 to ack Interrupt source status register */
	svec_remap(vdev, test_dev);

	/* setup IRQ if needed */
	err = svec_irq_init(vdev, test_dev);
	if (err) {
		dev_info(pdev, "registration failed.\n");
		return err;
	}

	/*
	 * Create sysfs interface to tune number of read access in the iRQ
	 * handle
	 */
	err = device_create_file(pdev, &dev_attr_irq_read_count);
	if (err) {
		dev_info(pdev, "Creating sysfs attribute failed.\n");
		return err;
	}

	dev_info(pdev, "registration OK.\n");
	return 0;
}

static int svec_remove(struct device *pdev, unsigned int dev_no)
{
	struct vme_dev *vdev = to_vme_dev(pdev);

	vme_free_irq(vdev->irq_vector);
	device_remove_file(pdev, &dev_attr_irq_read_count);
	dev_set_drvdata(pdev, NULL);
	return 0;
}

static const struct vme_device_id svec_vme64x_ids[] = {
	{
		.name = "svec_vme_test",
		.vendor = 0x00080030,
		.device = 0x00000198,
		.revision = 0x00000001,
	},
	{"\0", 0, 0, 0},
};

static struct vme_driver svec_vme_drv = {
	.probe = svec_probe,
	.remove = svec_remove,
	.driver = {
		.owner = THIS_MODULE,
		.name = DRV_NAME
	},
	.id_table = svec_vme64x_ids,
};

static int __init svec_init_module(void)
{
	return vme_register_driver(&svec_vme_drv, 0);
}

static void __exit svec_exit_module(void)
{
	vme_unregister_driver(&svec_vme_drv);
}

module_init(svec_init_module);
module_exit(svec_exit_module);

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("svec_vme_test driver");
MODULE_VERSION("");
