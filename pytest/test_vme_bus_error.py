# SPDX-License-Identifier: LGPL-2.1-or-later
# SPDX-FileCopyrightText: 2022 CERN


import pytest
import random
import subprocess
import os
import re
import time

from conftest import all_mapping_params
from config_validation import CheckConfig
from conftest import create_mmap_buf
from PyVME.PyVmeDevice import PyVme


def proc_berr_count():
    with open("/proc/vme/interrupts", "r") as f_int:
        proc_int = f_int.read()
    m = re.findall(r'(BUSERR\s+|VERR\s+)([0-9]+)', proc_int)
    return 0 if len(m) == 0 else int(m[0][1])


def clear_dmesg():
    return os.system(f'sudo dmesg -c')


def get_dmesg_bus_error():
    cmd = 'dmesg'
    p = subprocess.run([cmd], capture_output=True, shell=True,
                       encoding='utf-8')
    p.check_returncode()
    msg = p.stdout
    m = re.findall(r'(VME Bridge: Bus error\s+).+(Address\s+0x[0-9]+\s+am\s+0x[0-9]+)', msg)
    count = len(m)
    addr = [el[1].split()[1] for el in m]  # contains all addresses
    am = [el[1].split()[3] for el in m]  # contains all AddressModifier
    return count, addr, am


GEN_BUS_ERR_REG_OFFSET = 0x814


# All tests are skipped, in case the configuration is not valid
@pytest.mark.skipif(CheckConfig.is_valid() is not True,
                    reason=CheckConfig.get_err_msg())
class TestVmebusError():
    """VME bus test IRQ"""

    def test_bus_error(self, vdev_cfg, id="generate bus error"):
        """ The VME BUS test bitstream has the capability by writing into a
        specific register to generate a VME BUS error. This test writes a n
        (random) times into the register and assert the number of VME BUS error
        and the reported VME bus error address and address modifier
        """
        clear_dmesg()

        nb = random.randint(1, 10)
        args = all_mapping_params[2].values[0]  # A24/D32 mapping args
        mm, desc = create_mmap_buf(args['func'], 0x1000, dwidth=args['dwidth'])

        proc_bus_err_count = {}
        proc_bus_err_count['before'] = proc_berr_count()
        # Number of BUS error should correspond to random number (nb)
        for i in range(0, nb):
            time.sleep(0.1)
            at = GEN_BUS_ERR_REG_OFFSET
            mm[at:at + 4] = (1).to_bytes(4, 'big')
        # Release mapping
        PyVme.unmap(mm, desc, 1)
        time.sleep(0.1)
        proc_bus_err_count['after'] = proc_berr_count()

        proc_err_count = proc_bus_err_count['after'] - proc_bus_err_count['before']
        dmesg_err_count, addr, am = get_dmesg_bus_error()
        assert proc_err_count == dmesg_err_count  # number of BUS Error
        assert len(set(addr)) == 1  # identical BUS Error Address
        assert len(set(am)) == 1  # identical BUS Error address modifier
        assert addr[0] == hex(GEN_BUS_ERR_REG_OFFSET + vdev_cfg[args['func']]['ader'])
        assert am[0] == hex(vdev_cfg[args['func']]['am'])
