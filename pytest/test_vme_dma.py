# SPDX-License-Identifier: LGPL-2.1-or-later
# SPDX-FileCopyrightText: 2022 CERN


import pytest
import ctypes
import time
import re
import random

from config_validation import CheckConfig
from conftest import all_mapping_params
from conftest import create_mmap_buf
from PyVME.PyVmeDevice import PyVme


RAM_PATTERN_OFFSET = 0x1000
SRAM_OFFSET = 0x0
SRAM_SIZE = 0x800  # size in bytes
WRITE_ERROR_COUNT_REG_OFFSET = 0x810
BLT_BLOCK_SIZE = 256
MBLT_BLOCK_SIZE = 2048
PAGE_ALIGNMENT = 4096


""" All possible DMA settings.
Please note that the A16 DMA is marked 'XFAIL', because the data integrity teat
in A16 DMA is failing for the MenA20. It looks like an issue in the MenA25 HDL
code.
"""
dma_settings = [
    pytest.param({'func': 'f0', 'dwidth': 32, 'am': 0x09, 'buf_sz': 0x3C00,
                  'data_rate': {
                      'MenA25': {'read': 9, 'write': 8},
                      'MenA20': {'read': 14.5, 'write': 13.5},
                  }}),
    pytest.param({'func': 'f0', 'dwidth': 32, 'am': 0x0B, 'buf_sz': 0x3C00,
                  'data_rate': {
                      'MenA25': {'read': 13.5, 'write': 8.5},
                      'MenA20': {'read': 11, 'write': 11},
                  }}),
    pytest.param({'func': 'f0', 'dwidth': 32, 'am': 0x08, 'buf_sz': 0x3C00,
                  'data_rate': {
                      'MenA25': {'read': 30.5, 'write': 15.5},
                      'MenA20': {'read': 25, 'write': 19},
                  }}),
    pytest.param({'func': 'f1', 'dwidth': 32, 'am': 0x39, 'buf_sz': 0x3C00,
                  'data_rate': {
                      'MenA25': {'read': 9, 'write': 8},
                      'MenA20': {'read': 14.5, 'write': 13.5},
                  }}),
    pytest.param({'func': 'f1', 'dwidth': 32, 'am': 0x3B, 'buf_sz': 0x3C00,
                  'data_rate': {
                      'MenA25': {'read': 13.5, 'write': 8.5},
                      'MenA20': {'read': 11.2, 'write': 11.2},
                  }}),
    pytest.param({'func': 'f1', 'dwidth': 32, 'am': 0x38, 'buf_sz': 0x3C00,
                  'data_rate': {
                      'MenA25': {'read': 30.5, 'write': 15.5},
                      'MenA20': {'read': 25, 'write': 19},
                  }}),
    pytest.param({'func': 'f1', 'dwidth': 16, 'am': 0x39, 'buf_sz': 0x3C00,
                  'data_rate': {
                      'MenA25': {'read': 4.5, 'write': 4.5},
                      'MenA20': {'read': 8, 'write': 8},
                  }}),
    pytest.param({'func': 'f1', 'dwidth': 16, 'am': 0x3B, 'buf_sz': 0x3C00,
                  'data_rate': {
                      'MenA25': {'read': 5.5, 'write': 4.5},
                      'MenA20': {'read': 7, 'write': 7},
                  }}),
    pytest.param({'func': 'f2', 'dwidth': 16, 'am': 0x29, 'buf_sz': 0x400,
                  'data_rate': {
                      'MenA25': {'read': 3.5, 'write': 3},
                      'MenA20': {'read': 3, 'write': 3},
                  }}, marks=[pytest.mark.xfail]),
    pytest.param({'func': 'f2', 'dwidth': 32, 'am': 0x29, 'buf_sz': 0x400,
                  'data_rate': {
                      'MenA25': {'read': 5, 'write': 4.5},
                      'MenA20': {'read': 4.5, 'write': 4},
                  }}, marks=[pytest.mark.xfail]),
]

# parametrize DMA BLT only: used by 'novmeinc' testing
dma_blt_novmeinc_settings = [
    dma_settings[1], dma_settings[4], dma_settings[7],
]


# parametrize DMA MBLT only: used by 'novmeinc' testing
dma_mblt_novmeinc_settings = [
    dma_settings[2], dma_settings[5],
]


am_2_string = {
    0x09: 'A32 DMA',
    0x0B: 'A32 DMA BLT',
    0x08: 'A32 DMA MBLT',
    0x39: 'A24 DMA',
    0x3B: 'A24 DMA BLT',
    0x38: 'A24 DMA MBLT',
    0x29: 'A16 DMA',
}


def _dma_settings_id(args, direction, perf=False):
    sbc_name = CheckConfig.get_sbc_name()
    msg = '{} {}/D{} '.format(sbc_name, am_2_string[args['am']],
                              args['dwidth'])
    if perf:
        msg += '- expected data-transfer rate > '
        msg += '{} MB/s'.format(args['data_rate'][sbc_name][direction])
    return msg


def dma_read_perf_id_fn(args):
    return _dma_settings_id(args, 'read', True)


def dma_write_perf_id_fn(args):
    return _dma_settings_id(args, 'write', True)


def dma_read_integrity_id_fn(args):
    return _dma_settings_id(args, 'read')


def dma_write_integrity_id_fn(args):
    return _dma_settings_id(args, 'write')


# All tests are skipped, in case the configuration is not valid
@pytest.mark.skipif(CheckConfig.is_valid() is not True,
                    reason=CheckConfig.get_err_msg())
class TestVmebusDMA():
    """VME bus test DMA"""

    def _vme_int_dma_count(self):
        with open("/proc/vme/interrupts", "r") as f_int:
            proc_int = f_int.read()
        m = re.findall(r'(DMA[0-9]?\s+)([0-9]+)', proc_int)
        return sum([int(val[1]) for val in m])

    def _assert_perf(self, start, stop, args, loop, direction, logger):
        elapsed_time = stop - start  # in sec
        data_rate = (args['buf_sz'] * loop) / elapsed_time  # Bytes/sec
        data_rate = data_rate / 1000000  # MB/s
        sbc_name = CheckConfig.get_sbc_name()
        ref_data_rate = args['data_rate'][sbc_name][direction]
        msg = "measured data transfer rate {}MB/s, minimum expected {}MB/s".format(data_rate, ref_data_rate)
        assert data_rate >= ref_data_rate, msg
        logger.info(msg)

    def _aligned_array(self, alignment, size):
        """ Allocate a buffer aligned with the provided alignment constraint
        """
        mask = alignment - 1
        sz = size + mask
        buf = (ctypes.c_ubyte * sz)()
        misalignment = ctypes.addressof(buf) & mask
        if misalignment:
            offset = alignment - misalignment
        else:
            offset = 0
        return (ctypes.c_ubyte * size).from_buffer(buf, offset)

    @pytest.mark.parametrize("args", dma_settings, ids=dma_read_integrity_id_fn)
    def test_dma_read_integrity(self, vdev_cfg, args, f1_mmap_buf):
        """The test evaluates the DMA read data integrity. Write data using
        mapping in the RAM and read back using DMA.
        board.
        """
        addr_base = vdev_cfg[args['func']]['ader']
        ref_buf = (ctypes.c_ubyte * SRAM_SIZE)()
        for i in range(SRAM_SIZE):
            ref_buf[i] = random.randrange(0, 0xFF, 1)
            f1_mmap_buf[SRAM_OFFSET + i] = ref_buf[i]

        buf = (ctypes.c_ubyte * SRAM_SIZE)()
        PyVme.read_dma(args['am'], addr_base + SRAM_OFFSET,
                       ctypes.addressof(buf), len(buf), args['dwidth'], 0)

        for val1, val2 in zip(ref_buf, buf):
            assert val1 == val2

    @pytest.mark.parametrize("args", dma_settings, ids=dma_read_integrity_id_fn)
    def test_dma_write_integrity(self, vdev_cfg, args, f1_mmap_buf):
        """The test evaluates the DMA write data integrity. Write data using
        DMA in the RAM and read back using mapping.
        board.
        """
        addr_base = vdev_cfg[args['func']]['ader']
        ref_buf = (ctypes.c_ubyte * SRAM_SIZE)()
        for i in range(SRAM_SIZE):
            ref_buf[i] = random.randrange(0, 0xFF, 1)
        PyVme.write_dma(args['am'], addr_base + SRAM_OFFSET,
                        ctypes.addressof(ref_buf), len(ref_buf), args['dwidth'], 0)

        buf = (ctypes.c_ubyte * SRAM_SIZE)()
        for i in range(SRAM_SIZE):
            buf[i] = f1_mmap_buf[SRAM_OFFSET + i]

        for val1, val2 in zip(ref_buf, buf):
            assert val1 == val2

    def _novmeinc_read_integrity(self, vdev_cfg, args, block_sz):
        """Novmeinc read data integrity consists to write into the SVEC RAM
        random values using DMA with 'novmeinc" flag diabled, and read back the
        SVEC RAM with 'novmeinc' enabled.
        Therefore, each block size slice of teh read buffer should be
        equal to the first block size slice of the write buffer
        """
        addr_base = vdev_cfg[args['func']]['ader']
        # NOVMEINC access requires page aligned buffer: alignment=4096
        read_buf = self._aligned_array(4096, SRAM_SIZE)
        write_buf = self._aligned_array(4096, SRAM_SIZE)
        for i in range(SRAM_SIZE):
            write_buf[i] = random.randrange(0, 0xFF, 1)
        try:
            # Read once RAM pattern and fill the bufer
            novmeinc = 0  # disable 'novmeinc' flag
            PyVme.write_dma(args['am'], addr_base + SRAM_OFFSET,
                            ctypes.addressof(write_buf),
                            len(write_buf), args['dwidth'], novmeinc)

            novmeinc = 1  # enable 'novmeinc' flag
            PyVme.read_dma(args['am'], addr_base + SRAM_OFFSET,
                           ctypes.addressof(read_buf), len(read_buf),
                           args['dwidth'], novmeinc)
            loop = SRAM_SIZE // block_sz
            # each block size slice of teh read buffer == first block size
            # slice of the write buffer
            for i in range(0, loop):
                idx1 = i * block_sz
                idx2 = (i + 1) * block_sz
                for val1, val2 in zip(write_buf[0:block_sz], read_buf[idx1:idx2]):
                    assert val1 == val2

        except OSError as error:
            assert False, str(error)

    @pytest.mark.parametrize("args", dma_blt_novmeinc_settings, ids=dma_read_integrity_id_fn)
    def test_dma_blt_novmeinc_read_integrity(self, vdev_cfg, args, f1_mmap_buf, logger):
        """ This test checks the data integrity for DMA BLT when 'novmeinc'
        flag is enabled. As it is BLT the block transfer size is 256bytes.
        """
        block_sz = 256
        self._novmeinc_read_integrity(vdev_cfg, args, block_sz)

    @pytest.mark.parametrize("args", dma_mblt_novmeinc_settings, ids=dma_read_integrity_id_fn)
    def test_dma_mblt_novmeinc_read_integrity(self, vdev_cfg, args, f1_mmap_buf, logger):
        """ This test checks the data integrity for DMA MBLT when 'novmeinc'
        flag is enabled. As it is BLT the block transfer size is 2048bytes.
        """
        block_sz = 2048
        self._novmeinc_read_integrity(vdev_cfg, args, block_sz)

    def _novmeinc_write_integrity(self, vdev_cfg, args, block_sz):
        """Novmeinc write data integrity consists to write into the SVEC RAM
        random values using DMA with 'novmeinc" flag enabled, and read back the
        SVEC RAM with 'novmeinc' flag disabled.
        Therefore, the first block size slice of the read buffer should be
        equal to the last block size slice of the write buffer
        """
        addr_base = vdev_cfg[args['func']]['ader']
        # NOVMEINC access requires page aligned buffer: alignment=PAGE_SIZE
        read_buf = self._aligned_array(4096, SRAM_SIZE)
        write_buf = self._aligned_array(4096, SRAM_SIZE)
        for i in range(SRAM_SIZE):
            write_buf[i] = random.randrange(0, 0xFF, 1)
        try:
            # Read once RAM pattern and fill the bufer
            novmeinc = 1  # enable 'novmeinc' flag
            PyVme.write_dma(args['am'], addr_base + SRAM_OFFSET,
                            ctypes.addressof(write_buf),
                            len(write_buf), args['dwidth'], novmeinc)

            novmeinc = 0  # disable 'novmeinc' flag
            PyVme.read_dma(args['am'], addr_base + SRAM_OFFSET,
                           ctypes.addressof(read_buf), len(read_buf),
                           args['dwidth'], novmeinc)
            start = SRAM_SIZE - block_sz
            # last last block size slice of the write buffer == first block
            # size slice of the read buffer
            for val1, val2 in zip(write_buf[start:SRAM_SIZE], read_buf[0:block_sz]):
                assert val1 == val2
        except OSError as error:
            assert False, str(error)

    @pytest.mark.xfail()
    @pytest.mark.parametrize("args", dma_blt_novmeinc_settings,
                             ids=dma_read_integrity_id_fn)
    def test_dma_blt_novmeinc_write_integrity(self, vdev_cfg, args):
        """ This test checks the data integrity for DMA BLT when 'novmeinc'
        flag is enabled. As it is BLT the block transfer size is 256bytes.
        This test is marked 'XFAIL' because it is failing with the MenA25 due
        to a bug in wirting with novmeinc flag enabled. It works with MenA20.
        """
        block_sz = 256
        self._novmeinc_write_integrity(vdev_cfg, args, block_sz)

    @pytest.mark.xfail()
    @pytest.mark.parametrize("args", dma_mblt_novmeinc_settings,
                             ids=dma_read_integrity_id_fn)
    def test_dma_mblt_novmeinc_write_integrity(self, vdev_cfg, args):
        """ This test checks the data integrity for DMA MBLT when 'novmeinc'
        flag is enabled. As it is BLT the block transfer size is 2048bytes.
        This test is marked 'XFAIL' because it is failing with the MenA25 due
        to a bug in wirting with novmeinc flag enabled. It works with MenA20.
        """
        block_sz = 2048
        self._novmeinc_write_integrity(vdev_cfg, args, block_sz)

    @pytest.mark.parametrize("args", dma_settings, ids=dma_read_integrity_id_fn)
    def test_dma_read_write_integrity(self, vdev_cfg, args, f1_mmap_buf):
        """The test evaluates the DMA read/write data integrity based on
        writing error counter facility provided by the bitstream. Reading and
        writing back the SRAM pattern, an error counter is incremented in case
        of data intergrity issue.
        """
        addr_base = vdev_cfg[args['func']]['ader']
        buf = (ctypes.c_ubyte * args['buf_sz'])()
        try:
            # Read once RAM pattern and fill the bufer
            PyVme.read_dma(args['am'], addr_base + RAM_PATTERN_OFFSET,
                           ctypes.addressof(buf),
                           len(buf), args['dwidth'], 0)

            # Read current write error count register
            err_count = {}
            err_count["before"] = f1_mmap_buf[WRITE_ERROR_COUNT_REG_OFFSET >> 2]

            PyVme.write_dma(args['am'], addr_base + RAM_PATTERN_OFFSET,
                            ctypes.addressof(buf), len(buf), args['dwidth'], 0)

            # check write error counter (written back correct data)
            err_count["after"] = f1_mmap_buf[WRITE_ERROR_COUNT_REG_OFFSET >> 2]
            assert err_count["after"] - err_count["before"] == 0

        except OSError as error:
            assert False, str(error)

    @pytest.mark.parametrize("args", dma_settings, ids=dma_read_perf_id_fn)
    def test_dma_read_perf(self, vdev_cfg, args, logger):
        """The test evaluates the DMA read performance for all
        possible address/data-width configuration. The test fails if the
        measured data rate is less than the expected one for a given CPU
        board.
        """
        addr_base = vdev_cfg[args['func']]['ader']
        buf = (ctypes.c_ubyte * args['buf_sz'])()
        dma_int_count = {}
        dma_int_count["before"] = self._vme_int_dma_count()
        loop = 10000
        try:
            start = time.time()
            for i in range(0, loop):
                PyVme.read_dma(args['am'], addr_base + RAM_PATTERN_OFFSET,
                               ctypes.addressof(buf),
                               len(buf), args['dwidth'], 0)
            stop = time.time()
            dma_int_count["after"] = self._vme_int_dma_count()
            # check first number of DMA interrupts
            assert dma_int_count["after"] - dma_int_count["before"] == loop
            # Then check DMA performance
            self._assert_perf(start, stop, args, loop, 'read', logger)
        except OSError as error:
            assert False, str(error)

    @pytest.mark.parametrize("args", dma_settings, ids=dma_write_perf_id_fn)
    def test_dma_write_perf(self, vdev_cfg, args, f1_mmap_buf, logger):
        """The test evaluates the DMA read performance for all
        possible address/data-width configuration. The test fails if the
        measured data rate is less than the expected one for a given CPU
        board.
        """
        addr_base = vdev_cfg[args['func']]['ader']
        # instantiate a buffer and read once the SRAM_PATTERN just to write
        # valid data in this RAM area.
        buf = (ctypes.c_ubyte * args['buf_sz'])()
        PyVme.read_dma(args['am'], addr_base + RAM_PATTERN_OFFSET,
                       ctypes.addressof(buf), len(buf), args['dwidth'], 0)
        dma_int_count = {}
        dma_int_count["before"] = self._vme_int_dma_count()
        loop = 10000
        try:
            start = time.time()
            for i in range(0, loop):
                PyVme.write_dma(args['am'], addr_base + RAM_PATTERN_OFFSET,
                                ctypes.addressof(buf),
                                len(buf), args['dwidth'], 0)
            stop = time.time()

            dma_int_count["after"] = self._vme_int_dma_count()
            # check first number of DMA interrupts
            assert dma_int_count["after"] - dma_int_count["before"] == loop
            # Then check DMA performance
            self._assert_perf(start, stop, args, loop, 'write', logger)
        except OSError as error:
            assert False, str(error)
