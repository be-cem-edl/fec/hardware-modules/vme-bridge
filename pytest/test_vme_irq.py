# SPDX-License-Identifier: LGPL-2.1-or-later
# SPDX-FileCopyrightText: 2022 CERN


import pytest
import random
import re
import time
import os

from config_validation import CheckConfig


test_module_name = "svec_vme_test"


def _load_svec_module():
    res = os.system(f'sudo /usr/sbin/insmod $(pwd)/pytest/svec-vme-test-driver/{test_module_name}.ko')
    if res != 0:
        raise Exception(f'Trying to load {test_module_name} driver failed.')


def _unload_svec_module():
    if not os.path.exists('/sys/module/{}'.format(test_module_name)):
        return
    return os.system(f'sudo /usr/sbin/rmmod -s {test_module_name}')


@pytest.fixture(scope="class")
def load_svec_module_class_scope():
    p = _load_svec_module()
    yield p
    _unload_svec_module()


@pytest.fixture(scope="function")
def load_svec_module_func_scope():
    p = _load_svec_module()
    yield p
    _unload_svec_module()


# SVEC-VME-TEST register's offset to tune interrupt generation
IRQ_SRC_REG_OFFSET = 0x824
IRQ_FREQ_REG_OFFSET = 0x828
IRQ_COUNT_REG_OFFSET = 0x82C
IRQ_START_DELAY_REG_OFFSET = 0x820


# Interrupt frequency settings: used by several tests.
irq_freq_settings = [
    # The FPGA 125MHz clock is used to generate INT frequency
    pytest.param({'irq_freq': 125000000, 'irq_count': 5, 'tmo': 6},
                 id="INT 1Hz, INT handler minimal",),
    pytest.param({'irq_freq': 62500, 'irq_count': 1000, 'tmo': 2},
                 id="INT 1KHz, INT handler minimal",),
    pytest.param({'irq_freq': 12500, 'irq_count': 5000, 'tmo': 2},
                 id="INT 5KHz, INT handler minimal",),
    pytest.param({'irq_freq': 7812, 'irq_count': 8000, 'tmo': 2},
                 id="INT 8KHz, INT handler minimal",),
    pytest.param({'irq_freq': 6250, 'irq_count': 10000, 'tmo': 2},
                 id="INT 10KHz, INT handler minimal", marks=[pytest.mark.xfail]),
    pytest.param({'irq_freq': 5208, 'irq_count': 12000, 'tmo': 2},
                 id="INT 12KHz, INT handler minimal", marks=[pytest.mark.xfail]),
    pytest.param({'irq_freq': 4167, 'irq_count': 15000, 'tmo': 2},
                 id="INT 15KHz, INT handler minimal", marks=[pytest.mark.xfail]),
    pytest.param({'irq_freq': 3472, 'irq_count': 18000, 'tmo': 2},
                 id="INT 18KHz, INT handler minimal", marks=[pytest.mark.xfail]),
    pytest.param({'irq_freq': 3125, 'irq_count': 20000, 'tmo': 2},
                 id="INT 20KHz, INT handler minimal", marks=[pytest.mark.xfail]),
]


def proc_int_count(dev_name):
    with open("/proc/interrupts", "r") as f_int:
        proc_int = f_int.read()
    m = re.search(r'([0-9]+:)([0-9\s]+)(.*{})'.format(dev_name),
                  proc_int)
    return sum([int(val) for val in m.group(2).split()])


def vme_int_level_count(level):
    with open("/proc/vme/interrupts", "r") as f_int:
        proc_int = f_int.read()
    if level == 0:  # means all levels
        m = re.findall(r'(IRQ[1-7]\s+)([0-9]+)', proc_int)
    else:
        m = re.findall(r'(IRQ{}\s+)([0-9]+)'.format(level), proc_int)
    return sum([int(val[1]) for val in m])


def set_int_wait_clean(mmap_buf, args):
    idx = IRQ_FREQ_REG_OFFSET
    mmap_buf[idx:idx + 4] = args['irq_freq'].to_bytes(4, 'big')
    idx = IRQ_COUNT_REG_OFFSET
    mmap_buf[idx:idx + 4] = args['irq_count'].to_bytes(4, 'big')
    idx = IRQ_START_DELAY_REG_OFFSET
    mmap_buf[idx:idx + 4] = (0x100).to_bytes(4, 'big')
    loop_count = args['tmo']
    while True:
        time.sleep(1)
        idx = IRQ_COUNT_REG_OFFSET
        v = mmap_buf[idx:idx + 4]
        int_count = int.from_bytes(v, 'big')
        if int_count == 0 or loop_count == 0:
            break
        int_count = 0
        loop_count -= 1
    # wait additionnal sec because the HW generates one more interrupt than requested
    time.sleep(1)
    # Stop interrupt in case something went wrong
    idx = IRQ_START_DELAY_REG_OFFSET
    mmap_buf[idx:idx + 4] = b'\x00\x00\x00\x00'
    idx = IRQ_COUNT_REG_OFFSET
    mmap_buf[idx:idx + 4] = b'\x00\x00\x00\x00'
    idx = IRQ_SRC_REG_OFFSET
    v = mmap_buf[idx:idx + 4]  # ack interrupt just in case


# All tests are skipped, in case the configuration is not valid
@pytest.mark.skipif(CheckConfig.is_valid() is not True,
                    reason=CheckConfig.get_err_msg())
class TestVmebusInt():
    """VME bus test Interrupt"""

    @pytest.mark.parametrize("irq_freq_args", irq_freq_settings)
    def test_irq_freq(self, vdev_class_scope, load_svec_module_class_scope,
                      f1_mmap_buf, irq_freq_args, logger):
        """The test configures the SPEC board to generate different interrupt
        rate and check the limit when the software starts to loose interrupt.
        For this test a very basic SVEC driver is used, whose purpose is to
        register a basic interrupt handler to ack the interrupt source register
        """

        int_count = {}
        int_count["before"] = proc_int_count(test_module_name)

        set_int_wait_clean(f1_mmap_buf, irq_freq_args)

        int_count["after"] = proc_int_count(test_module_name)

        irq_diff = int_count["after"] - int_count["before"]
        expected = irq_freq_args['irq_count'] + 1
        if irq_diff == expected:
            assert True
        else:
            lost = expected - irq_diff
            msg = f'lost {lost} interrupts(expected {expected} handled {irq_diff})'
            logger.info(msg)
            assert False

    @pytest.mark.parametrize(
        "irq_read_count",
        [
            pytest.param(30, id="INT 5KHz, INT handler with 30 VME SCT read"),
            pytest.param(40, id="INT 5KHz, INT handler with 40 VME SCT read",
                         marks=[pytest.mark.xfail]),
            pytest.param(50, id="INT 5KHz, INT handler with 50 VME SCT read",
                         marks=[pytest.mark.xfail]),
            pytest.param(60, id="INT 5KHz, INT handler with 60 VME SCT read",
                         marks=[pytest.mark.xfail]),
            pytest.param(70, id="INT 5KHz, INT handler with 70 VME SCT read",
                         marks=[pytest.mark.xfail]),
            pytest.param(80, id="INT 5KHz, INT handler with 80 VME SCT read",
                         marks=[pytest.mark.xfail]),
            pytest.param(90, id="INT 5KHz, INT handler with 90 VME SCT read",
                         marks=[pytest.mark.xfail]),
            pytest.param(100, id="INT 5KHz, INT handler with 100 VME SCT read",
                         marks=[pytest.mark.xfail]),
        ],
    )
    def test_perf_using_irq(self, vdev_class_scope,
                            load_svec_module_class_scope, f1_mmap_buf,
                            irq_read_count, svec_slot, logger):
        """INT performance test: this test set the interrupt frequency at 5KHz.
        Then set a sysfs attribute defining the number VME Bus read access
        inserted in the driver interrupt hanlder.
        An Interrupt rate of 5KHz, gives a maximum of 200us to process the
        interrupt. The MenA25 can afford close to 70 VME read single access,
        while the MenA20 afford 40 VME read single access
        As it is a performance test, as soon as one test case fails, the
        remaining test cases are skipped.
        """

        int_count = {}

        path = "/sys/bus/vme/devices/vme.{:02d}/irq_read_count".format(svec_slot[0])
        with open(path, "r+") as f:
            f.write(str(irq_read_count))

        int_count["before"] = proc_int_count(test_module_name)

        irq_setting = irq_freq_settings[2].values[0]  # 5KHz setting
        set_int_wait_clean(f1_mmap_buf, irq_setting)

        int_count["after"] = proc_int_count(test_module_name)

        irq_diff = int_count["after"] - int_count["before"]
        expected = irq_setting['irq_count'] + 1
        if irq_diff == expected:
            assert True
        else:
            lost = expected - irq_diff
            msg = f'lost {lost} interrupts(expected {expected} handled {irq_diff})'
            logger.info(msg)
            assert False


def irq_level_id_fn(irq_level):
    return f"Test Interrupt Level: level {irq_level}, Vector 0xD9, Frequency 5KHz"


def irq_vector_id_fn(irq_vector):
    return f"Test Interrupt Vector (random): Vector {irq_vector}, Level 2, Frequency 1KHz"


# All tests are skipped, in case the configuration is not valid
@pytest.mark.skipif(CheckConfig.is_valid() is not True,
                    reason=CheckConfig.get_err_msg())
class TestIrqLevelVector():
    @pytest.mark.parametrize("irq_level", range(1, 7), ids=irq_level_id_fn)
    def test_irq_level(self, vdev_func_scope, load_svec_module_func_scope,
                       irq_level, f1_mmap_buf):
        """ This test exercices all possible VME interrupt level. For the
        moment level 7 is not tested because this level is not supported by
        the VME bus driver
        """

        all_levels_int_count = level_int_count = {}

        all_levels_int_count["before"] = vme_int_level_count(0)
        level_int_count["before"] = vme_int_level_count(irq_level)

        irq_setting = irq_freq_settings[2].values[0]  # 5KHz setting
        set_int_wait_clean(f1_mmap_buf, irq_setting)

        all_levels_int_count["after"] = vme_int_level_count(0)
        level_int_count["after"] = vme_int_level_count(irq_level)

        irq_diff = all_levels_int_count["after"] - all_levels_int_count["before"]
        assert irq_diff == irq_setting['irq_count'] + 1
        irq_diff = level_int_count["after"] - level_int_count["before"]
        assert irq_diff == irq_setting['irq_count'] + 1  # the expected level

    @pytest.mark.parametrize("irq_vector", random.sample(range(0xFF), 5),
                             ids=irq_vector_id_fn)
    def test_irq_vector(self, vdev_func_scope, load_svec_module_func_scope,
                        f1_mmap_buf):
        """ This test exercices 5 random interrupt vectors among 255.
        """

        int_count = {}
        int_count["before"] = proc_int_count(test_module_name)

        irq_setting = irq_freq_settings[1].values[0]  # 1KHz setting
        set_int_wait_clean(f1_mmap_buf, irq_setting)

        int_count["after"] = proc_int_count(test_module_name)

        irq_diff = int_count["after"] - int_count["before"]
        assert irq_diff == irq_setting['irq_count'] + 1
