# SPDX-License-Identifier: LGPL-2.1-or-later
# SPDX-FileCopyrightText: 2022 CERN


import pytest
import re
import ctypes
import struct

from config_validation import CheckConfig
from conftest import all_mapping_params
from conftest import create_mmap_buf
from PyVME.PyVmeDevice import PyVme


READ_COUNT_REG_OFFSET = 0x808
WRITE_COUNT_REG_OFFSET = 0x80C
SRAM_OFFSET = 0x0
SRAM_SIZE = 0x800


# All tests are skipped, in case the configuration is not valid
@pytest.mark.skipif(CheckConfig.is_valid() is not True,
                    reason=CheckConfig.get_err_msg())
class TestVmebusMapping():
    """VME bus test mappings"""

    @pytest.mark.parametrize("args", all_mapping_params)
    def test_mapping(self, vdev_cfg, args):
        """This test exercise all possible VME mapping configuration and for
        each one assert that it is the correct mapping exposed by the VME BUS
        driver.
        """
        addr_base = vdev_cfg[args['func']]['ader']
        am = vdev_cfg[args['func']]['am']
        dwidth = args['dwidth']
        sz = 0x2000
        desc = PyVme.VME_MAPPING(data_width=dwidth, am=am, sizel=sz,
                                 vme_addrl=addr_base)
        mm = PyVme.mmap(desc, 1)

        match = None
        with open("/proc/vme/windows", "r") as f_wind:
            proc_wind = f_wind.read()
            pattern = "{:x}.*{:x}.*0x{:02x}.*D{}".format(addr_base, sz, am,
                                                         dwidth)
            match = re.search(pattern, proc_wind)
        PyVme.unmap(mm, desc, 1)
        assert match is not None

    @pytest.mark.parametrize("args", all_mapping_params)
    def test_read_count_reg(self, args):
        """For each mapping, read a uint32 register and checks the number of
        transaction on the VME bus. For instance for a mapping in D16 reading a
        32bits register requires 2 read transaction.
        The bitstream in the SVEC module has a 32bits specific register in charge
        of counting the VME read access
        """

        mm, desc = create_mmap_buf(args['func'], 0x1000, args['dwidth'])

        v = ctypes.c_uint32.from_buffer(mm, READ_COUNT_REG_OFFSET).value
        # byte order: little endian to big endian
        count_before = struct.unpack("<I", struct.pack(">I", v))[0]
        v = ctypes.c_uint32.from_buffer(mm, READ_COUNT_REG_OFFSET).value
        # byte order: little endian to big endian
        count_after = struct.unpack("<I", struct.pack(">I", v))[0]

        # Release mapping
        PyVme.unmap(mm, desc, 1)

        # Read count depends of the mapping data width
        # Reading 32bits register in D16 mapping requires 2 read
        # Reading 32bits register in D32 mapping requires 1 read (obvious)
        assert count_after - count_before == 32 // args['dwidth']

    @pytest.mark.parametrize("args", all_mapping_params)
    def test_write_count_reg(self, args):
        """For each mapping, write a uint32 register and checks the number of
        transaction on the VME bus. For instance for a mapping in D16 writing a
        32bits register requires 2 write transaction.
        The bitstream in the SVEC module has a 32bits specific register in charge
        of counting the VME write access
        """

        mm, desc = create_mmap_buf(args['func'], 0x1000, args['dwidth'])

        v = ctypes.c_uint32.from_buffer(mm, WRITE_COUNT_REG_OFFSET).value
        # byte order: little endian to big endian
        count_before = struct.unpack("<I", struct.pack(">I", v))[0]

        # write an uint32 in the SRAM
        ctypes.c_uint32.from_buffer(mm, SRAM_OFFSET).value = 0xa5a5a5a5

        v = ctypes.c_uint32.from_buffer(mm, WRITE_COUNT_REG_OFFSET).value
        # byte order: little endian to big endian
        count_after = struct.unpack("<I", struct.pack(">I", v))[0]

        # Release mapping
        PyVme.unmap(mm, desc, 1)

        # Write count depends of the mapping data width
        # Writing 32bits register in D16 mapping requires 2 write
        # Writing 32bits register in D32 mapping requires 1 write (obvious)
        assert count_after - count_before == 32 // args['dwidth']

    @pytest.mark.parametrize("args", all_mapping_params)
    def test_many_writes(self, args):
        """For each mapping, fill many times the SRAM.  Try to saturate FIFOs
        in the PCIe path"""

        mm, desc = create_mmap_buf(args['func'], 0x1000, args['dwidth'])

        # Use memset to be sure to only write
        # The creation of a ctype instance may read the initial value
        buf_type = ctypes.c_char * SRAM_SIZE
        buf = buf_type.from_buffer(mm, SRAM_OFFSET)
        for j in range(4):
            ctypes.memset(ctypes.addressof(buf), 0xff, SRAM_SIZE)
        del buf

        # Check memory has been written
        for i in range(0, SRAM_SIZE):
            assert ctypes.c_uint8.from_buffer(mm, SRAM_OFFSET + i).value == 0xff

        # Release mapping
        PyVme.unmap(mm, desc, 1)
