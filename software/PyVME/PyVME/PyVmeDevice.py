# SPDX-FileCopyrightText: 2023 2023 CERN (home.cern)
#
# SPDX-License-Identifier: LGPL-2.1-or-later

"""
SPDX-License-Identifier: LGPL-2.1-or-later
SPDX-FileCopyrightText: 2020 CERN
"""

import collections
import struct
import fcntl
import time
import os
import mmap
import ctypes


class PyVme():
    """It handles generic VME accesses"""
    VME_IOCTL_START_DMA = ((3 << 30) | (80 << 16) | (ord('V') << 8) | 10)
    VME_IOCTL_FIND_MAPPING = ((3 << 30) | (80 << 16) | (ord('V') << 8) | 3)
    VME_IOCTL_RELEASE_MAPPING = ((1 << 30) | (80 << 16) | (ord('V') << 8) | 4)
    VME_IOCTL_GET_CREATE_ON_FIND_FAIL = ((2 << 30) | (4 << 16) | (ord('V') << 8) | 5)
    VME_IOCTL_SET_CREATE_ON_FIND_FAIL = ((1 << 30) | (4 << 16) | (ord('V') << 8) | 6)
    VME_IOCTL_GET_DESTROY_ON_REMOVE = ((2 << 30) | (4 << 16) | (ord('V') << 8) | 7)
    VME_IOCTL_SET_DESTROY_ON_REMOVE = ((1 << 30) | (4 << 16) | (ord('V') << 8) | 8)


    class VME_DMA(ctypes.Structure):
        _fields_ = [
                    ('status', ctypes.c_uint),
                    ('length', ctypes.c_uint),
                    ('novmeinc', ctypes.c_uint),
                    ('dir', ctypes.c_int),

                    ('src_data_width', ctypes.c_int),
                    ('src_am', ctypes.c_int),
                    ('src_v2esst_mode', ctypes.c_int),
                    ('src_bcast_select', ctypes.c_uint),
                    ('src_addru', ctypes.c_uint),
                    ('src_addrl', ctypes.c_uint),
                    ('dst_data_width', ctypes.c_int),
                    ('dst_am', ctypes.c_int),
                    ('dst_v2esst_mode', ctypes.c_int),
                    ('dst_bcast_select', ctypes.c_uint),
                    ('dst_addru', ctypes.c_uint),
                    ('dst_addrl', ctypes.c_uint),

                    ('vme_block_size', ctypes.c_int),
                    ('vme_backoff_time', ctypes.c_int),
                    ('pci_block_size', ctypes.c_int),
                    ('pci_backoff_time', ctypes.c_int),
                    ]


    class VME_MAPPING(ctypes.Structure):
        _fields_ = [
                    ('window_num', ctypes.c_int),
                    ('padding', ctypes.c_int),
                    ('kernel_va', ctypes.c_void_p),
                    ('user_va', ctypes.c_void_p),
                    ('fd', ctypes.c_int),
                    ('window_enabled', ctypes.c_int),
                    ('data_width', ctypes.c_int),
                    ('am', ctypes.c_int),
                    ('read_prefetch_enabled', ctypes.c_int),
                    ('read_prefetch_size', ctypes.c_int),
                    ('v2esst_mode', ctypes.c_int),
                    ('bcast_select', ctypes.c_int),
                    ('pci_addru', ctypes.c_uint),
                    ('pci_addrl', ctypes.c_uint),
                    ('sizeu', ctypes.c_uint),
                    ('sizel', ctypes.c_uint),
                    ('vme_addru', ctypes.c_uint),
                    ('vme_addrl', ctypes.c_uint),
                    ]

    def __init__(self):
        pass

    @staticmethod
    def bridge_name():
        """Get the bridge name"""
        with open ("/sys/bus/vme/bridge_type", "r") as f_brg:
            return f_brg.read().rstrip()

    @staticmethod
    def dma(desc):
        """Perform a DMA transfer"""
        mode = 'r' if desc.dir == 2 else 'w'
        with open("/dev/vme_dma", mode) as f_dma:
            fcntl.ioctl(f_dma.fileno(), PyVme.VME_IOCTL_START_DMA, desc)

    @staticmethod
    def read_dma(address_modifier, vme_address, host_address,
                 size, data_width, novmeinc=0):
        """Read data using DMA"""
        desc = PyVme.VME_DMA(status=0,
                             length=size,
                             novmeinc=novmeinc,
                             dir=2,
                             src_data_width=data_width,
                             src_am=address_modifier,
                             src_v2esst_mode=0,
                             src_bcast_select=0,
                             src_addru=0,
                             src_addrl=vme_address,
                             dst_data_width=data_width,
                             dst_am=address_modifier,
                             dst_v2esst_mode=0,
                             dst_bcast_select=0,
                             dst_addru=(host_address >> 32) & 0xFFFFFFFF,
                             dst_addrl=host_address & 0xFFFFFFFF,
                             vme_block_size=7,
                             vme_backoff_time=0,
                             pci_block_size=7,
                             pci_backoff_time=0)
        PyVme.dma(desc)

    @staticmethod
    def write_dma(address_modifier, vme_address, host_address,
                  size, data_width, novmeinc=0):
        """Write data using DMA"""
        desc = PyVme.VME_DMA(status=0,
                             length=size,
                             novmeinc=novmeinc,
                             dir=1,
                             src_data_width=data_width,
                             src_am=address_modifier,
                             src_v2esst_mode=0,
                             src_bcast_select=0,
                             src_addru=(host_address >> 32) & 0xFFFFFFFF,
                             src_addrl=host_address & 0xFFFFFFFF,
                             dst_data_width=data_width,
                             dst_am=address_modifier,
                             dst_v2esst_mode=0,
                             dst_bcast_select=0,
                             dst_addru=0,
                             dst_addrl=vme_address,
                             vme_block_size=7,
                             vme_backoff_time=0,
                             pci_block_size=7,
                             pci_backoff_time=0)
        PyVme.dma(desc)

    @staticmethod
    def mmap(desc, force):
        desc.fd = os.open("/dev/vme_mwindow", os.O_RDWR)
        old_force = bytearray(4)
        new_force = struct.pack("<I", force)
        fcntl.ioctl(desc.fd, PyVme.VME_IOCTL_GET_CREATE_ON_FIND_FAIL, old_force)
        fcntl.ioctl(desc.fd, PyVme.VME_IOCTL_SET_CREATE_ON_FIND_FAIL, new_force)
        mm=None
        try:
            fcntl.ioctl(desc.fd, PyVme.VME_IOCTL_FIND_MAPPING, desc)
            off = desc.pci_addrl & ~(mmap.PAGESIZE - 1)
            size = desc.pci_addrl + desc.sizel - off
            mm = mmap.mmap(fileno=desc.fd, length=size, offset=off)
        except OSError as err:
            mm=None
        finally:
            fcntl.ioctl(desc.fd, PyVme.VME_IOCTL_SET_CREATE_ON_FIND_FAIL, old_force)
        return mm

    @staticmethod
    def unmap(mm, desc, force):
        if mm is None:
            return
        mm.close()
        old_dest = bytearray(4)
        new_force = struct.pack("<I", force)
        if force == 1:
            fcntl.ioctl(desc.fd, PyVme.VME_IOCTL_GET_DESTROY_ON_REMOVE, old_dest)
            fcntl.ioctl(desc.fd, PyVme.VME_IOCTL_SET_DESTROY_ON_REMOVE, new_force)
        fcntl.ioctl(desc.fd, PyVme.VME_IOCTL_RELEASE_MAPPING, desc)
        if force == 1:
            fcntl.ioctl(desc.fd, PyVme.VME_IOCTL_SET_DESTROY_ON_REMOVE, old_dest)
        os.close(desc.fd)


class PyVmeDevice(object):
    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def __init__(self, slot):
        self.slot = slot
        self.path_to_slot = "/sys/bus/vme/devices/slot.{:02d}/".format(slot)
        self.path_to_dev = os.path.join(self.path_to_slot,
                                        "vme.{:02d}".format(slot))

    def register(self, am = None, addr = None, size = None,
                 irq_vector = None, irq_level = None,
                 devname = None):
        vme_register_argmuments = ["--slot", str(self.slot),]
        if am is not None:
            vme_register_argmuments.append("--address-modifier")
            vme_register_argmuments.append("0x{:x}".format(am))
        if addr is not None:
            vme_register_argmuments.append("--address")
            vme_register_argmuments.append("0x{:x}".format(addr))
        if size is not None:
            vme_register_argmuments.append("--size")
            vme_register_argmuments.append(str(size))
        if irq_vector is not None:
            vme_register_argmuments.append("--irq-vector")
            vme_register_argmuments.append(str(irq_vector))
        if irq_level is not None:
            vme_register_argmuments.append("--irq-level")
            vme_register_argmuments.append(str(irq_level))
        if devname is not None:
            vme_register_argmuments.append("--name")
            vme_register_argmuments.append(str(devname))

        vme_register_command = ["/usr/local/bin/vme-register", ]
        vme_register_command.extend(vme_register_argmuments)
        os.system(' '.join(vme_register_command))

    def rescan(self):
        with open(os.path.join(self.path_to_slot, "rescan"), "w") as f:
            f.write("1")

    def unregister(self):
        try:
            with open(os.path.join(self.path_to_dev, "remove"), "w") as f:
                f.write("1")
        except OSError:
            pass
        time.sleep(1)

    def is_registered(self):
        return os.path.exists(self.path_to_dev)

    def disable(self):
        with open(os.path.join(self.path_to_dev, "enable"), "w") as f:
            f.write("0")

    def enable(self):
        with open(os.path.join(self.path_to_dev, "enable"), "w") as f:
            f.write("1")

    def is_enabled(self):
        with open(os.path.join(self.path_to_dev, "enable"), "r") as f:
            return bool(int(f.read()))

    def __fetch_csr(self):
        with open(os.path.join(self.path_to_dev, "csr"), "rb") as fcsr:
            return fcsr.read()

    def ader_set(self, func, addr, am, dsfr=0, xam=0):
        self.disable()
        with open(os.path.join(self.path_to_dev, "ader"), "w") as f_ader:
            ader = (addr & 0xFFFFFF00) | ((am & 0xFF) << 2) | ((dsfr & 0x1) << 1) | ((xam & 0x1) << 0)
            f_ader.write("{:d} 0x{:x}".format(func, ader))
        self.enable()

    def ader_get(self, func):
        csr_bin = self.__fetch_csr()
        ader = 0
        ader = ader | (csr_bin[867 + (func * 0x10)] << 24)
        ader = ader | (csr_bin[871 + (func * 0x10)] << 16)
        ader = ader | (csr_bin[875 + (func * 0x10)] << 8)
        ader = ader | (csr_bin[879 + (func * 0x10)] << 0)

        return ader
