# SPDX-FileCopyrightText: 2023 2023 CERN (home.cern)
#
# SPDX-License-Identifier: LGPL-2.1-or-later

"""
@package docstring
@author: Federico Vaga <federico.vaga@cern.ch>

SPDX-License-Identifier: LGPL-2.1-or-later
SPDX-FileCopyrightText: 2020 CERN  (home.cern)
"""

from . import PyVmeDevice

__all__ = (
    "PyVmeDevice",
)
