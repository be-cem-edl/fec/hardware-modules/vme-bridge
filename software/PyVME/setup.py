#!/usr/bin/env python

# SPDX-FileCopyrightText: 2023 2023 CERN (home.cern)
#
# SPDX-License-Identifier: CC-BY-SA-4.0+

"""
SPDX-License-Identifier: CC-BY-SA-4.0+
SPDX-FileCopyrightText: 2020 CERN
"""

from distutils.core import setup

setup(name='PyVME',
      version='1.0',
      description='Python Module to handle VME devices',
      author='Federico Vaga',
      author_email='federico.vaga@cern.ch',
      maintainer="Federico Vaga",
      maintainer_email="federico.vaga@cern.ch",
      url='',
      packages=['PyVME'],
      license='LGPL-3.0-or-later',
     )
