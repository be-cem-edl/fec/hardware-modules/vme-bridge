# SPDX-FileCopyrightText: 2023 2023 CERN (home.cern)
#
# SPDX-License-Identifier: GPL-2.0-or-later

-include Makefile.specific
# include the build environment
REPO_PARENT ?= $(shell pwd)/../../..
-include $(REPO_PARENT)/common.mk

GIT_VERSION := $(shell git describe --dirty --long --tags)
KVERSION ?= $(shell uname -r)
KERNELSRC ?= /lib/modules/$(KVERSION)/build

CPPCHECK ?= cppcheck
FLAWFINDER ?= flawfinder

all: driver
driver: modules

coccicheck:
	$(MAKE) -C $(KERNELSRC) M=`pwd` GIT_VERSION=$(GIT_VERSION) \
		CROSS_COMPILE=$(CROSS_COMPILE) $@

modules_no_crc_fix:
	$(MAKE) -C $(KERNELSRC) M=`pwd` GIT_VERSION=$(GIT_VERSION) CROSS_COMPILE=$(CROSS_COMPILE) modules

# This is a hack to avoid to redeploy everything because of a CRC change.
# We believe that the new and the old vmebridges are ABI compatible
# depsite the CRC change. REMOVE THIS when we are able to redeloy everything
modules: modules_no_crc_fix
ifeq ($(CPU),L867)
	mv Module.symvers Module.symvers.new
	mv vmebus.ko vmebus.ko.new
	cp crc-deployment/Module.symvers .
	./patch-export-crcs -s Module.symvers -o vmebus.ko vmebus.ko.new
endif

modules_install: modules

DESTDIR ?=
prefix ?= /usr/local
datarootdir ?= $(prefix)/share

install: modules_install
	mkdir -m 0775 -p $(DESTDIR)$(datarootdir)/vmebus
	$(MAKE) -C $(KERNELSRC) M=`pwd` GIT_VERSION=$(GIT_VERSION) CROSS_COMPILE=$(CROSS_COMPILE) modules_install
	install -D -t $(DESTDIR)$(datarootdir)/vmebus -m 0644 Module.symvers

clean cleanall:
	$(RM) -f *.{a,cmd,ko,mod,mod.c,*.new,o,o.d,order,so,swp,doxy-ctc,ctc,ctctest.L*.sh,Module.symvers}
	$(MAKE) -C $(KERNELSRC) M=`pwd` clean

DRIVERS_DEPLOY=vmebus.ko

# add the path to the drivers
DRIVERS_LIST=$(DRIVERS_DEPLOY)

HEADERS_LIST=vmebus.h

cppcheck:
	$(CPPCHECK) -q -I.  --enable=all *.c *.h

flawfinder:
	$(FLAWFINDER) -SQDC --error-level=5 .

.PHONY: all driver modules modules_install install cppcheck flawfinder clean cleanall
