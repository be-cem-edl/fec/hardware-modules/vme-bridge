// SPDX-FileCopyrightText: 2023 2023 CERN (home.cern)
//
// SPDX-License-Identifier: GPL-2.0-or-later

/*
 * pldz002_irq.c - pldz002 PCI-VME bridge interrupt management
 *
 * Copyright (c) 2009 Sebastien Dugue
 */

/*
 *  This file provides the PCI-VME bridge interrupt management code.
 */

#include <linux/interrupt.h>
#include <linux/sched.h>
#include <linux/wait.h>
#include <linux/irqdomain.h>

#include "pldz002.h"
#include "vme_irq.h"
#include "vme_bridge.h"

static struct interrupt_stats int_stats[] = {
	{.name = "SPURIOUS"},
	{.name = "IRQ1"}, {.name = "IRQ2"}, {.name = "IRQ3"}, {.name = "IRQ4"},
	{.name = "IRQ5"}, {.name = "IRQ6"}, {.name = "IRQ7"},
	{.name = "BUSERR"}, {.name = "ACFAIL"}, {.name = "SYSFAIL"},
	{.name = "DMA"}, {.name = "DMAERR"},
	{.name = "MBOXRD0"}, {.name = "MBOXWR0"},
	{.name = "MBOXRD1"}, {.name = "MBOXWR1"},
	{.name = "MBOXRD2"}, {.name = "MBOXWR2"},
	{.name = "MBOXRD3"}, {.name = "MBOXWR3"},
	{.name = "LOCMON0"}, {.name = "LOCMON1"}
};

static void pldz002_vme_error(struct vme_bridge_device *vbridge,
			void __iomem *regs_vaddr, struct vme_bus_error *error)
{
	error->address = ioread32(regs_vaddr + PLDZ002_BERR_ADDR);
	error->am = ioread32(regs_vaddr + PLDZ002_BERR_ACC) &
					PLDZ002_BERR_ACC_AM_MASK; /*AM  mask */
}

/*
 * BusError interrupt handler returns 1 if interrupt has been found otherwhise 0
 */
static inline int pldz002_handle_vme_error_interrupt(
		struct vme_bridge_device *vbridge, void __iomem *regs_vaddr)
{
	struct vme_bus_error error;
	unsigned long flags;
	spinlock_t *lock;
	uint32_t berr;

	berr = ioread8(regs_vaddr + PLDZ002_MSTR) & PLDZ002_MSTR_BERR;
	if (berr == 0)
		return 0; // No VME bus error

	lock = &vbridge->verr.lock;

	spin_lock_irqsave(lock, flags);

	pldz002_vme_error(vbridge, regs_vaddr, &error);
	/* clear error */
	iowrite8(PLDZ002_MSTR_BERR | PLDZ002_MSTR_IBERREN,
		 regs_vaddr + PLDZ002_MSTR);
	/* dispatch bus error to clients */
	vme_bus_error_dispatch(vbridge, &error);

	spin_unlock_irqrestore(lock, flags);
	int_stats[PLDZ002_INT_STATS_BERR].count++;
	return 1; // VME bus error occurred
}

static int pldz002_iack(void *chip, int irq)
{
	struct pldz002_chip *pldz002 = chip;
	void __iomem *iack_vaddr = pldz002->regions[PLDZ002_REGION_IACK].vaddr;
	int vec;

	/*
	 * fetch vector (VME IACK cycle)
	 * See Men Arch spec (cahp 3.12) for an explanation of IACK
	 * address space
	 */
	vec = ioread8(iack_vaddr + (irq << 1) + 1);
	int_stats[PLDZ002_INT_STATS_IRQ1 + irq - 1].count++;
	return vec & 0xff;
}

/*
 * IRQ  handler returns 1 if interrupt has been found otherwhise 0
 */
static inline int pldz002_handle_irq_interrupt(struct vme_bridge_device *vbridge,
					       void __iomem *regs_vaddr)
{
	uint32_t istat;

	istat = ioread8(regs_vaddr + PLDZ002_ISTAT) & PLDZ002_INT_IRQM;
	if (istat == 0)
		return 0; // No IRQ found
	vme_handle_interrupt(istat, vbridge);
	return 1;
}

#define PLDZ002_CHAN_MASK 1 // single DMA channel
/*
 * DMA interrupt handler returns 1 if interrupt has been found otherwhise 0
 */
static inline int pldz002_handle_dma_interrupt(struct vme_bridge_device *vbridge,
					       void __iomem *regs_vaddr)
{
	uint32_t istat;
	struct vme_dma_chan *vdchan = &vbridge->dma_mgr.vdchan[0];
	struct pldz002_dma_chan *dchan = vdchan->private;
	int dma_err;
	unsigned long flags;

	istat = ioread8(regs_vaddr + PLDZ002_DMASTA) &
			(PLDZ002_DMASTA_IRQ | PLDZ002_DMASTA_ERR);
	if (istat == 0)
		return 0; // No IRQ found

	dma_err = istat & PLDZ002_DMASTA_ERR;
	if (dma_err) /* DMA failed: ACK IRQ and ERR */
		iowrite8(PLDZ002_DMASTA_IRQ | PLDZ002_DMASTA_ERR,
					regs_vaddr + PLDZ002_DMASTA);
	else /* Successful DMA. Ack DMA_IRQ */
		iowrite8(PLDZ002_DMASTA_IRQ, regs_vaddr + PLDZ002_DMASTA);

	/* critical section to protect tx_desc_current shared resource */
	spin_lock_irqsave(&vdchan->lock, flags);
	/*
	 * if no other batch to schedule or DMA error, transaction is completed
	 * No additionnal batch corresponds to curr_sg set to NULL
	 */
	if ((dchan->curr_sg == NULL) || dma_err) {
#if KERNEL_VERSION(3, 10, 0) <= LINUX_VERSION_CODE
		/* reports error if DMA has not been aborted by client */
		if (dma_err && vdchan->tx_desc_current != NULL) {
			vdchan->tx_desc_current->tx_res =
				(vdchan->tx_desc_current->direction == DMA_DEV_TO_MEM) ?
				DMA_TRANS_READ_FAILED : DMA_TRANS_WRITE_FAILED;
		}
#endif
		/* ends critical section before jumping */
		spin_unlock_irqrestore(&vdchan->lock, flags);
		goto tx_end;
	}

	/* Schedule next batch : should be called from critical section */
	pldz002_schedule_next_dma_batch(vdchan);
	/* ends critical section before leaving */
	spin_unlock_irqrestore(&vdchan->lock, flags);
	return 1;

tx_end: /* end of transaction and  set counters */
	vme_dmaengine_irq_handler(PLDZ002_CHAN_MASK, vbridge);
	if (!dma_err)
		int_stats[PLDZ002_INT_STATS_DMA].count++;
	else
		int_stats[PLDZ002_INT_STATS_DMAERR].count++;
	return 1;
}


/**
 * vme_bridge_interrupt() - VME bridge main interrupt handler
 *
 */
static irqreturn_t pldz002_interrupt_handler(int irq, void *arg)
{
	struct vme_bridge_device *vbridge = arg;
	struct pldz002_chip *chip = vbridge->chip_mgr.private;
	void __iomem *regs_vaddr = chip->regions[PLDZ002_REGION_REGS].vaddr;
	int handled;

	do {
		handled = 0;
		handled |= pldz002_handle_vme_error_interrupt(vbridge,
							      regs_vaddr);
		handled |= pldz002_handle_irq_interrupt(vbridge, regs_vaddr);
		handled |= pldz002_handle_dma_interrupt(vbridge, regs_vaddr);
	} while (handled);
	/*
	 * counting of spurious interrupt has been removed for 2 reasons:
	 * - MenA25 is using MSI interrupt which is not shared contrary to legacy
	 * PCI interrupts.
	 * - The ir_handler loops through A25 irq sources registers and handles
	 *   all irqs. As a result, it may happen that if two irqs are generated
	 *   close to each other two MSI are generated, but they are served in
	 *   a single IRQ handler execution. In this case, immediately after we
	 *   exit from the irq handler, we re-enter to irq handler as a result
	 *   of another MSI (for the 2nd irq that we already handled in the 1st
	 *   execution of irq handler). This MSI was qualified as spurious IRQ
	 *   which is not true and is due to the implementation i the FPGA
	 *   where all irq sources are mapped to a single MSI irq vector.
	 *   So as result, even if nothing has been handled, as we are sure
	 *   that the interrupt was for the VME bridge (thanks to MSI) we
	 *   return IRQ_HANDLED in any cases.
	 */
	return IRQ_HANDLED;
}

/**
 * pldz002_generate_interrupt() - Generate an interrupt on the VME bus
 * @level: IRQ level (1-7)
 * @vector: IRQ vector (0-255)
 * @msecs: Timeout for IACK in milliseconds
 *
 *  This function generates an interrupt on the VME bus and waits for IACK
 * for msecs milliseconds.
 *
 *  Returns 0 on success or -ETIME if the timeout expired.
 *
 */
#define _PLDZ002_INTERRUPTER_ID 1 /** interrupter dummy ID */
static int pldz002_generate_interrupt(struct vme_bridge_device *vbridge,
				      int level, int vector, signed long msecs)
{
	struct pldz002_chip *chip = vbridge->chip_mgr.private;
	signed long timeout = msecs_to_jiffies(msecs);
	void __iomem *regs_vaddr;
	uint8_t val;

	regs_vaddr = chip->regions[PLDZ002_REGION_REGS].vaddr;

	val = ioread8(regs_vaddr + PLDZ002_INTR) & PLDZ002_INTR_INTEN;
	if (val)
		return -EBUSY; /* interrupter busy */

	iowrite8(vector, regs_vaddr + PLDZ002_INTID);
	iowrite8(level | PLDZ002_INTR_INTEN, regs_vaddr + PLDZ002_INTR);

	/* Wait for timeout */
	set_current_state(TASK_INTERRUPTIBLE);
	timeout = schedule_timeout(timeout);

	/* Check if generated interrupt has been acknowledged */
	val = ioread8(regs_vaddr + PLDZ002_INTR) & PLDZ002_INTR_INTEN;
	if (val)
		return -ETIME; /* interrupt not yet acked */
	return 0; /* interrupt acked */
}

static int pldz002_enable_interrupts(struct vme_bridge_device *vbridge)
{
	unsigned int intmask;
	struct pldz002_chip *chip = vbridge->chip_mgr.private;
	void __iomem *regs_vaddr;

	regs_vaddr = chip->regions[PLDZ002_REGION_REGS].vaddr;

	/* Enable LM Interrupts */
	intmask = 0x0;
	if (vbridge->syscon)
		intmask |= PLDZ002_INT_IRQM;
	iowrite8(intmask, regs_vaddr + PLDZ002_IMASK);
	vbridge->irq_mgr.vme_interrupts_enabled = intmask;

	/* Enable Bus Error Interrupt */
	iowrite8(PLDZ002_MSTR_IBERREN, regs_vaddr + PLDZ002_MSTR);
	return 0;
}

static int pldz002_disable_interrupts(struct vme_bridge_device *vbridge)
{
	unsigned int intmask;
	struct pldz002_chip *chip = vbridge->chip_mgr.private;
	void __iomem *regs_vaddr;

	regs_vaddr = chip->regions[PLDZ002_REGION_REGS].vaddr;

	/* Disable LM Interrupts */
	intmask = 0x0;
	iowrite8(intmask, regs_vaddr + PLDZ002_IMASK);
	vbridge->irq_mgr.vme_interrupts_enabled = intmask;

	/* Disable Bus Error Interrupt */
	iowrite8(0x0, regs_vaddr + PLDZ002_MSTR);
	return 0;
}

static struct vme_bridge_irq_ops pldz002_irq_ops = {
	.enable_interrupts = pldz002_enable_interrupts,
	.disable_interrupts = pldz002_disable_interrupts,
	.interrupt_handler = pldz002_interrupt_handler,
	.iack = pldz002_iack,
	.generate_interrupt = pldz002_generate_interrupt,
};

int pldz002_irq_set_ops(struct vme_bridge_device *vbridge)
{
	vbridge->irq_mgr.ops = &pldz002_irq_ops;
	vbridge->int_stats = int_stats;
	vbridge->int_stats_count = ARRAY_SIZE(int_stats);
	return 0;
}
