// SPDX-FileCopyrightText: 2023 2023 CERN (home.cern)
//
// SPDX-License-Identifier: GPL-2.0-or-later

/*
 * Copyright (c) 2019
 * Author Federico Vaga <federico.vaga@cern.ch>
 * Author Sebastien Dugue
 */

#ifdef CONFIG_PROC_FS

#include <linux/version.h>
#include <linux/io.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>

#include "vme_bridge.h"
#include "vme_procfs.h"
#include "vme_compat.h"
#include "pldz002.h"


/**
 * pldz002_proc_crcsr_show() - List all supported bitsteam versions
 *
 */
static int pldz002_proc_supp_show(struct seq_file *m, void *data)
{
	seq_puts(m, "3.15\n3.11\n3.10\n");

	return 0;
}

static int pldz002_proc_supp_open(struct inode *inode, struct file *file)
{
	return single_open(file, pldz002_proc_supp_show, PDE_DATA(inode));
}

#if KERNEL_VERSION (5, 5, 0) > LINUX_VERSION_CODE
static const struct file_operations pldz002_proc_supp_ops = {
	.open           = pldz002_proc_supp_open,
	.read           = seq_read,
	.llseek         = seq_lseek,
	.release        = single_release,
};
#else
static const struct proc_ops pldz002_proc_supp_ops = {
	.proc_open           = pldz002_proc_supp_open,
	.proc_read           = seq_read,
	.proc_lseek          = seq_lseek,
	.proc_release        = single_release,
};
#endif

#define PLDZ002_PROCFS_NAME_SUPP "supported_bitstream"

/**
 * pldz002_procfs_register() - Create the VME proc tree
 * @vme_root: Root directory of the VME proc tree
 */
static void pldz002_procfs_register(struct vme_bridge_device *vbridge)
{
	struct proc_dir_entry *entry;

	vbridge->procfs_mgr.proc_chip = proc_mkdir("pldz002",
			vbridge->procfs_mgr.proc_root);

	entry = proc_create_data(PLDZ002_PROCFS_NAME_SUPP, 0444,
			vbridge->procfs_mgr.proc_chip,
			&pldz002_proc_supp_ops, vbridge->chip_mgr.private);
	if (!entry)
		pr_warn(PFX "Failed to create proc 'supported_bitstream' node\n");

}

/**
 * pldz002_procfs_unregister() - Remove the VME proc tree
 *
 */
static void pldz002_procfs_unregister(struct vme_bridge_device *vbridge)
{
	remove_proc_entry(PLDZ002_PROCFS_NAME_SUPP,
			  vbridge->procfs_mgr.proc_chip);
	remove_proc_entry("pldz002", vbridge->procfs_mgr.proc_root);
}

#else

static void pldz002_procfs_register(struct vme_bridge_device *vbridge) {}
static void pldz002_procfs_unregister(struct vme_bridge_device *vbridge) {}

#endif /* CONFIG_PROC_FS */

static struct vme_bridge_procfs_ops pldz002_procfs_ops = {
	.procfs_register = pldz002_procfs_register,
	.procfs_unregister = pldz002_procfs_unregister,
};

int pldz002_procfs_set_ops(struct vme_bridge_device *vbridge)
{
	vbridge->procfs_mgr.ops = &pldz002_procfs_ops;
	return 0;
}
