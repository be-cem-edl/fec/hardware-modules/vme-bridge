// SPDX-FileCopyrightText: 2023 2023 CERN (home.cern)
//
// SPDX-License-Identifier: GPL-2.0-or-later

/*
 * vme_irq.c - PCI-VME bridge interrupt management
 *
 * Copyright (c) 2009 Sebastien Dugue
 */

/*
 *  This file provides the PCI-VME bridge interrupt management code.
 */

#include <linux/kernel.h>

#include "pldz002.h"
#include "vme_window.h"
#include "vme_bridge.h"
#include "vme_compat.h"

/**
 * Returns PLDZ002 VME address space based on address modifier and data width.
 * @param am addressmodifier
 * @param dwidth data width
 */
static int pldz002_get_vme_space(enum vme_address_modifier am,
				 enum vme_data_width data_width)
{
	if (am == VME_CR_CSR)
		return PLDZ002_VME_CRCSR;

	switch (data_width) {
	case VME_D64:
		switch (am) {
		default: /* Mute warnings */
			break;
		}
		break;
	case VME_D32:
		switch (am) {
		case VME_A32_LCK:
		case VME_A32_USER_DATA_SCT:
		case VME_A32_USER_PRG_SCT:
		case VME_A32_SUP_DATA_SCT:
		case VME_A32_SUP_PRG_SCT:
			return PLDZ002_VME_A32D32;

		case VME_A24_USER_DATA_SCT:
		case VME_A24_USER_PRG_SCT:
		case VME_A24_SUP_DATA_SCT:
		case VME_A24_SUP_PRG_SCT:
			return PLDZ002_VME_A24D32;

		case VME_A16_USER:
		case VME_A16_LCK:
		case VME_A16_SUP:
			return PLDZ002_VME_A16D32;
		default: /* Mute warnings */
			break;
		}
		break;
	case VME_D16:
		switch (am) {
		case VME_A24_USER_DATA_SCT:
		case VME_A24_USER_PRG_SCT:
		case VME_A24_SUP_DATA_SCT:
		case VME_A24_SUP_PRG_SCT:
			return PLDZ002_VME_A24D16;

		case VME_A16_USER:
		case VME_A16_LCK:
		case VME_A16_SUP:
			return PLDZ002_VME_A16D16;
		default:    /* Mute warnings */
			break;
		}
		break;
	/* Not available */
	case VME_D8:
		switch (am) {
		case VME_A24_USER_DATA_SCT:
		case VME_A24_USER_PRG_SCT:
		case VME_A24_SUP_DATA_SCT:
		case VME_A24_SUP_PRG_SCT:
			return PLDZ002_VME_A24D16;

		case VME_A16_USER:
		case VME_A16_LCK:
		case VME_A16_SUP:
			return PLDZ002_VME_A16D16;
		default:    /* Mute warnings */
			break;
		}
		break;
	default:    /* Mute warnings */
		break;
	}

	pr_err(PFX "Invalid VME space: am 0x%x and data width %d\n",
	       am, data_width);

	return -EINVAL;
}


/*
 * In order to save windows in the bridge, we map the whole address space
 * onto a single window, so that subsequent mappings of the same kind will
 * all be attached to it.
 */
static int pldz002_optimize_window_size(struct window *wind,
					enum pldz002_vme_spaces spc)
{
	uint64_t vme_addr_org, vme_addr_opt;
	uint64_t size_opt;

	switch (spc) {
	case PLDZ002_VME_A16D16:
	case PLDZ002_VME_A16D32:
		vme_addr_opt = 0;
		size_opt = PLDZ002_VME_A16_SIZE;
		break;
	case PLDZ002_VME_A24D16:
	case PLDZ002_VME_A24D32:
		vme_addr_opt = 0;
		size_opt = PLDZ002_VME_A24_SIZE;
		break;
	case PLDZ002_VME_CRCSR:
		vme_addr_opt = 0;
		size_opt = PLDZ002_VME_CRCSR_SIZE;
		break;
	case PLDZ002_VME_A32D32:
		if (wind->users != 0) {	/* already a A32 window setup */
			return -EBUSY;
		}

		/* check for <A32 BAR size> crossing in requested window */
		vme_addr_org = (uint64_t)wind->desc.vme_addru << 32 |
				wind->desc.vme_addrl;
		size_opt = (uint64_t)wind->desc.sizeu << 32 |
				wind->desc.sizel;
		vme_addr_opt = vme_addr_org & (PLDZ002_VME_A32_SIZE - 1);
		if (vme_addr_opt + size_opt > PLDZ002_VME_A32_SIZE)
			return -EINVAL;

		vme_addr_opt = vme_addr_org & ~(PLDZ002_VME_A32_SIZE - 1);
		size_opt = PLDZ002_VME_A32_SIZE; // set full A32 size

		break;
	default:
		return -EINVAL;
	}

	wind->desc.vme_addru = (uint32_t)(vme_addr_opt >> 32);
	wind->desc.vme_addrl = (uint32_t)vme_addr_opt;
	wind->desc.sizeu = 0;
	wind->desc.sizel = size_opt;
	return 0;
}

/*
 *
 */
static void release_pci_rsrc(struct window *wind)
{
	if (wind->desc.kernel_va) {
		iounmap(wind->desc.kernel_va);
		wind->desc.kernel_va = NULL;
	}
	release_resource(wind->rsrc);
	kfree(wind->rsrc);
	wind->rsrc = NULL;
}

static int pldz002_map_pci_rsrc(struct vme_bridge_device *vbridge,
				struct window *wind,
				enum pldz002_vme_spaces spc)
{
	resource_size_t addr;
	struct resource *parent_rsrc;
	int rc;

	wind->rsrc = kzalloc(sizeof(struct resource), GFP_KERNEL);
	if (wind->rsrc == NULL)
		return -ENOMEM;

	wind->rsrc->name = wind->name;
	wind->rsrc->flags = IORESOURCE_MEM;

	switch (spc) {
	case PLDZ002_VME_A16D16: /* BAR0 */
		addr = pci_resource_start(vbridge->pdev, 0) +
						PLDZ002_VME_A16D16_OFFSET;
		parent_rsrc = &vbridge->pdev->resource[0];
		break;

	case PLDZ002_VME_A16D32: /* BAR0 */
		addr = pci_resource_start(vbridge->pdev, 0) +
						PLDZ002_VME_A16D32_OFFSET;
		parent_rsrc = &vbridge->pdev->resource[0];
		break;

	case PLDZ002_VME_A24D16: /* BAR2 */
		addr = pci_resource_start(vbridge->pdev, 2) +
						PLDZ002_VME_A24D16_OFFSET;
		parent_rsrc = &vbridge->pdev->resource[2];
		break;

	case PLDZ002_VME_A24D32: /* BAR2 */
		addr = pci_resource_start(vbridge->pdev, 2) +
						PLDZ002_VME_A24D32_OFFSET;
		parent_rsrc = &vbridge->pdev->resource[2];
		break;

	case PLDZ002_VME_A32D32: /* BAR3 */
		addr = pci_resource_start(vbridge->pdev, 3) +
						PLDZ002_VME_A32D32_OFFSET;
		parent_rsrc = &vbridge->pdev->resource[3];
		break;

	case PLDZ002_VME_CRCSR: /* BAR4 */
		addr = pci_resource_start(vbridge->pdev, 4) +
						PLDZ002_VME_CRCSR_OFFSET;
		parent_rsrc = &vbridge->pdev->resource[4];
		break;

	default:
		return -EINVAL;
	}
	wind->rsrc->start = addr;
	wind->desc.pci_addru = (uint32_t)(addr >> 32);
	wind->desc.pci_addrl = (uint32_t)addr;
	wind->rsrc->end = wind->rsrc->start + wind->desc.sizel - 1;

	rc = insert_resource_l(parent_rsrc, wind->rsrc);
	/*FIXME not aligned: is it ok? Probably yes because */
	if (rc) {
		dev_err(&vbridge->pdev->dev,
			"Can't insert new resource %pR (res:%d)\n",
			wind->rsrc, rc);
		goto out_free;
	}

	wind->desc.kernel_va = ioremap(wind->rsrc->start, wind->desc.sizel);
	if (wind->desc.kernel_va == NULL) {
		pr_err(PFX
		       "Failed to map window %d start 0x%llx size 0x%.8x\n",
		       wind->desc.window_num,
		       (uint64_t)wind->rsrc->start, wind->desc.sizel);
		rc = -ENOMEM;
		goto out_release;
	}

	return 0;

out_release:
	release_resource(wind->rsrc);
out_free:
	kfree(wind->rsrc);
	wind->rsrc = NULL;

	return rc;
}

/**
 * pldz002_create_window() - Create a PCI-VME window
 * @desc: Window descriptor
 *
 * Setup the pldz002 window registers and enable the window for
 * use.
 *
 * Returns 0 on success, %EINVAL in case of wrong parameter.
 */
static int pldz002_setup_window(struct vme_bridge_device *vbridge,
				struct window *wind)
{
	int rc;
	int spc;
	uint32_t addr;
	uint32_t size;
	struct pldz002_chip *chip = vbridge->chip_mgr.private;
	void __iomem *regs_vaddr;

	spc = pldz002_get_vme_space(wind->desc.am, wind->desc.data_width);
	if (spc < 0)
		return spc;

	/*
	 * VME window size optimization depending of address apces are Chip
	 * spcecific
	 */
	rc = pldz002_optimize_window_size(wind, spc);
	if (rc < 0) {
		pr_err("%s optimize_wind failed:%d\n", __func__, rc);
		return rc;
	}

	rc = pldz002_map_pci_rsrc(vbridge, wind, spc);
	if (rc)
		return rc;

	/*
	 * Do some sanity checking on the window descriptor.
	 * PCI address, VME address and window size should be aligned
	 * on 64k boudaries. So round down the VME address and round up the
	 * VME size to 64K boundaries.
	 * The PCI address should already have been rounded.
	 */
	addr = wind->desc.vme_addrl;
	size = wind->desc.sizel;

	if (addr & 0xffff)
		addr = wind->desc.vme_addrl & ~0xffff;

	if (size & 0xffff) {
		size = (wind->desc.vme_addrl + wind->desc.sizel + 0x10000);
		size &= ~0xffff;
	}

	wind->desc.vme_addrl = addr;
	wind->desc.sizel = size;

	if (wind->desc.pci_addrl & 0xffff) {
		rc = -EINVAL;
		goto out_free_rsrc;
	}

	/*
	 * Comment found in MEN original code:
	 * the new LONGADD register consists of the adress highbyte.
	 * According to IC designer lower address bits are unused
	 * for a certain size, so we just write the highbyte
	 */
	regs_vaddr = chip->regions[PLDZ002_REGION_REGS].vaddr;
//	pr_info(PFX "Set LONGADD = 0x%02x\n", (addr >> 24) & 0xff );
	iowrite8((addr >> 24) & 0xff, regs_vaddr + PLDZ002_LONGADD);

	/* Finally, as everything went fine, enable the window */
	wind->desc.window_enabled = 1;
	return 0;

out_free_rsrc:
	/* Unmap the window and release resource*/
	release_pci_rsrc(wind);
	return rc;
}

/**
 * pldz002_remove_window() - Disable a PCI-VME window
 * @desc: Window descriptor (only the window number is used)
 *
 * Disable the PCI-VME window specified in the descriptor.
 */
static void pldz002_release_window(struct vme_bridge_device *vbridge,
				   struct window *wind)
{
	wind->desc.window_enabled = 0;
	/* Unmap the window and release resource*/
	release_pci_rsrc(wind);
}

static struct vme_bridge_window_ops pldz002_wind_ops = {
	.get_nr_windows = pldz002_get_nr_windows,
	.get_window_attr = NULL,
	.setup_window = pldz002_setup_window,
	.release_window = pldz002_release_window,
};

int pldz002_window_set_ops(struct vme_bridge_device *vbridge)
{
	vbridge->wind_mgr.ops = &pldz002_wind_ops;
	return 0;
}
