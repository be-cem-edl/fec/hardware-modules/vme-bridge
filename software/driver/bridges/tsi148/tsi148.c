// SPDX-FileCopyrightText: 2023 2023 CERN (home.cern)
//
// SPDX-License-Identifier: GPL-2.0-or-later

/*
 * pldz002.c - Low level support for TSI148 PCI-VME Bridge chip
 *
 */

#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/dmaengine.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/sched.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/byteorder/generic.h>

#include "vme_bridge.h"
#include "tsi148.h"

/* CRG mapping on the VME bus - Maps to the top of the A24 address space */
#define A24D32_WINDOW	7
#define A24D32_AM	VME_A24_USER_DATA_SCT
#define A24D32_DBW	VME_D32
#define A24D32_VME_SIZE	0x1000000

#define CRG_WINDOW	7
#define CRG_AM		VME_A24_USER_DATA_SCT
#define CRG_DBW		VME_D32
#define CRG_VME_BASE	0x00fff000
#define CRG_VME_SIZE	0x1000

/*
 * Utility functions
 */

/**
 * tsi148_am_to_attr() - Convert a standard VME address modifier to TSI148
 *                       attributes
 * @am: Address modifier
 * @addr_size: Address size
 * @transfer_mode: Transfer Mode
 * @user_access: User / Supervisor access
 * @data_access: Data / Program access
 *
 */
int tsi148_am_to_attr(enum vme_address_modifier am,
		      unsigned int *addr_size,
		      unsigned int *transfer_mode,
		      unsigned int *user_access,
		      unsigned int *data_access)
{
	switch (am) {
	case VME_A16_USER:
		*addr_size = TSI148_A16;
		*transfer_mode = TSI148_SCT;
		*data_access = TSI148_DATA;
		*user_access = TSI148_USER;
		break;
	case VME_A16_SUP:
		*addr_size = TSI148_A16;
		*transfer_mode = TSI148_SCT;
		*data_access = TSI148_DATA;
		*user_access = TSI148_SUPER;
		break;
	case VME_A24_USER_MBLT:
		*addr_size = TSI148_A24;
		*transfer_mode = TSI148_MBLT;
		*data_access = TSI148_DATA;
		*user_access = TSI148_USER;
		break;
	case VME_A24_USER_DATA_SCT:
		*addr_size = TSI148_A24;
		*transfer_mode = TSI148_SCT;
		*data_access = TSI148_DATA;
		*user_access = TSI148_USER;
		break;
	case VME_A24_USER_PRG_SCT:
		*addr_size = TSI148_A24;
		*transfer_mode = TSI148_SCT;
		*data_access = TSI148_PROG;
		*user_access = TSI148_USER;
		break;
	case VME_A24_USER_BLT:
		*addr_size = TSI148_A24;
		*transfer_mode = TSI148_BLT;
		*data_access = TSI148_DATA;
		*user_access = TSI148_USER;
		break;
	case VME_A24_SUP_MBLT:
		*addr_size = TSI148_A24;
		*transfer_mode = TSI148_MBLT;
		*data_access = TSI148_DATA;
		*user_access = TSI148_SUPER;
		break;
	case VME_A24_SUP_DATA_SCT:
		*addr_size = TSI148_A24;
		*transfer_mode = TSI148_SCT;
		*data_access = TSI148_DATA;
		*user_access = TSI148_SUPER;
		break;
	case VME_A24_SUP_PRG_SCT:
		*addr_size = TSI148_A24;
		*transfer_mode = TSI148_SCT;
		*data_access = TSI148_PROG;
		*user_access = TSI148_SUPER;
		break;
	case VME_A24_SUP_BLT:
		*addr_size = TSI148_A24;
		*transfer_mode = TSI148_BLT;
		*data_access = TSI148_DATA;
		*user_access = TSI148_SUPER;
		break;
	case VME_A32_USER_MBLT:
		*addr_size = TSI148_A32;
		*transfer_mode = TSI148_MBLT;
		*data_access = TSI148_DATA;
		*user_access = TSI148_USER;
		break;
	case VME_A32_USER_DATA_SCT:
		*addr_size = TSI148_A32;
		*transfer_mode = TSI148_SCT;
		*data_access = TSI148_DATA;
		*user_access = TSI148_USER;
		break;
	case VME_A32_USER_PRG_SCT:
		*addr_size = TSI148_A32;
		*transfer_mode = TSI148_SCT;
		*data_access = TSI148_PROG;
		*user_access = TSI148_USER;
		break;
	case VME_A32_USER_BLT:
		*addr_size = TSI148_A32;
		*transfer_mode = TSI148_BLT;
		*data_access = TSI148_DATA;
		*user_access = TSI148_USER;
		break;
	case VME_A32_SUP_MBLT:
		*addr_size = TSI148_A32;
		*transfer_mode = TSI148_MBLT;
		*data_access = TSI148_DATA;
		*user_access = TSI148_SUPER;
		break;
	case VME_A32_SUP_DATA_SCT:
		*addr_size = TSI148_A32;
		*transfer_mode = TSI148_SCT;
		*data_access = TSI148_DATA;
		*user_access = TSI148_SUPER;
		break;
	case VME_A32_SUP_PRG_SCT:
		*addr_size = TSI148_A32;
		*transfer_mode = TSI148_SCT;
		*data_access = TSI148_PROG;
		*user_access = TSI148_SUPER;
		break;
	case VME_A32_SUP_BLT:
		*addr_size = TSI148_A32;
		*transfer_mode = TSI148_BLT;
		*data_access = TSI148_DATA;
		*user_access = TSI148_SUPER;
		break;
	case VME_A64_SCT:
		*addr_size = TSI148_A64;
		*transfer_mode = TSI148_SCT;
		*data_access = TSI148_DATA;
		*user_access = TSI148_SUPER;
		break;
	case VME_A64_BLT:
		*addr_size = TSI148_A64;
		*transfer_mode = TSI148_BLT;
		*data_access = TSI148_DATA;
		*user_access = TSI148_SUPER;
		break;
	case VME_A64_MBLT:
		*addr_size = TSI148_A64;
		*transfer_mode = TSI148_MBLT;
		*data_access = TSI148_DATA;
		*user_access = TSI148_SUPER;
		break;
	case VME_CR_CSR:
		*addr_size = TSI148_CRCSR;
		*transfer_mode = TSI148_SCT;
		*data_access = TSI148_DATA;
		*user_access = TSI148_USER;
		break;

	default:
		return -1;
	}

	return 0;
}
/*
 * TSI148 window support
 */


/**
 * am_to_crgattrs() - Convert address modifier to CRG Attributes
 * @am: Address modifier
 */
static int am_to_crgattrs(enum vme_address_modifier am)
{
	unsigned int attrs = 0;
	unsigned int addr_size;
	unsigned int user_access;
	unsigned int data_access;
	unsigned int transfer_mode;

	if (tsi148_am_to_attr(am, &addr_size, &transfer_mode, &user_access,
		       &data_access))
		return -EINVAL;

	switch (addr_size) {
	case TSI148_A16:
		attrs |= TSI148_LCSR_CRGAT_AS_16;
		break;
	case TSI148_A24:
		attrs |= TSI148_LCSR_CRGAT_AS_24;
		break;
	case TSI148_A32:
		attrs |= TSI148_LCSR_CRGAT_AS_32;
		break;
	case TSI148_A64:
		attrs |= TSI148_LCSR_CRGAT_AS_64;
		break;
	default:
		return -EINVAL;
	}

	switch (data_access) {
	case TSI148_PROG:
		attrs |= TSI148_LCSR_CRGAT_PGM;
		break;
	case TSI148_DATA:
		attrs |= TSI148_LCSR_CRGAT_DATA;
		break;
	default:
		return -EINVAL;
	}

	switch (user_access) {
	case TSI148_SUPER:
		attrs |= TSI148_LCSR_CRGAT_SUPR;
		break;
	case TSI148_USER:
		attrs |= TSI148_LCSR_CRGAT_NPRIV;
		break;
	default:
		return -EINVAL;
	}

	return attrs;
}

/**
 * tsi148_setup_crg() - Setup the CRG inbound mapping
 * @vme_base: VME base address for the CRG mapping
 * @am: Address modifier for the mapping
 */
static int tsi148_setup_crg(struct tsi148_regs *pci_map, unsigned int vme_base,
			    enum vme_address_modifier am)
{
	int attrs;

	iowrite32be(0, &pci_map->lcsr.cbau);
	iowrite32be(vme_base, &pci_map->lcsr.cbal);

	attrs = am_to_crgattrs(am);

	if (attrs < 0) {
		iowrite32be(0, &pci_map->lcsr.cbal);
		return -1;
	}

	attrs |= TSI148_LCSR_CRGAT_EN;

	iowrite32be(attrs, &pci_map->lcsr.crgat);

	return 0;
}

/**
 * tsi148_disable_crg() - Disable the CRG VME mapping
 *
 */
static void tsi148_disable_crg(struct tsi148_regs *pci_map)
{
	iowrite32be(0, &pci_map->lcsr.crgat);
	iowrite32be(0, &pci_map->lcsr.cbau);
	iowrite32be(0, &pci_map->lcsr.cbal);
}

/*
 * vme_bridge_map_regs() - Map VME registers
 * @pdev: PCI device to map
 *
 * Maps the 4k VME bridge register space.
 *
 * RETURNS: zero on success or -ERRNO value
 */
static int tsi148_request_regions(struct vme_bridge_device *vme_bridge)
{
	unsigned int tmp;
	unsigned int vid, did;
	int rc = 0;
	struct tsi148_chip *chip;

	/* Check nobody is using our MMIO resource (BAR 0) */
	rc = pci_request_region(vme_bridge->pdev, 0, DRV_MODULE_NAME);
	if (rc) {
		pr_err(PFX
		       "Failed to allocate PCI resource for BAR 0\n");
		return rc;
	}

	/* Map the chip registers - those are on BAR 0 */
	chip = vme_bridge->chip_mgr.private;
	chip->pci_map = pci_iomap(vme_bridge->pdev, 0, 4096);

	if (!chip->pci_map) {
		pr_err(PFX "Failed to map bridge register space\n");
		return -ENOMEM;
	}

	/* Check that the mapping worked out OK */
	tmp = ioread32(chip->pci_map);
	vid = tmp & 0xffff;
	did = (tmp >> 16) & 0xffff;

	if ((vid != PCI_VENDOR_ID_TUNDRA) &&
	    (did != PCI_DEVICE_ID_TUNDRA_TSI148)) {
		pr_err(PFX
		       "Failed reading mapped VME bridge registers\n");
		pci_iounmap(vme_bridge->pdev, chip->pci_map);
		chip->pci_map = NULL;
		return -ENODEV;
	}

	return 0;
}

/*
 * tsi148_vme_map_crg() - Map the bridge CRG address space
 */
int tsi148_vme_map_crg(struct tsi148_chip *chip)
{
	int rc;
	unsigned int tmp;
	unsigned int vid, did;
	struct vme_mapping a24d32_desc;
	struct vme_mapping crg_desc;

	memset(&a24d32_desc, 0, sizeof(a24d32_desc));
	memset(&crg_desc, 0, sizeof(crg_desc));

	/* Create and A24/D32 window */
	a24d32_desc.window_num = A24D32_WINDOW;
	a24d32_desc.am = A24D32_AM;
	a24d32_desc.read_prefetch_enabled = 0;
	a24d32_desc.data_width = A24D32_DBW;
	a24d32_desc.sizeu = 0;
	a24d32_desc.sizel = A24D32_VME_SIZE;
	a24d32_desc.vme_addru = 0;
	a24d32_desc.vme_addrl = 0;

	rc = vme_create_window(&a24d32_desc);
	if (rc != 0) {
		pr_err(PFX "Failed to create A24/D32 window\n");
		return rc;
	}


	/* Create a VME mapping for the CRG address space */
	crg_desc.am = CRG_AM;
	crg_desc.read_prefetch_enabled = 0;
	crg_desc.data_width = CRG_DBW;
	crg_desc.sizeu = 0;
	crg_desc.sizel = CRG_VME_SIZE;
	crg_desc.vme_addru = 0;
	crg_desc.vme_addrl = CRG_VME_BASE;

	rc = vme_find_mapping(&crg_desc, 0);
	if (rc != 0) {
		pr_err(PFX "Failed to create CRG mapping\n");
		return rc;
	}

	/* Setup the CRG inbound address and attributes accordingly */
	rc = tsi148_setup_crg(chip->pci_map, CRG_VME_BASE, CRG_AM);
	if (rc != 0) {
		pr_err(PFX "Failed to setup CRG\n");
		goto out_destroy_window;
	}

	/* Check that the mapping is OK */
	tmp = ioread32(crg_desc.kernel_va);
	vid = tmp & 0xffff;
	did = (tmp >> 16) & 0xffff;

	if ((vid != PCI_VENDOR_ID_TUNDRA) &&
	    (did != PCI_DEVICE_ID_TUNDRA_TSI148)) {
		pr_err(PFX
		       "Failed reading mapped VME CRG\n");
		tsi148_disable_crg(chip->pci_map);
		rc = -ENODEV;
		goto out_destroy_window;
	}

	/* Store vme va and vme window num for freeing resource */
	chip->vme_map = crg_desc.kernel_va;
	chip->vme_wind_num = crg_desc.window_num;

	pr_debug(PFX "Mapped CRG space @ %p (VME %08x)\n",
	       crg_desc.kernel_va, crg_desc.vme_addrl);
	return 0;

out_destroy_window:
	if (vme_destroy_window(crg_desc.window_num) != 0)
		pr_err(PFX "%s - Failed to destroy window\n", __func__);

	return rc;
}

void tsi148_vme_unmap_crg(struct tsi148_chip *chip)
{
	/* Release VME mapping */
	tsi148_disable_crg(chip->pci_map);
	vme_destroy_window(chip->vme_wind_num);
}

static void tsi148_release_regions(struct vme_bridge_device *vbridge)
{
	struct tsi148_chip *chip = vbridge->chip_mgr.private;

	/* Release PCI mapping */
	pci_iounmap(vbridge->pdev, chip->pci_map);
	pci_release_region(vbridge->pdev, 0);
}

/**
 * tsi148_quiesce() - Shutdown the TSI148 chip
 *
 * Put VME bridge in quiescent state.  Disable all decoders,
 * clear all interrupts and clear board fail and power-up reset
 */
static void tsi148_init_bridge(struct vme_bridge_device *vme_bridge)
{
	int i;
	unsigned int val;
	struct tsi148_chip *chip = vme_bridge->chip_mgr.private;

	/* Shutdown all inbound and outbound windows. */
	for (i = 0; i < TSI148_NR_OUT_WINDOWS; i++) {
		iowrite32be(0, &chip->pci_map->lcsr.itrans[i].itat);
		iowrite32be(0, &chip->pci_map->lcsr.otrans[i].otat);
	}

	for (i = 0; i < TSI148_NR_DMA_CHANNELS; i++) {
		iowrite32be(0, &chip->pci_map->lcsr.dma[i].dma_desc.dnlau);
		iowrite32be(0, &chip->pci_map->lcsr.dma[i].dma_desc.dnlal);
		iowrite32be(0, &chip->pci_map->lcsr.dma[i].dma_desc.dcnt);
	}

	/* Shutdown Location monitor. */
	iowrite32be(0, &chip->pci_map->lcsr.lmat);

	/* Shutdown CRG map. */
	tsi148_disable_crg(chip->pci_map);

	/* Clear error status. */
	iowrite32be(TSI148_LCSR_EDPAT_EDPCL, &chip->pci_map->lcsr.edpat);
	iowrite32be(TSI148_LCSR_VEAT_VESCL, &chip->pci_map->lcsr.veat);
	iowrite32be(TSI148_LCSR_PCSR_SRTO | TSI148_LCSR_PCSR_SRTE |
		    TSI148_LCSR_PCSR_DTTE | TSI148_LCSR_PCSR_MRCE,
		    &chip->pci_map->lcsr.pstat);

	/* Clear VIRQ interrupt (if any) */
	iowrite32be(TSI148_LCSR_VICR_IRQC, &chip->pci_map->lcsr.vicr);

	/* Disable and clear all interrupts. */
	iowrite32be(0, &chip->pci_map->lcsr.inteo);
	iowrite32be(0xffffffff, &chip->pci_map->lcsr.intc);
	iowrite32be(0xffffffff, &chip->pci_map->lcsr.inten);

	/* Map all Interrupts to PCI INTA */
	iowrite32be(0, &chip->pci_map->lcsr.intm1);
	iowrite32be(0, &chip->pci_map->lcsr.intm2);

	/*
	 * Set bus master timeout
	 * The timeout overrides the DMA block size if set too low.
	 * Enable release on request so that there will not be a re-arbitration
	 * for each transfer.
	 */
	val = ioread32be(&chip->pci_map->lcsr.vmctrl);
	val &= ~(TSI148_LCSR_VMCTRL_VTON_M | TSI148_LCSR_VMCTRL_VREL_M);
	val |= (TSI148_LCSR_VMCTRL_VTON_512 | TSI148_LCSR_VMCTRL_VREL_T_D_R);
	iowrite32be(val, &chip->pci_map->lcsr.vmctrl);

	/* Clear board fail, and power-up reset */
	val = ioread32be(&chip->pci_map->lcsr.vstat);
	val &= ~TSI148_LCSR_VSTAT_BDFAIL;
	val |= TSI148_LCSR_VSTAT_CPURST;
	iowrite32be(val, &chip->pci_map->lcsr.vstat);
}

static struct vme_bridge_chip_ops tsi148_chip_ops = {
	.get_chip_name = tsi148_get_chip_name,
	.request_regions = tsi148_request_regions,
	.release_regions = tsi148_release_regions,
	.get_regsaddr = tsi148_get_regsaddr,
	.get_slotnum = tsi148_get_slotnum,
	.get_syscon = tsi148_get_syscon,
	.init_bridge = tsi148_init_bridge,
	.bus_error_check = tsi148_bus_error_check,
};

int tsi148_set_ops(struct vme_bridge_device *vbridge)
{

	/* Allocates tsi148_chip to hold specific information */
	vbridge->chip_mgr.private =
			kzalloc(sizeof(struct tsi148_chip), GFP_KERNEL);
	if (vbridge->chip_mgr.private == NULL) {
		pr_err(PFX "Could not allocate tsi148 chip struct\n");
		return -ENOMEM;
	}
	/* set chip specific operations */
	vbridge->chip_mgr.ops = &tsi148_chip_ops;

	/* bridge specific init for VME windows mgr */
	tsi148_window_set_ops(vbridge);
	/* bridge specific init for dma mgr */
	tsi148_dma_set_ops(vbridge);
	/* bridge specific init for irq domain */
	tsi148_irq_set_ops(vbridge);
	/* bridge specific init for procfs */
	tsi148_procfs_set_ops(vbridge);

	return 0;
}
