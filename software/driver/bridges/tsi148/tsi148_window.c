// SPDX-FileCopyrightText: 2023 2023 CERN (home.cern)
//
// SPDX-License-Identifier: GPL-2.0-or-later

/*
 * vme_irq.c - PCI-VME bridge interrupt management
 *
 * Copyright (c) 2009 Sebastien Dugue
 */

/*
 *  This file provides the PCI-VME bridge interrupt management code.
 */

#include <linux/kernel.h>

#include "tsi148.h"
#include "vme_window.h"
#include "vme_bridge.h"


/*
 * sub64hi() - Calculate the upper 32 bits of the 64 bit difference
 * hi/lo0 - hi/lo1
 */
static int sub64hi(unsigned int lo0, unsigned int hi0,
		   unsigned int lo1, unsigned int hi1)
{
	if (lo0 < lo1)
		return hi0 - hi1 - 1;
	return hi0 - hi1;
}

/*
 * sub64hi() - Calculate the upper 32 bits of the 64 bit sum
 * hi/lo0 + hi/lo1
 */
static int add64hi(unsigned int lo0, unsigned int hi0,
		   unsigned int lo1, unsigned int hi1)
{
	if ((lo0 + lo1) < lo1)
		return hi0 + hi1 + 1;
	return hi0 + hi1;
}


/**
 * attr_to_am() - Convert TSI148 attributes to a standard VME address modifier
 * @addr_size: Address size
 * @transfer_mode: Transfer Mode
 * @user_access: User / Supervisor access
 * @data_access: Data / Program access
 * @am: Address modifier
 *
 */
static void attr_to_am(unsigned int addr_size, unsigned int transfer_mode,
		      unsigned int user_access, unsigned int data_access,
		      int *am)
{

	if (addr_size == TSI148_A16) {
		if (user_access == TSI148_USER)
			*am = VME_A16_USER;
		else
			*am = VME_A16_SUP;
	} else if (addr_size == TSI148_A24) {
		if (user_access == TSI148_USER) {
			if (transfer_mode == TSI148_SCT) {
				if (data_access == TSI148_DATA)
					*am = VME_A24_USER_DATA_SCT;
				else
					*am = VME_A24_USER_PRG_SCT;
			} else if (transfer_mode == TSI148_BLT)
				*am = VME_A24_USER_BLT;
			else if (transfer_mode == TSI148_MBLT)
				*am = VME_A24_USER_MBLT;
		} else if (user_access == TSI148_SUPER) {
			if (transfer_mode == TSI148_SCT) {
				if (data_access == TSI148_DATA)
					*am = VME_A24_SUP_DATA_SCT;
				else
					*am = VME_A24_SUP_PRG_SCT;
			} else if (transfer_mode == TSI148_BLT) {
				*am = VME_A24_SUP_BLT;
			} else if (transfer_mode == TSI148_MBLT) {
				*am = VME_A24_SUP_MBLT;
			}
		}
	} else if (addr_size == TSI148_A32) {
		if (user_access == TSI148_USER) {
			if (transfer_mode == TSI148_SCT) {
				if (data_access == TSI148_DATA)
					*am = VME_A32_USER_DATA_SCT;
				else
					*am = VME_A32_USER_PRG_SCT;
			} else if (transfer_mode == TSI148_BLT)
				*am = VME_A32_USER_BLT;
			else if (transfer_mode == TSI148_MBLT)
				*am = VME_A32_USER_MBLT;
		} else if (user_access == TSI148_SUPER) {
			if (transfer_mode == TSI148_SCT) {
				if (data_access == TSI148_DATA)
					*am = VME_A32_SUP_DATA_SCT;
				else
					*am = VME_A32_SUP_PRG_SCT;
			} else if (transfer_mode == TSI148_BLT) {
				*am = VME_A32_SUP_BLT;
			} else if (transfer_mode == TSI148_MBLT) {
				*am = VME_A32_SUP_MBLT;
			}
		}
	} else if (addr_size == TSI148_A64) {
		if (transfer_mode == TSI148_SCT)
			*am = VME_A64_SCT;
		else if (transfer_mode == TSI148_BLT)
			*am = VME_A64_BLT;
		else if (transfer_mode == TSI148_MBLT)
			*am = VME_A64_MBLT;
	} else if (addr_size == TSI148_CRCSR) {
		*am = VME_CR_CSR;
	}
}

/**
 * tsi148_setup_window_attributes() - Build the window attributes word
 * @v2esst_mode: VME 2eSST transfer speed
 * @data_width: VME data width (D16 or D32 only)
 * @am: VME address modifier
 *
 * This function builds the window attributes word. All parameters are
 * checked.
 *
 * Returns %EINVAL if any parameter is not consistent.
 */
static int tsi148_setup_window_attributes(enum vme_2esst_mode v2esst_mode,
					  enum vme_data_width data_width,
					  enum vme_address_modifier am)
{
	unsigned int attrs = 0;
	unsigned int addr_size;
	unsigned int user_access;
	unsigned int data_access;
	unsigned int transfer_mode;

	switch (v2esst_mode) {
	case VME_SST160:
	case VME_SST267:
	case VME_SST320:
		attrs |= ((v2esst_mode << TSI148_LCSR_OTAT_2eSSTM_SHIFT) &
			  TSI148_LCSR_OTAT_2eSSTM_M);
		break;
	default:
		return -EINVAL;
	}

	switch (data_width) {
	case VME_D16:
		attrs |= ((TSI148_DW_16 << TSI148_LCSR_OTAT_DBW_SHIFT) &
			  TSI148_LCSR_OTAT_DBW_M);
		break;
	case VME_D32:
		attrs |= ((TSI148_DW_32 << TSI148_LCSR_OTAT_DBW_SHIFT) &
			  TSI148_LCSR_OTAT_DBW_M);
		break;
	default:
		return -EINVAL;
	}

	if (tsi148_am_to_attr(am, &addr_size, &transfer_mode, &user_access,
		       &data_access))
		return -EINVAL;

	switch (transfer_mode) {
	case TSI148_SCT:
	case TSI148_BLT:
	case TSI148_MBLT:
	case TSI148_2eVME:
	case TSI148_2eSST:
	case TSI148_2eSSTB:
		attrs |= ((transfer_mode << TSI148_LCSR_OTAT_TM_SHIFT) &
			  TSI148_LCSR_OTAT_TM_M);
		break;
	default:
		return -EINVAL;
	}

	switch (user_access) {
	case TSI148_SUPER:
		attrs |= TSI148_LCSR_OTAT_SUP;
		break;
	case TSI148_USER:
		break;
	default:
		return -EINVAL;
	}

	switch (data_access) {
	case TSI148_PROG:
		attrs |= TSI148_LCSR_OTAT_PGM;
		break;
	case TSI148_DATA:
		break;
	default:
		return -EINVAL;
	}

	switch (addr_size) {
	case TSI148_A16:
	case TSI148_A24:
	case TSI148_A32:
	case TSI148_A64:
	case TSI148_CRCSR:
	case TSI148_USER1:
	case TSI148_USER2:
	case TSI148_USER3:
	case TSI148_USER4:
		attrs |= ((addr_size << TSI148_LCSR_OTAT_AMODE_SHIFT) &
			  TSI148_LCSR_OTAT_AMODE_M);
		break;
	default:
		return -EINVAL;
	}

	return attrs;
}

/*
 *
 */
static void release_pci_rsrc(struct window *wind)
{
	if (wind->desc.kernel_va) {
		iounmap(wind->desc.kernel_va);
		wind->desc.kernel_va = NULL;
	}
	release_resource(wind->rsrc);
	kfree(wind->rsrc);
	wind->rsrc = NULL;
}

/*
 * In order to save windows in the bridge, we map the whole address space
 * onto a single window, so that subsequent mappings of the same kind will
 * all be attached to it.
 */
static void tsi148_optimize_window_size(struct vme_mapping *desc)
{
	unsigned int resize = 0;

	switch (desc->am) {
	case VME_A24_USER_MBLT:
	case VME_A24_USER_DATA_SCT:
	case VME_A24_USER_PRG_SCT:
	case VME_A24_USER_BLT:
	case VME_A24_SUP_MBLT:
	case VME_A24_SUP_DATA_SCT:
	case VME_A24_SUP_PRG_SCT:
	case VME_A24_SUP_BLT:
		resize = 0x1000000;
		break;
	case VME_A16_USER:
	case VME_A16_LCK:
	case VME_A16_SUP:
		resize = 0x10000;
		break;
	case VME_CR_CSR:
		/*
		 * range [0x0, 0x80000] is undefined. Last
		 * slot is mapped at [0xA80000, 0xAFFFFF].
		 * That's why we need +1
		 */
		resize = VME_CRCSR_SIZE * (VME_SLOT_MAX + 1);
		break;
	default:
		break;
	}

	if (!resize)
		return;

	//pr_debug(PFX "window %d: optimizing size to 0x%08x\n",
	//		 desc->window_num, resize);
	desc->sizeu = 0;
	desc->sizel = resize;
	desc->vme_addru = 0;
	desc->vme_addrl = 0;
}

/*
 * Get the proper PCI I/O region and map the window to it
 */
static int tsi148_map_pci_rsrc(struct vme_bridge_device *vbridge,
			       struct window *wind)
{
	int rc;

	wind->rsrc = kzalloc(sizeof(struct resource), GFP_KERNEL);
	if (wind->rsrc == NULL)
		return -ENOMEM;

	wind->rsrc->name = wind->name;
	wind->rsrc->start = 0;
	wind->rsrc->end = wind->desc.sizel;
	wind->rsrc->flags = IORESOURCE_MEM;
	/*
	 * Allocate a PCI region for our window. Align the region to a 64K
	 * boundary for the VME bridge chip.
	 */
	rc = pci_bus_alloc_resource(vbridge->pdev->bus, wind->rsrc,
				    wind->desc.sizel, 0x10000,
				    PCIBIOS_MIN_MEM, 0, NULL, NULL);

	if (rc) {
		pr_err(PFX "%s - Failed to allocate bus resource for window %d start 0x%lx size 0x%.8x\n",
		       __func__, wind->desc.window_num,
		       (unsigned long)wind->rsrc->start, wind->desc.sizel);

		goto out_free;
	}

	wind->desc.kernel_va = ioremap(wind->rsrc->start, wind->desc.sizel);

	if (wind->desc.kernel_va == NULL) {
		pr_err(PFX "%s - failed to map window %d start 0x%lx size 0x%.8x\n",
		       __func__, wind->desc.window_num,
		       (unsigned long)wind->rsrc->start, wind->desc.sizel);

		rc = -ENOMEM;
		goto out_release;
	}

	wind->desc.pci_addrl = wind->rsrc->start;
	return 0;

out_release:
	release_resource(wind->rsrc);
out_free:
	kfree(wind->rsrc);
	wind->rsrc = NULL;

	return rc;
}

/**
 * tsi148_create_window() - Create a PCI-VME window
 * @desc: Window descriptor
 *
 * Setup the TSI148 window registers and enable the window for
 * use.
 *
 * Returns 0 on success, %EINVAL in case of wrong parameter.
 */
static int tsi148_setup_window(struct vme_bridge_device *vbridge,
			       struct window *wind)
{
	int wind_num = wind->desc.window_num;
	int rc;
	unsigned int addr;
	unsigned int size;
	struct tsi148_chip *chip = vbridge->chip_mgr.private;
	struct tsi148_otrans *trans = &chip->pci_map->lcsr.otrans[wind_num];
	unsigned int otat = 0;
	unsigned int oteal, oteau;
	unsigned int otofl, otofu;

	/*
	 * VME window size optimization depending of address apces are Chip
	 * specific
	 */
	tsi148_optimize_window_size(&wind->desc);

	rc = tsi148_map_pci_rsrc(vbridge, wind);
	if (rc)
		return rc;

	/*
	 * Do some sanity checking on the window descriptor.
	 * PCI address, VME address and window size should be aligned
	 * on 64k boudaries. So round down the VME address and round up the
	 * VME size to 64K boundaries.
	 * The PCI address should already have been rounded.
	 */
	addr = wind->desc.vme_addrl;
	size = wind->desc.sizel;

	if (addr & 0xffff)
		addr = wind->desc.vme_addrl & ~0xffff;

	if (size & 0xffff) {
		size = (wind->desc.vme_addrl + wind->desc.sizel + 0x10000);
		size &= ~0xffff;
	}

	wind->desc.vme_addrl = addr;
	wind->desc.sizel = size;

	if (wind->desc.pci_addrl & 0xffff) {
		rc = -EINVAL;
		goto out_free_rsrc;
	}

	/* Setup the window attributes */
	rc = tsi148_setup_window_attributes(wind->desc.v2esst_mode,
					    wind->desc.data_width,
					    wind->desc.am);

	if (rc < 0)
		goto out_free_rsrc;

	otat = (unsigned int)rc;

	/* Enable the window */
	otat |= TSI148_LCSR_OTAT_EN;

	if (wind->desc.read_prefetch_enabled) {
		/* clear Read prefetch disable bit */
		otat &= ~TSI148_LCSR_OTAT_MRPFD;
		otat |= ((wind->desc.read_prefetch_size <<
			  TSI148_LCSR_OTAT_PFS_SHIFT) & TSI148_LCSR_OTAT_PFS_M);
	} else {
		/* Set Read prefetch disable bit */
		otat |= TSI148_LCSR_OTAT_MRPFD;
	}

	/* Setup the VME 2eSST broadcast select */
	iowrite32be(wind->desc.bcast_select & TSI148_LCSR_OTBS_M,
		    &trans->otbs);

	/* Setup the PCI side start address */
	iowrite32be(wind->desc.pci_addrl & TSI148_LCSR_OTSAL_M,
		    &trans->otsal);
	iowrite32be(wind->desc.pci_addru, &trans->otsau);

	/*
	 * Setup the PCI side end address
	 * Note that 'oteal' is formed by the upper 2 bytes common to the
	 * addresses in the last 64K chunk of the window.
	 */
	oteal = (wind->desc.pci_addrl + wind->desc.sizel - 1) & ~0xffff;
	oteau = add64hi(wind->desc.pci_addrl, wind->desc.pci_addru,
			wind->desc.sizel, wind->desc.sizeu);
	iowrite32be(oteal, &trans->oteal);
	iowrite32be(oteau, &trans->oteau);

	/* Setup the VME offset */
	otofl = wind->desc.vme_addrl - wind->desc.pci_addrl;
	otofu = sub64hi(wind->desc.vme_addrl, wind->desc.vme_addru,
			wind->desc.pci_addrl, wind->desc.pci_addru);
	iowrite32be(otofl, &trans->otofl);
	iowrite32be(otofu, &trans->otofu);

	/*
	 * Finally, write the window attributes, also enabling the
	 * window
	 */
	iowrite32be(otat, &trans->otat);
	wind->desc.window_enabled = 1;

	return 0;

out_free_rsrc:
	release_pci_rsrc(wind);
	return rc;
}

/**
 * tsi148_release_window() - Disable a PCI-VME window
 * @desc: Window descriptor (only the window number is used)
 *
 * Disable the PCI-VME window specified in the descriptor.
 */
static void tsi148_release_window(struct vme_bridge_device *vbridge,
				  struct window *wind)
{
	struct tsi148_chip *chip = vbridge->chip_mgr.private;
	int wind_num = wind->desc.window_num;
	struct tsi148_otrans *trans = &chip->pci_map->lcsr.otrans[wind_num];

	/* Clear the attribute register thus disabling the window */
	iowrite32be(0, &trans->otat);

	/* Wipe the window registers */
	iowrite32be(0, &trans->otsal);
	iowrite32be(0, &trans->otsau);
	iowrite32be(0, &trans->oteal);
	iowrite32be(0, &trans->oteau);
	iowrite32be(0, &trans->otofl);
	iowrite32be(0, &trans->otofu);
	iowrite32be(0, &trans->otbs);

	wind->desc.window_enabled = 0;

	/* Unmap the window and release resource*/
	release_pci_rsrc(wind);
}

/**
 * tsi148_get_window_attr() - Get a hardware window attributes
 * @desc: Descriptor of the window (only the window number is used)
 *
 */
static void tsi148_get_window_attr(struct vme_bridge_device *vbridge,
			    struct vme_mapping *desc)
{
	int window_num = desc->window_num;
	unsigned int transfer_mode;
	unsigned int user_access;
	unsigned int data_access;
	unsigned int addr_size;
	int am = -1;
	struct tsi148_chip *chip = vbridge->chip_mgr.private;
	struct tsi148_otrans *trans = &chip->pci_map->lcsr.otrans[window_num];
	unsigned int otat;
	unsigned int otsau, otsal;
	unsigned int oteau, oteal;
	unsigned int otofu, otofl;

	/* Get window Control & Bus attributes register */
	otat = ioread32be(&trans->otat);

	if (otat & TSI148_LCSR_OTAT_EN)
		desc->window_enabled = 1;

	if (!(otat & TSI148_LCSR_OTAT_MRPFD))
		desc->read_prefetch_enabled = 1;

	desc->read_prefetch_size = (otat & TSI148_LCSR_OTAT_PFS_M) >>
		TSI148_LCSR_OTAT_PFS_SHIFT;

	desc->v2esst_mode = (otat & TSI148_LCSR_OTAT_2eSSTM_M) >>
		TSI148_LCSR_OTAT_2eSSTM_SHIFT;

	switch ((otat & TSI148_LCSR_OTAT_DBW_M) >> TSI148_LCSR_OTAT_DBW_SHIFT) {
	case TSI148_LCSR_OTAT_DBW_16:
		desc->data_width = VME_D16;
		break;
	case TSI148_LCSR_OTAT_DBW_32:
		desc->data_width = VME_D32;
		break;
	}

	desc->bcast_select = ioread32be(&chip->pci_map->lcsr.otrans[window_num].otbs);

	/*
	 * The VME address modifiers is encoded into the 4 following registers.
	 * Here we then have to map that back to a single value - This is
	 * coming out real ugly (sigh).
	 */
	addr_size = (otat & TSI148_LCSR_OTAT_AMODE_M) >>
		TSI148_LCSR_OTAT_AMODE_SHIFT;

	transfer_mode = (otat & TSI148_LCSR_OTAT_TM_M) >>
		TSI148_LCSR_OTAT_TM_SHIFT;

	user_access = (otat & TSI148_LCSR_OTAT_SUP) ?
		TSI148_SUPER : TSI148_USER;

	data_access = (otat & TSI148_LCSR_OTAT_PGM) ?
		TSI148_PROG : TSI148_DATA;

	attr_to_am(addr_size, transfer_mode, user_access, data_access, &am);

	if (am == -1)
		pr_warn(PFX
		       "%s - unsupported AM:\n"
		       "\taddr size: %x TM: %x usr/sup: %d data/prg:%d\n",
		       __func__, addr_size, transfer_mode, user_access,
		       data_access);

	desc->am = am;

	/* Get window mappings */
	otsal = ioread32be(&chip->pci_map->lcsr.otrans[window_num].otsal);
	otsau = ioread32be(&chip->pci_map->lcsr.otrans[window_num].otsau);
	oteal = ioread32be(&chip->pci_map->lcsr.otrans[window_num].oteal);
	oteau = ioread32be(&chip->pci_map->lcsr.otrans[window_num].oteau);
	otofl = ioread32be(&chip->pci_map->lcsr.otrans[window_num].otofl);
	otofu = ioread32be(&chip->pci_map->lcsr.otrans[window_num].otofu);


	desc->pci_addrl = otsal;
	desc->pci_addru = otsau;

	desc->sizel = oteal - otsal;
	desc->sizeu = sub64hi(oteal, oteau, otsal, otsau);

	desc->vme_addrl = otofl + otsal;

	if (addr_size == TSI148_A64)
		desc->vme_addru = add64hi(otofl, otofu, otsal, otsau);
	else
		desc->vme_addru = 0;
}

static struct vme_bridge_window_ops tsi148_wind_ops = {
	.get_nr_windows = tsi148_get_nr_windows,
	.get_window_attr = tsi148_get_window_attr,
	.setup_window = tsi148_setup_window,
	.release_window = tsi148_release_window,
};

int tsi148_window_set_ops(struct vme_bridge_device *vbridge)
{
	vbridge->wind_mgr.ops = &tsi148_wind_ops;
	return 0;
}
