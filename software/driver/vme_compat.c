// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * Copyright 2018 CERN
 * Author: Federico Vaga <federico.vaga@cern.ch>
 */
#include <linux/sched.h>
#include <linux/ioport.h>
#include <linux/mm.h>

#include "vme_bridge.h"
#include "vme_compat.h"

/**
 * NOTE Because of the "dirty hack" further down, access
 * to kallsyms_lookup_name() is needed, however the symbol
 * is not exported starting with kernel v5.6.0. This is a
 * further hack to provide access to the symbol.
 */
#if LINUX_VERSION_CODE >= KERNEL_VERSION(5, 6, 0)
#define KPROBE_LOOKUP 1
#include <linux/kprobes.h>
#else
#include <linux/kallsyms.h>
#endif

long get_user_pages_l(unsigned long start, unsigned long nr_pages,
		      unsigned int gup_flags, struct page **pages,
		      struct vm_area_struct **vmas)
{
#if KERNEL_VERSION(6, 4, 0) > LINUX_VERSION_CODE
#if LINUX_VERSION_CODE >= KERNEL_VERSION(4, 8, 0)
	return get_user_pages(start, nr_pages, gup_flags, pages, NULL);
#else
	unsigned int rw = (gup_flags & FOLL_WRITE ? 1 : 0);

#if LINUX_VERSION_CODE >= KERNEL_VERSION(4, 6, 0)
	return get_user_pages(start, nr_pages, rw, 0, pages, NULL);
#else
	return get_user_pages(current, current->mm, start, nr_pages, rw, 0,
			      pages, NULL);
#endif
#endif
#else
	return get_user_pages(start, nr_pages, gup_flags, pages);
#endif

}

/**
 * NOTE this is a dirty hack. Our driver is a kernel module,
 * so it has not access to all the kernel features. One of this
 * feature is the 'insert_resource' function which allows to add
 * a new resource in the resource tree. Here we use kallsyms/ kprobe to get
 * the pointer to that function.
 */
int insert_resource_l(struct resource *parent,
		      struct resource *new)
{
	typedef int (*insert_resource_t)(struct resource *parent, struct resource *new);
	insert_resource_t insert_resource_p;

#if LINUX_VERSION_CODE >= KERNEL_VERSION(5, 6, 0)

	struct kprobe kp = {
		.symbol_name = "insert_resource"
	};

	register_kprobe(&kp);
	insert_resource_p = (insert_resource_t) kp.addr;
	unregister_kprobe(&kp);
#else
	insert_resource_p = (void *) kallsyms_lookup_name("insert_resource");
#endif
	return insert_resource_p(parent, new);
}
