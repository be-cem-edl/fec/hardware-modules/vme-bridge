// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * vme_dma.c - PCI-VME bridge DMA management
 *
 * Copyright (c) 2009 Sebastien Dugue
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  either version 2 of the  License, or (at your
 * option) any later version.
 *
 */

/*
 *  This file provides the PCI-VME bridge DMA management code.
 */

#include <linux/jiffies.h>
#include <linux/pagemap.h>
#include <linux/sched.h>
#include <linux/delay.h>
#include <linux/mm.h>
#include <linux/jiffies.h>
#include <linux/semaphore.h>

#include "vmebus.h"
#include "vme_bridge.h"
#include "vme_compat.h"
#include "vme_dmaengine.h"


static int sgl_fill_user_pages(struct page **pages, unsigned long uaddr,
			const unsigned int nr_pages, int rw)
{
	int ret;

	/* Get user pages for the DMA transfer */
	mmap_read_lock(current->mm);
	ret = get_user_pages_l(uaddr, nr_pages, (rw ? FOLL_WRITE : 0),
			       pages, NULL);
	mmap_read_unlock(current->mm);

	return ret;
}

static int sgl_fill_kernel_pages(struct page **pages, unsigned long kaddr,
			const unsigned int nr_pages, int rw)
{
	void *addr = (void *)kaddr;
	int i;


	if (is_vmalloc_addr(addr)) {
		for (i = 0; i < nr_pages; i++)
			pages[i] = vmalloc_to_page(addr + PAGE_SIZE * i);
	} else {
		/* Note: this supports lowmem pages only */
		if (!virt_addr_valid(kaddr))
			return -EINVAL;
		for (i = 0; i < nr_pages; i++)
			pages[i] = virt_to_page(kaddr + PAGE_SIZE * i);
	}


	return nr_pages;
}

/**
 * sgl_map_user_pages() - Pin user pages and put them into a scatter gather list
 * @sgl: Scatter gather list to fill
 * @nr_pages: Number of pages
 * @uaddr: User buffer address
 * @count: Length of user buffer
 * @rw: Direction (0=read from userspace / 1 = write to userspace)
 * @to_user: 1 - transfer is to/from a user-space buffer. 0 - kernel buffer.
 *
 *  This function pins the pages of the userspace buffer and fill in the
 * scatter gather list.
 */
static int sgl_map_user_pages(struct scatterlist *sgl,
			      const unsigned int nr_pages, unsigned long uaddr,
			      size_t length, int rw, int to_user)
{
	int rc;
	int i;
	struct page **pages;

	pages = kcalloc(nr_pages, sizeof(struct page *), GFP_KERNEL);
	if (!pages)
		return -ENOMEM;

	if (to_user) {
		rc = sgl_fill_user_pages(pages, uaddr, nr_pages, rw);
		if (rc >= 0 && rc < nr_pages) {
			/* Some pages were pinned, release these */
			for (i = 0; i < rc; i++)
				put_page(pages[i]);
			rc = -ENOMEM;
			goto out_free;
		}
	} else {
		rc = sgl_fill_kernel_pages(pages, uaddr, nr_pages, rw);
	}

	if (rc < 0)
		/* We completely failed to get the pages */
		goto out_free;

	/* Populate the scatter/gather list */
	sg_init_table(sgl, nr_pages);

	/* Take a shortcut here when we only have a single page transfer */
	if (nr_pages > 1) {
		unsigned int off = offset_in_page(uaddr);
		unsigned int len = PAGE_SIZE - off;

		sg_set_page(&sgl[0], pages[0], len, off);
		length -= len;

		for (i = 1; i < nr_pages; i++) {
			sg_set_page(&sgl[i], pages[i],
				     (length < PAGE_SIZE) ? length : PAGE_SIZE,
				     0);
			length -= PAGE_SIZE;
		}
	} else
		sg_set_page(&sgl[0], pages[0], length, offset_in_page(uaddr));

out_free:
	/* We do not need the pages array anymore */
	kfree(pages);

	return nr_pages;
}

/**
 * sgl_unmap_user_pages() - Release the scatter gather list pages
 * @sgl: The scatter gather list
 * @nr_pages: Number of pages in the list
 * @dirty: Flag indicating whether the pages should be marked dirty
 * @to_user: 1 when transfer is to/from user-space (0 for to/from kernel)
 *
 */
static void sgl_unmap_user_pages(struct scatterlist *sgl,
				 const unsigned int nr_pages, int dirty,
				 int to_user)
{
	int i;

	if (!to_user)
		return;

	for (i = 0; i < nr_pages; i++) {
		struct page *page = sg_page(&sgl[i]);

		if (dirty && !PageReserved(page))
			SetPageDirty(page);

		put_page(page);
	}
}


/**
 * The match is fine if the proposed channel belongs to the
 * VME Bridge instance
 */
static bool vme_dmaengine_filter(struct dma_chan *dchan, void *arg)
{
	struct vme_bridge_device *vbridge = arg;
	struct vme_dma_chan *vdchan = vbridge->dma_mgr.vdchan;
	int i;


	/*
	 * Nobody can ask for a DMA channel while the engine has been paused.
	 * We do not want any DMA transfer to start or continue while we are
	 * in a global pause
	 */
	if (test_bit(VME_BRIDGE_DMA_PAUSE, vbridge->dma_mgr.flags))
		return 0;

	for (i = 0; i < vbridge->dma_mgr.nr_vdchan; ++i) {
		if (dchan == &vdchan[i].dchan)
			return 1;
	}

	return 0;
}

/**
 * Client data structure to keep DMA transaction result as the wake-up of the
 * client is async with the DMA completion.
 */
struct vme_dma_tx_ctxt {
	struct dmaengine_result dma_res;
	struct vme_dma_chan *vchan;
};

#if KERNEL_VERSION(3, 10, 0) <= LINUX_VERSION_CODE
/**
 * DMA engine calls client callback on DMA TX completion, with the result
 * giving the status of the transaction. This status is copied into client DMA
 * context to be able to know how the DMA went when the client is waken-up.
 * This DMA engine callback with result is only available in kernel > 3.10
 */
static void vme_dma_tx_complete(void *arg,
				const struct dmaengine_result *result)
{
	struct  vme_dma_tx_ctxt *dma_tx_ctxt = arg;

	memcpy(&dma_tx_ctxt->dma_res, result, sizeof(*result));
	wake_up(&dma_tx_ctxt->vchan->wait);
}
#else
static void vme_dma_tx_complete(void *arg)
{
	struct dma_chan *dchan = arg;
	struct vme_dma_chan *vchan = to_vme_dma_chan(dchan);

	wake_up(&vchan->wait);
}
#endif

/**
 * If the system hs been configured to enforce a specifc DMA configuration
 * adjust the DMA descriptor
 * @desc DMA descriptor
 */
static void __vme_do_dma_adjust(struct vme_dma *desc)
{
	struct vme_bridge_device *vbridge = vbridge_gbl;

	if (test_bit(VME_DEV_FLAG_FORCE_DMA_CFG, vbridge->dma_mgr.flags))
		desc->ctrl = vbridge->dma_mgr.dma_param;
}

/**
 * Tell if the given transfer is completed
 * @tx Transfer descriptor
 *
 * Return: 1 if the transfer is complete (SUCCESS or ERROR), 0 otherwise
 */
static int vme_dma_is_tx_complete(struct dma_chan *chan, dma_cookie_t cookie,
				  struct vme_dma_tx_ctxt *dma_tx_ctxt)
{
	enum dma_status status;
#if KERNEL_VERSION(3, 10, 0) <= LINUX_VERSION_CODE
	/*
	 * if the dma result in client context tells that the DMA transaction
	 * failed, there is no need to request if tx is completed.
	 * On success, is_tx_complete is checked to be sure that it is this
	 * cookie transaction.
	 */
	if (dma_tx_ctxt->dma_res.result != DMA_TRANS_NOERROR)
		/* dma result has been updated memaning that tx is over */
		return 1; // Done, no need to wait more
#endif
	status = dma_async_is_tx_complete(chan, cookie, NULL, NULL);
	if (status == DMA_SUCCESS || status == DMA_ERROR)
		return 1; // Done, no need to wait more
	return 0;// Not completed, please still wait
}

/**
 * Request a DMA channel
 * @vbridge: VME bridge instance to use
 * @timeout_ms: timeout in ms
 *
 * Return: a valid dma_chan on success, otherwise NULL
 *
 * We do an active wait here to reproduce the old vme_do_dma() behaviour
 * which was activly waiting in a semaphore. Instead of "polluting" the DMA
 * engine code with semaphores for an old API, I prefer to have this active
 * loop here.
 */
static struct dma_chan *vme_do_dma_request_channel(struct vme_bridge_device *vbridge,
						   unsigned int timeout_ms)
{
	struct dma_chan *dchan = NULL;
	dma_cap_mask_t mask;
	unsigned long j = jiffies + msecs_to_jiffies(timeout_ms);

	dma_cap_zero(mask);
	dma_cap_set(DMA_SLAVE, mask);
	dma_cap_set(DMA_PRIVATE, mask);

	while (1) {
		dchan = dma_request_channel(mask, vme_dmaengine_filter,
					    vbridge);
		if (!IS_ERR_OR_NULL(dchan))
			break;

		if (time_after(jiffies, j)) {
			pr_err("VME: %s timeout\n", __func__);
			break;
		}
	}

	return dchan;
}

/*
 * This alignment check is part of the DMA engine, but as a DMA
 * transaction may be preceded by a single access due to some aligmnent
 * constraints we perform the check here to reject the DMA request in the event
 * of misalignment
 */
static int __vme_check_alignment(struct vme_dma *vme_desc)
{
	struct vme_dma_attr *vme_attr;
	int vme_align;

	vme_attr = (vme_desc->dir == VME_DMA_FROM_DEVICE) ?
		&vme_desc->src : &vme_desc->dst;
	switch(vme_attr->data_width) {
	case VME_D32:
		vme_align = 4;
		break;
	case VME_D16:
		vme_align = 2;
		break;
	default:
		pr_err("VME: Can't prepare DMA transfer: DMA data width "
			"not supported %d\n", vme_attr->data_width);
		return -1;
	}
	if (unlikely(vme_attr->addrl & (vme_align - 1))) {
		pr_err("VME: Can't prepare DMA transfer: wrong VME "
			"aligment/size with respect to data width %d: "
			"vme_addr:0x%x size:0x%x\n", vme_attr->data_width,
			vme_attr->addrl, vme_desc->length);
		return -1;
	}
	return 0;
}

/**
 * Do whatever is necessary to fix a completed DMA transfer
 */
static int __vme_dma_pre_transfer(struct vme_bridge_device *vbridge,
				    struct dma_pre_post_transfer_ctxt *dma_ctxt,
				    int to_user)
{
	int res;

	/* Before doing any thing check some common basic VME alignment */
	res = __vme_check_alignment(&dma_ctxt->desc_new);
	if (res)
		return res;
	if (vbridge->dma_mgr.ops->dma_pre_transfer)
		return vbridge->dma_mgr.ops->dma_pre_transfer(dma_ctxt, to_user);
	return 0;
}

/**
 * Do whatever is necessary to fix a completed DMA transfer
 */
static void __vme_dma_post_transfer(struct vme_bridge_device *vbridge,
				     struct dma_pre_post_transfer_ctxt *dma_ctxt,
				     int to_user)
{
	if (vbridge->dma_mgr.ops->dma_post_transfer)
		vbridge->dma_mgr.ops->dma_post_transfer(dma_ctxt, to_user);
}

static inline enum dma_data_direction dma_data_direction_get(struct vme_dma *desc) {
	switch (desc->dir) {
		case VME_DMA_TO_DEVICE:
			return DMA_TO_DEVICE;
		case VME_DMA_FROM_DEVICE:
			return DMA_FROM_DEVICE;
		default:
			return DMA_NONE;
	}
}

static inline enum dma_transfer_direction dma_transfer_direction_get(struct vme_dma *desc) {
	switch (desc->dir) {
		case VME_DMA_TO_DEVICE:
			return DMA_MEM_TO_DEV;
		case VME_DMA_FROM_DEVICE:
			return DMA_DEV_TO_MEM;
		default:
			return DMA_TRANS_NONE;
	}
}


/*
 * @to_user:	1 - the transfer is to/from a user-space buffer
 *		0 - the transfer is to/from a kernel buffer
 */
static int __vme_do_dma(struct vme_dma *desc, int to_user)
{
	struct vme_bridge_device *vbridge = vbridge_gbl;
	struct vme_dma_chan *vchan;
	struct dma_chan *dchan;
	struct dma_async_tx_descriptor *tx = NULL;
	int err = 0, nr_pages, sg_mapped, sg_pages;
	struct scatterlist *sgl;
	uint64_t uaddr;
	enum dma_data_direction direction = dma_data_direction_get(desc);
	dma_cookie_t cookie;
	char *err_msg;
	struct vme_dma_tx_ctxt dma_tx_ctxt;

	switch (direction) {
		case DMA_TO_DEVICE:
			uaddr = desc->src.addru;
			uaddr <<= 32;
			uaddr |= desc->src.addrl;
			break;
		case DMA_FROM_DEVICE:
			uaddr = desc->dst.addru;
			uaddr <<= 32;
			uaddr |= desc->dst.addrl;
			break;
		default:
			dev_err(&vbridge->pdev->dev,
				"Unsupported DMA direction %i\n", direction);
			return -EINVAL;
	}

	/* Check for overflow */
	if ((uaddr + desc->length) < uaddr)
		return -EINVAL;

	nr_pages = ((uaddr & ~PAGE_MASK) + desc->length + ~PAGE_MASK)
		>> PAGE_SHIFT;

	__vme_do_dma_adjust(desc);

	/* DMA request with 5min timeout */
	down(&vbridge->dma_mgr.sem_vme_do_dma);
	dchan = vme_do_dma_request_channel(vbridge_gbl, 300000);
	if (!dchan) {
		err = -ENODEV;
		goto out;
	}

	vchan = to_vme_dma_chan(dchan);

	/* Build scatterlist */
	sgl = kcalloc(nr_pages, sizeof(struct scatterlist), GFP_KERNEL);
	if (!sgl) {
		err = -ENOMEM;
		goto out_sgl_alloc;
	}
	sg_pages = sgl_map_user_pages(sgl, nr_pages, uaddr,
				      desc->length,
				      (direction == DMA_FROM_DEVICE),
				      to_user);
	if (sg_pages <= 0) {
		err = sg_pages;
		goto out_sgl_map;
	}
#if KERNEL_VERSION(5, 18, 0) > LINUX_VERSION_CODE
	sg_mapped = pci_map_sg(vbridge->pdev, sgl,
#else
	sg_mapped = dma_map_sg(&vbridge->pdev->dev, sgl,
#endif
			       sg_pages, direction);

	/*
	 * The proper function from the DMA engine does not allow us to set
	 * the context, but we need it
	 */
	if (dchan->device && dchan->device->device_prep_slave_sg)
		tx = dchan->device->device_prep_slave_sg(dchan, sgl, sg_mapped,
							 dma_transfer_direction_get(desc),
							 0, desc);
	if (!tx) {
		err = -EINVAL;
		goto out_prep_slave;
	}

	dma_tx_ctxt.vchan = vchan;
	dma_tx_ctxt.dma_res.result = DMA_TRANS_NOERROR;
	dma_tx_ctxt.dma_res.residue = 0;
#if KERNEL_VERSION(3, 10, 0) <= LINUX_VERSION_CODE
	/* Setup the DMA completion callback */
	tx->callback_result = vme_dma_tx_complete;
	tx->callback_param = (void *)&dma_tx_ctxt;
#else
	tx->callback = vme_dma_tx_complete;
	tx->callback_param = (void *)&dchan;
#endif

	/* Execute the DMA transfer */
	cookie = dmaengine_submit(tx);
	if (cookie < 0) {
		err = cookie;
		goto out_submit;
	}

	dma_async_issue_pending(dchan);

	/*
	 * Wait for DMA completion - 60s timeout
	 * If our cookie is not marked as success than we wait more until
	 * the timeout
	 */
	err = wait_event_interruptible_timeout(vchan->wait,
			vme_dma_is_tx_complete(dchan, cookie, &dma_tx_ctxt),
			msecs_to_jiffies(60000));

	/* Check the status of our transfer */
	if (err <= 0) {
		/* timeout elapsed or signal interruption */
		err_msg = (err == 0) ? "DMA timeout elapsed" :
			"DMA interrupted by a signal";
		dev_err(dchan->device->dev, "%s\n", err_msg);
		/*
		 * In both cases try to terminate DMA TX
		 * TODO: not tested....
		 */
		//dchan->device->device_control(dchan, DMA_TERMINATE_ALL, 0);
	} else { // DMA is completed failed or succeeded
#if KERNEL_VERSION(3, 10, 0) <= LINUX_VERSION_CODE
		switch (dma_tx_ctxt.dma_res.result) {
		case DMA_TRANS_NOERROR:
			err = 0;
			break;
		default:
			dev_err(&dchan->dev->device, "DMA Failed %d\n",
				dma_tx_ctxt.dma_res.result);
			err = -EINVAL;
			break;
		}

#else
		switch (dma_async_is_tx_complete(dchan, cookie, NULL, NULL)) {
		case DMA_SUCCESS:
			err = 0;
			break;
		default:
			//keeps this error code as it was for compatibility
			err = -EINVAL;
			break;
		}
#endif
	}

out_submit:
out_prep_slave:
	/* Release scatterlist resources */
#if KERNEL_VERSION(5, 18, 0) > LINUX_VERSION_CODE
	pci_unmap_sg(vbridge->pdev, sgl, sg_mapped, direction);
#else
	dma_unmap_sg(&vbridge->pdev->dev, sgl, sg_mapped, direction);
#endif
	sgl_unmap_user_pages(sgl, sg_pages, 0, to_user);
out_sgl_map:
	kfree(sgl);
out_sgl_alloc:
	dma_release_channel(dchan);
	up(&vbridge->dma_mgr.sem_vme_do_dma);
out:
	return err;
}

static int _vme_do_dma(struct vme_dma *desc, int to_user)
{
	struct vme_bridge_device *vbridge = vbridge_gbl;
	struct dma_pre_post_transfer_ctxt dma_ctxt;
	int err = 0;

	/*
	 * be paranoid: mapping pages with a size=0 lead to crash into kernel
	 * map_single. So to avoid this we check if dma length is not 0
	 */
	if (desc->length == 0)
		return -EINVAL;
	memset (&dma_ctxt, 0, sizeof(dma_ctxt));
	/* clone original dma descriptor into dma context */
	memcpy(&dma_ctxt.desc_new, desc, sizeof(dma_ctxt.desc_new));

	err = __vme_dma_pre_transfer(vbridge, &dma_ctxt, to_user);
	if (err)
		return err;

	/*
	 * the dma_transfer_pre() function may change the transfer size
	 * to adapt it to the hardware capabilities, this means that it could
	 * be zero and we do not need to do a DMA transfer which otherwise
	 * will be detected as erroneous.
	 */
	if (dma_ctxt.desc_new.length > 0) {
		err = __vme_do_dma(&dma_ctxt.desc_new, to_user);
		if (err)
			goto release_vme_mapping;
	}

	__vme_dma_post_transfer(vbridge, &dma_ctxt, to_user);

release_vme_mapping:
	if (dma_ctxt.map.kernel_va != NULL)
		vme_release_mapping(&dma_ctxt.map, 1);
	return err;
}

/**
 * vme_do_dma() - Do a DMA transfer
 * @desc: DMA transfer descriptor
 *
 *  This function first checks the validity of the user supplied DMA transfer
 * parameters. It then tries to find an available DMA channel to do the
 * transfer, setups that channel and starts the DMA.
 *
 *  Returns 0 on success, or a standard kernel error code on failure.
 */
int vme_do_dma(struct vme_dma *desc)
{
	return _vme_do_dma(desc, 1);
}
EXPORT_SYMBOL_GPL(vme_do_dma);

/**
 * vme_do_dma_kernel() - Do a DMA transfer to/from a kernel buffer
 * @desc: DMA transfer descriptor
 *
 * Returns 0 on success, or a standard kernel error code on failure.
 */
int vme_do_dma_kernel(struct vme_dma *desc)
{
	return _vme_do_dma(desc, 0);
}
EXPORT_SYMBOL_GPL(vme_do_dma_kernel);

/**
 * vme_dma_ioctl() - ioctl file method for the VME DMA device
 * @file: Device file descriptor
 * @cmd: ioctl number
 * @arg: ioctl argument
 *
 *  Currently the VME DMA device supports the following ioctl:
 *
 *    VME_IOCTL_START_DMA
 */
long vme_dma_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
	int rc = 0;
	struct vme_dma desc;
	void __user *argp = (void __user *)arg;

	switch (cmd) {
	case VME_IOCTL_START_DMA:
		/* Get the DMA transfer descriptor */
		if (copy_from_user(&desc, (void *)argp, sizeof(struct vme_dma)))
			return -EFAULT;

		/* Do the DMA */
		rc = vme_do_dma(&desc);

		if (rc)
			return rc;

		/*
		 * Copy back the DMA transfer descriptor containing the DMA
		 * updated status.
		 */
		if (copy_to_user((void *)argp, &desc, sizeof(struct vme_dma)))
			return -EFAULT;

		break;

	default:
		rc = -ENOIOCTLCMD;
	}


	return rc;
}
