/* SPDX-License-Identifier: GPL-2.0-or-later */
/**
 * VME tsi dmaengine
 *
 * Copyright (c) 2016 CERN
 * Author: Federico Vaga <federico.vaga@cern.ch>
 *
 */

#ifndef _VME__PROCFS_H_
#define _VME__PROCFS_H_

#include <linux/debugfs.h>

struct vme_bridge_procfs_ops {
	void (*procfs_register)(struct vme_bridge_device *vbridge);
	void (*procfs_unregister)(struct vme_bridge_device *vbridge);
};

struct vme_bridge_procfs_mgr {
#ifdef CONFIG_PROC_FS
	struct proc_dir_entry  *proc_root;
	struct proc_dir_entry  *proc_chip;
#endif

	struct vme_bridge_procfs_ops *ops;

	void			  *private; /* to hold specific HW info */
};

#endif /* _VME__PROCFS_H_ */
