// Copyright (C) 2017 CERN (www.cern.ch)
// SPDX-FileCopyrightText: 2023 2023 CERN (home.cern)
//
// SPDX-License-Identifier: GPL-2.0-or-later

#include <linux/version.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/delay.h>
#include <linux/atomic.h>
#include <linux/io.h>

#include "../include/vmebus.h"

#define PFX "BusErrHandler: "
#define DRV_MODULE_VERSION	"1.0"

/* Number of invalid access trials */
#define TRY_NR		10

/* IRQ handlers counter to check if all of them are fired */
atomic_t irq_counter = ATOMIC_INIT(0);

static struct vme_mapping map_desc = {
	.data_width		= VME_D32,
	.am			= VME_A24_USER_DATA_SCT,
	.read_prefetch_enabled	= 0,
	.sizeu			= 0,
	.sizel			= 0x10000,
	.vme_addru		= 0,
	.vme_addrl		= 0x00000
};

static void berr_handler(struct vme_bus_error* err)
{
	atomic_inc(&irq_counter);
	/*vme_bus_error_check(1);*/
	printk(KERN_DEBUG PFX "Bus Error Handler: error address=0x%llx am=0x%x\n",
			err->address, err->am);
}

static struct vme_berr_handler* berr_handler_desc;

static int __init berrhandler_init_module(void)
{
	int rc, i;
	struct vme_bus_error error_desc;
	int error_count = 0;

	if ((rc = vme_find_mapping(&map_desc, 1)) != 0) {
		printk(KERN_ERR PFX "Failed to map");
		return rc;
	}

	/* Register bus error handler */
	error_desc.address	= map_desc.vme_addrl;
	error_desc.am		= map_desc.am;
	berr_handler_desc = vme_register_berr_handler(&error_desc,
			map_desc.sizel, &berr_handler);

	if (!berr_handler_desc)
	{
		printk(KERN_ERR PFX "Could not register bus error handler");
		rc = -EINVAL;
		goto out_release_mapping;
	}

	/* Generate errors */
	for (i = 0; i < TRY_NR; i++) {
		printk(KERN_DEBUG PFX "Generating invalid access #%d\n", i);
		ioread32be(map_desc.kernel_va + i * 256);

		/* Wait for the IRQ to execute */
		mdelay(500);

		/* Check and clear error */
		if (vme_bus_error_check(1)) {
			printk(KERN_ERR PFX "Bus Error #%d detected\n", i);
			++error_count;
		} else {
			printk(KERN_ERR PFX "Bus Error #%d not detected\n", i);
		}
	}

	printk(KERN_INFO PFX "Detected %d of %d errors\n", error_count, TRY_NR);
	printk(KERN_INFO PFX "Bus error handler was executed %d of %d times\n",
			atomic_read(&irq_counter), TRY_NR);

	return 0;

out_release_mapping:
	vme_release_mapping(&map_desc, 1);

	return rc;
}

static void __exit berrhandler_exit_module(void)
{
	if (berr_handler_desc)
		vme_unregister_berr_handler(berr_handler_desc);

	if (vme_release_mapping(&map_desc, 1) != 0)
		printk(KERN_WARNING PFX "Failed to release mapping\n");
}

module_init(berrhandler_init_module);
module_exit(berrhandler_exit_module);

MODULE_AUTHOR("Maciej Suminski <maciej.suminski@cern.ch>");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("VME bus error handler test module");
MODULE_VERSION(DRV_MODULE_VERSION);
