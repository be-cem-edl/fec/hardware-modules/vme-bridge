# SPDX-FileCopyrightText: 2023 2023 CERN (home.cern)
#
# SPDX-License-Identifier: LGPL-2.1-or-later

TOPDIR = $(shell pwd)/..
REPO_PARENT ?= $(TOPDIR)/../..

# include the build environment
-include $(REPO_PARENT)/common.mk

AR ?= ar
RANLIB ?= ranlib
RM ?= rm -f

SRCS =	create_window.c destroy_window.c setflag.c \
	test_mapping.c testctr.c testctr2.c testctr_ces.c \
	testvd80.c vd80spd.c vd80spd-dma.c mm6390-dma.c \
	testvd802.c hsm-dma.c berrtest.c doublemap.c

GIT_VERSION := $(shell git describe --dirty --long --tags)
CFLAGS		= -Wall -DDEBUG -D_GNU_SOURCE -g -I../include
CFLAGS 		+= -DGIT_VERSION=\"$(GIT_VERSION)\"
LIBVMEBUS	= ../lib/libvmebus.a
LIBVD80		= libvd80.a
LDLIBS		= -lrt

CPPCHECK ?= cppcheck
FLAWFINDER ?= flawfinder

ALL=$(basename $(SRCS))
all: $(ALL)

sources: $(SRCS) $(SCRIPTS)

testctr_ces: testctr_ces.c $(LIBVMEBUS)
testctr2: testctr2.c $(LIBVMEBUS)

testvd80: testvd80.c $(LIBVD80) $(LIBVMEBUS)
testvd802: testvd802.c $(LIBVMEBUS)
vd80spd: vd80spd.c $(LIBVD80) $(LIBVMEBUS)
vd80spd-dma: vd80spd-dma.c $(LIBVD80) $(LIBVMEBUS)

libvd80.o: libvd80.c libvd80.h

libvd80.a: libvd80.o
	$(AR) rv $(LIBVD80) $^
	$(RANLIB) $(LIBVD80)

libvd80doc: libvd80.c libvd80.h vd80.h
	mkdir -p doc
	doxygen libvd80.dox

mm6390-dma: mm6390-dma.c $(LIBVMEBUS)
hsm-dma: hsm-dma.c $(LIBVMEBUS)
berrtest: berrtest.c $(LIBVMEBUS)
doublemap: doublemap.c $(LIBVMEBUS)

doc: libvd80doc

clean cleanall:
	$(RM) *.o *.a $(ALL)
	$(RM) -r doc

# deliver create_window to the same place as driver is
DESTDIR ?=
prefix ?= /usr/local
exec_prefix ?= $(prefix)
bindir ?= $(exec_prefix)/bin

install: all
	mkdir -m 0775 -p $(DESTDIR)$(bindir)
	install -D -t $(DESTDIR)$(bindir) -m 0755 $(ALL)

cppcheck:
	$(CPPCHECK) -q -I. -I$(TOPDIR)/include  --enable=all *.c *.h

flawfinder:
# libvmebus.c has UTF-8 value 0xe9. Thus flawfinder throws error for inconsistent encoding.
#	$(FLAWFINDER) -SQDC --error-level=3 .

.PHONY: all sources doc clean libvd80doc cppcheck flawfinder
