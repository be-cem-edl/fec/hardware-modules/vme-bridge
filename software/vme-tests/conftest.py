# SPDX-FileCopyrightText: 2023 2023 CERN (home.cern)
#
# SPDX-License-Identifier: LGPL-2.1-or-later

import pytest
from subprocess import Popen
import subprocess
from PyVME.PyVmeDevice import PyVmeDevice
import time

VME64X_VD80_SLOTS = [9, ]


@pytest.fixture(scope="module")
def vme_dev_param():
    return {"slot": 11,
            "am": 0x39,
            "addr": 0x580000,
            "size": 0x80000,
            "irq_vector": 0xd9,
            "irq_level": 2,
            "devname": "fmc-svec-a24"
            }

@pytest.fixture(scope="module")
def vme_device_static(vme_dev_param):
    vme_device_static = VMEDevice("vme.{:02d}".format(vme_dev_param["slot"]),
                                  vme_dev_param["slot"],
                                  vme_dev_param["am"],
                                  vme_dev_param["addr"],
                                  vme_dev_param["size"],
                                  vme_dev_param["irq_vector"],
                                  vme_dev_param["irq_level"],
                                  vme_dev_param["devname"])
    try:
        vme_device_static.register()
    except Exception as e:
        vme_device_static.unregister()
        raise e
    yield vme_device_static
    vme_device_static.unregister()

@pytest.fixture(scope="module", params=VME64X_VD80_SLOTS)
def vme_device_vd80(request):
    vdev = PyVmeDevice(request.param)
    try:
        vdev.rescan()
    except Exception as e:
        vdev.unregister()
        raise e
    yield vdev
    vdev.unregister()

@pytest.fixture(scope="module")
def svec(svec_slot):
    vdev = PyVmeDevice(svec_slot)
    try:
        vdev.rescan()
    except Exception as e:
        vdev.unregister()
        raise e
    yield vdev
    vdev.unregister()
    # Some tests may load the driver - remove it
    subprocess.run(["rmmod", "svec-fmc-carrier"], check=False)

@pytest.fixture(scope="module")
def svec_ader(svec_slot):
    vdev = PyVmeDevice(svec_slot)
    try:
        vdev.rescan()
        vdev.ader_set(0, 0x01000000, 0x09)
        vdev.ader_set(1, 0x00100000, 0x39)
    except Exception as e:
        vdev.unregister()
        raise e
    yield vdev
    vdev.unregister()

@pytest.fixture(scope="module")
def svec_empty(svec_slot_empty):
    vdev = PyVmeDevice(svec_slot_empty)
    try:
        vdev.rescan()
    except Exception as e:
        vdev.unregister()
        raise e
    yield vdev
    vdev.unregister()
    # Some tests may load the driver - remove it
    subprocess.run(["rmmod", "svec-fmc-carrier"], check=False)


@pytest.fixture(scope="module")
def svec_all():
    svecs = []
    try:
        for slot in pytest.svec_slots:
            vdev = PyVmeDevice(slot)
            vdev.rescan()
            svecs.append(vdev)
    except Exception as e:
        for vdev in svecs:
            vdev.unregister()
        raise e
    yield svecs
    for vdev in svecs:
        vdev.unregister()

# Execute this at the beginning

@pytest.fixture(scope="session", autouse=True)
def apply_command_line_options(request):
    if request.config.getoption("--no-svec-program") is False:
        svec_program_all()

def svec_program_all():
    gateware = "svec_golden_fifo.bin"
    # Program all SVECs with golden bitstream
    subprocess.run(["modprobe",
                    "-d", "/user/fvaga/deployment/L867/",
                    "svec-fmc-carrier"], check=True)
    for slot in pytest.svec_slots:
        with PyVmeDevice(slot) as vdev:
            vdev.register(None, None, 0x80000, None, None, "fmc-svec-a24")
            with open("/sys/kernel/debug/vme.{:02d}/fpga_firmware".format(slot), "w") as f_fpga:
                f_fpga.write(gateware)
            time.sleep(0.5)
    time.sleep(1)
    subprocess.run(["rmmod", "svec-fmc-carrier"], check=True)


VME_CTRV_MODULE = range(1, 6)
@pytest.fixture(scope="module", params=VME_CTRV_MODULE)
def ctrv(request):
    Popen(["echo 'mo {:d} reset q' | /usr/local/bin/ctrtest".format(request.param)],
          shell=True)

    yield request.param

@pytest.fixture(scope="module")
def ctrv_all():
    for module in VME_CTRV_MODULE:
            Popen(["echo 'mo {:d} reset q' | /usr/local/bin/ctrtest".format(module)],
                  shell=True)

    return VME_CTRV_MODULE

def pytest_generate_tests(metafunc):
    if "svec_slot" in metafunc.fixturenames:
        metafunc.parametrize("svec_slot", pytest.svec_slots,
                             scope="module")
    if "svec_slot_empty" in metafunc.fixturenames:
        metafunc.parametrize("svec_slot_empty", pytest.svec_slots_empty,
                             scope="module")

def pytest_addoption(parser):
    parser.addoption("--no-svec-program", action="store_true", default=False)
    parser.addoption("--svec-slot", action="append",
                     type=int, choices=range(1, 22),
                     default=[])
    parser.addoption("--svec-slot-empty", action="append",
                     type=int, choices=range(1, 22),
                     default=[])
    parser.addoption("--vd80-slot", action="append",
                     type=int, choices=range(1, 22),
                     default=[])

def pytest_configure(config):
    pytest.svec_slots_empty = []
    pytest.svec_slots = []
    pytest.VME_CTRV_MODULE = VME_CTRV_MODULE
    if config.getoption("--svec-slot-empty"):
        pytest.svec_slots = config.getoption("--svec-slot-empty")
    if config.getoption("--svec-slot"):
        pytest.svec_slots = config.getoption("--svec-slot")
