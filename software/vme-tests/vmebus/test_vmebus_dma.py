# SPDX-FileCopyrightText: 2023 2023 CERN (home.cern)
#
# SPDX-License-Identifier: LGPL-2.1-or-later

"""
SPDX-License-Identifier: GPL-3.0-or-later
SPDX-FileCopyrightText: 2020 CERN
"""

import pytest
import subprocess
import fcntl
import struct
import collections
import ctypes
import pdb
import random
from PyVME.PyVmeDevice import PyVmeDevice
from PyVME.PyVmeDevice import PyVme


SVEC_RAM_OFFSET=0x4000
SVEC_RAM_SIZE= 8 * 1024
SVEC_RAM_OFFSET_INDIRECT_ADDR=0x2000
SVEC_RAM_OFFSET_INDIRECT_DATA=0x2004

@pytest.fixture(scope="module", params=range(1024, SVEC_RAM_SIZE, 1024))
def dma_buffers(request):
    buffer1 = (ctypes.c_ubyte * request.param)()
    buffer2 = (ctypes.c_ubyte * request.param)()
    for i in range(request.param):
        buffer1[i] = random.randrange(0, 0xFF, 1)
        buffer2[i] = 0
    return (buffer1, buffer2)

@pytest.fixture(scope="module")
def dma_dummy_buffers():
    size = 1024
    buffer1 = (ctypes.c_ubyte * size)()
    buffer2 = (ctypes.c_ubyte * size)()
    for i in range(size):
        buffer1[i] = random.randrange(0, 0xFF, 1)
        buffer2[i] = 0
    return (buffer1, buffer2)


def param_test_dma():
    supported_dma_modes = [
                           (0, 32, 0x08),
                           (0, 32, 0x09),
                           (0, 32, 0x0B),
                           (1, 32, 0x38),
                           (1, 32, 0x39),
                           (1, 32, 0x3B),
                           (1, 16, 0x39),
#                           (1, 16, 0x3B),
    ]
    if PyVme.bridge_name() == "tundra-tsi148":
#        supported_dma_modes.append((0, 16, 0x8))
        supported_dma_modes.append((0, 16, 0x9))
        supported_dma_modes.append((0, 16, 0xB))
        supported_dma_modes.append((1, 16, 0x38))

    return supported_dma_modes

def param_test_dma_novmeinc():
    """Only D32 because the register is 32bit only"""

    supported_dma_modes = [
                           (0, 32, 0x09),
                           (1, 32, 0x39),
                           ]

    return supported_dma_modes

def param_test_dma_param_men_a25_fail():
    unsupported_dma_modes = [(0, 8, 0x08),
                             (0, 8, 0x09),
                             (0, 8, 0x0B),
                             (0, 16, 0x08),
                             (0, 16, 0x09),
                             (0, 16, 0x0B),
                             (1, 16, 0x38),
                             (1, 8, 0x38),
                             (1, 8, 0x39),
                             (1, 8, 0x3B),]
    return unsupported_dma_modes

class TestVmebusDma():
    """VME bus test on DMA"""

    @pytest.mark.parametrize("func,data_width,addr_mod",
                             param_test_dma())
    def test_dma_write(self, svec_ader, dma_buffers, func, data_width,
                       addr_mod):
        """Write by DMA and read by mapping"""
        addr_base = svec_ader.ader_get(func) & 0xFFFFFF00

        try:
            PyVme.write_dma(addr_mod, addr_base + SVEC_RAM_OFFSET,
                            ctypes.addressof(dma_buffers[0]),
                            len(dma_buffers[0]), data_width, 0)
        except OSError as error:
            assert False, str(error)

        desc = PyVme.VME_MAPPING(data_width=32,
                                 am=0x39 if func == 1 else 0x9,
                                 sizel=0x80000,
                                 vme_addrl=addr_base)
        mm = PyVme.mmap(desc, 1)
        for i in range(len(dma_buffers[1])):
            dma_buffers[1][i] = mm[SVEC_RAM_OFFSET + i]
        PyVme.unmap(mm, desc, 1)

        for val1, val2 in zip(dma_buffers[0], dma_buffers[1]):
            assert val1 == val2

    @pytest.mark.parametrize("func,data_width,addr_mod",
                             param_test_dma_novmeinc())
    def test_dma_write_novmeinc(self, svec_ader, dma_buffers, func, data_width,
                                addr_mod):
        """Write by DMA and read by mapping"""
        addr_base = svec_ader.ader_get(func) & 0xFFFFFF00

        desc = PyVme.VME_MAPPING(data_width=32,
                                 am=0x39 if func == 1 else 0x9,
                                 sizel=0x80000,
                                 vme_addrl=addr_base)
        mm = PyVme.mmap(desc, 1)
        mm[SVEC_RAM_OFFSET_INDIRECT_ADDR:SVEC_RAM_OFFSET_INDIRECT_ADDR+4] = b"\x00\x00\x00\x00"

        try:
            PyVme.write_dma(addr_mod, addr_base + SVEC_RAM_OFFSET_INDIRECT_DATA,
                            ctypes.addressof(dma_buffers[0]),
                            len(dma_buffers[0]), data_width, 1)
        except OSError as error:
            PyVme.unmap(mm, desc, 1)
            assert False, str(error)

        mm[SVEC_RAM_OFFSET_INDIRECT_ADDR:SVEC_RAM_OFFSET_INDIRECT_ADDR+4] = b"\x00\x00\x00\x00"
        for i in range(len(dma_buffers[1])):
            dma_buffers[1][i] = mm[SVEC_RAM_OFFSET + i]
        PyVme.unmap(mm, desc, 1)

        for val1, val2 in zip(dma_buffers[0], dma_buffers[1]):
            assert val1 == val2

    @pytest.mark.parametrize("func,data_width,addr_mod",
                             param_test_dma())
    def test_dma_read(self, svec_ader, dma_buffers, func, data_width,
                      addr_mod):
        """Write by mapping and read by DMA"""
        addr_base = svec_ader.ader_get(func) & 0xFFFFFF00

        desc = PyVme.VME_MAPPING(data_width=32,
                                 am=0x39 if func == 1 else 0x9,
                                 sizel=0x80000,
                                 vme_addrl=addr_base)
        mm = PyVme.mmap(desc, 1)
        for i in range(len(dma_buffers[0])):
            mm[SVEC_RAM_OFFSET + i] = dma_buffers[0][i]
        PyVme.unmap(mm, desc, 1)

        try:
            PyVme.read_dma(addr_mod, addr_base + SVEC_RAM_OFFSET,
                           ctypes.addressof(dma_buffers[1]),
                           len(dma_buffers[1]), data_width, 0)
        except OSError as error:
            assert False, str(error)

        for val1, val2 in zip(dma_buffers[0], dma_buffers[1]):
            assert val1 == val2

    @pytest.mark.parametrize("func,data_width,addr_mod",
                             param_test_dma_novmeinc())
    def test_dma_read_novmeinc(self, svec_ader, dma_buffers, func, data_width,
                               addr_mod):
        """Write by mapping and read by DMA"""
        addr_base = svec_ader.ader_get(func) & 0xFFFFFF00

        desc = PyVme.VME_MAPPING(data_width=32,
                                 am=0x39 if func == 1 else 0x9,
                                 sizel=0x80000,
                                 vme_addrl=addr_base)
        mm = PyVme.mmap(desc, 1)
        assert mm is not None, "Failed to mmap card"
        for i in range(len(dma_buffers[0])):
            mm[SVEC_RAM_OFFSET + i] = dma_buffers[0][i]
        mm[SVEC_RAM_OFFSET_INDIRECT_ADDR:SVEC_RAM_OFFSET_INDIRECT_ADDR+4] = b"\x00\x00\x00\x00"
        PyVme.unmap(mm, desc, 1)

        try:
            PyVme.read_dma(addr_mod, addr_base + SVEC_RAM_OFFSET_INDIRECT_DATA,
                           ctypes.addressof(dma_buffers[1]),
                           len(dma_buffers[1]), data_width, 1)
        except OSError as error:
            assert False, str(error)

        for val1, val2 in zip(dma_buffers[0], dma_buffers[1]):
            assert val1 == val2

    @pytest.mark.parametrize("func,data_width,addr_mod",
                             param_test_dma())
    def test_dma_write_and_read(self, svec_ader, dma_buffers, func, data_width,
                                addr_mod):
        """Read and write on a SVEC FPGA internal memory using DMA"""
        addr_base = svec_ader.ader_get(func) & 0xFFFFFF00

        try:
            PyVme.write_dma(addr_mod, addr_base + SVEC_RAM_OFFSET,
                            ctypes.addressof(dma_buffers[0]),
                            len(dma_buffers[0]), data_width)
            PyVme.read_dma(addr_mod, addr_base + SVEC_RAM_OFFSET,
                           ctypes.addressof(dma_buffers[1]),
                           len(dma_buffers[1]), data_width)
        except OSError as error:
            assert False, str(error)

        for val1, val2 in zip(dma_buffers[0], dma_buffers[1]):
            assert val1 == val2

    @pytest.mark.parametrize("func,data_width,addr_mod",
                             param_test_dma_novmeinc())
    def test_dma_write_and_read_novmeinc(self, svec_ader, dma_buffers, func,
                                         data_width, addr_mod):
        """Read and write on a SVEC FPGA internal memory using DMA"""
        addr_base = svec_ader.ader_get(func) & 0xFFFFFF00

        desc = PyVme.VME_MAPPING(data_width=32,
                                 am=0x39 if func == 1 else 0x9,
                                 sizel=0x80000,
                                 vme_addrl=addr_base)
        mm = PyVme.mmap(desc, 1)

        try:
            mm[SVEC_RAM_OFFSET_INDIRECT_ADDR:SVEC_RAM_OFFSET_INDIRECT_ADDR+4] = b"\x00\x00\x00\x00"
            PyVme.write_dma(addr_mod, addr_base + SVEC_RAM_OFFSET_INDIRECT_DATA,
                            ctypes.addressof(dma_buffers[0]),
                            len(dma_buffers[0]), data_width)
            mm[SVEC_RAM_OFFSET_INDIRECT_ADDR:SVEC_RAM_OFFSET_INDIRECT_ADDR+4] = b"\x00\x00\x00\x00"
            PyVme.read_dma(addr_mod, addr_base + SVEC_RAM_OFFSET_INDIRECT_DATA,
                           ctypes.addressof(dma_buffers[1]),
                           len(dma_buffers[1]), data_width)
        except OSError as error:
            PyVme.unmap(mm, desc, 1)
            assert False, str(error)
        PyVme.unmap(mm, desc, 1)

        for val1, val2 in zip(dma_buffers[0], dma_buffers[1]):
            assert val1 == val2

    @pytest.mark.skipif(PyVme.bridge_name() != "men-pldz002",
                        reason="specific test for MEN-A25 (PLDZ002)")
    @pytest.mark.parametrize("func,data_width,addr_mod",
                             param_test_dma_param_men_a25_fail())
    def test_dma_men_a25_fail(self, svec_ader, dma_dummy_buffers, func,
                              data_width, addr_mod):
        addr = svec_ader.ader_get(func) & 0xFFFFFF00
        addr = addr + SVEC_RAM_OFFSET

        with pytest.raises(OSError) as err:
            PyVme.read_dma(addr_mod, addr,
                           ctypes.addressof(dma_dummy_buffers[1]),
                           len(dma_dummy_buffers[1]), data_width)
