# SPDX-FileCopyrightText: 2023 2023 CERN (home.cern)
#
# SPDX-License-Identifier: LGPL-2.1-or-later

"""
SPDX-License-Identifier: GPL-3.0-or-later
SPDX-FileCopyrightText: 2020 CERN
"""

import pytest
import os
import subprocess
import random
import time
from PyVME.PyVmeDevice import PyVmeDevice

@pytest.fixture(scope="module")
def vme_register_param_rnd():
    yield {
        "slot": random.randint(1, 21),
        "size": random.randrange(0x1000, 0x100000, 0x1000),
    }

class TestVmebusRegister(object):
    """It tests that the tool vme-register"""
    def test_registeration_cmd(self, vme_register_param_rnd):
        path = "/sys/bus/vme/devices/slot.{:02d}/vme.{:02d}".format(vme_register_param_rnd["slot"], vme_register_param_rnd["slot"])
        assert not os.path.exists(path)

        vme_register_command = ["sudo",
                                "/user/fvaga/deployment/L867/bin/vme-register",
                                "--slot", str(vme_register_param_rnd["slot"]),
                                "--size", hex(vme_register_param_rnd["size"]),]
        subprocess.run(vme_register_command, check=True)
        assert os.path.exists(path)
        vme_register_command = ["sudo",
                                "/user/fvaga/deployment/L867/bin/vme-module",
                                "--slot", str(vme_register_param_rnd["slot"]),
                                "--remove"]
        subprocess.run(vme_register_command, check=True)
        assert not os.path.exists(path)

    def test_registeration_cls(self, vme_register_param_rnd):
        path = "/sys/bus/vme/devices/slot.{:02d}/vme.{:02d}".format(vme_register_param_rnd["slot"], vme_register_param_rnd["slot"])
        assert not os.path.exists(path)

        with PyVmeDevice(vme_register_param_rnd["slot"]) as vdev:
            vdev.register(size=vme_register_param_rnd["size"])
            assert os.path.exists(path)
            vdev.unregister()
            assert not os.path.exists(path)

    def test_unregister_when_device_in_use(self, vme_register_param_rnd):
        """Test that device disappears only when we stop using it"""
        path_dev =  "/sys/bus/vme/devices/slot.{:02d}/vme.{:02d}/".format(vme_register_param_rnd["slot"], vme_register_param_rnd["slot"])
        with PyVmeDevice(vme_register_param_rnd["slot"]) as vdev:
            vdev.register(size=vme_register_param_rnd["size"])
            assert os.path.exists(path_dev)
            with open(os.path.join(path_dev, "specification"), "r") as f_spec:
                with open(os.path.join(path_dev, "remove"), "w") as f_rem:
                    f_rem.write("1")
                time.sleep(1)
                assert os.path.exists(path_dev)
                f_spec.read()
            time.sleep(1)
            assert not os.path.exists(path_dev)
