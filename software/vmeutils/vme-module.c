// SPDX-FileCopyrightText: 2023 2023 CERN (home.cern)
//
// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-License-Identifier: LGPL-2.1-or-later

/*
 * Copyright (C) 2018 CERN (www.cern.ch)
 * Author: Federico Vaga <federico.vaga@cern.ch>
 */
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#include "vmeutils.h"

const char program_name[] = "vme-module";

int main(int argc, char *argv[])
{
	int ret;

	command_version(argc, argv);
	ret = cmd_module(argc, argv);
	if (ret < 0 ) {
		fprintf(stderr, "%s: failed %s\n",
			program_name, vme_strerror(errno));
		exit(EXIT_FAILURE);
	}
	exit(EXIT_SUCCESS);
}
